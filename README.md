# Distributed Self-Cal Prototype

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sdp-distributed-self-cal-prototype/badge/?version=latest)](https://developer.skao.int/projects/ska-sdp-distributed-self-cal-prototype/en/latest/?badge=latest)

Prototype self-calibration pipeline to distribute processing across HPC cluster nodes, allowing scalability for large datasets and a reduction in overall computation time.

## Description
The pipeline can currently only create a dirty image from a calibrated zarr dataset (MSv4) but more functionality is being actively developed.

This pipeline uses the [SKA SDP Processing Function Library](https://gitlab.com/ska-telescope/sdp/ska-sdp-func) for gridding visibilities and the [SwiFTly algorithm](https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-swiftly) to perform distributed imaging. [Dask](https://github.com/dask/dask) is used as the distributed computing framework to handle the scaling of computations on a HPC cluster.

## Installation

### Requirements
- Python
- git
- conda or podman

### Installation methods
There are two recommended installation methods depending on use case and operating systems:

1. Installation on a Linux machine can be done using `conda` as a package and environment manager
2. Installation on any machine can be done inside a container using `podman`. This avoids all dependency issues, which are particularly problematic for Mac users. It also has the advantage of using the same base image as our CI/CD pipeline.

In both cases, first clone the repository, including SKA submodules, with SSH:

```bash
git clone --recurse-submodules git@gitlab.com:ska-telescope/sdp/science-pipeline-workflows/ska-sdp-distributed-self-cal-prototype.git
```

or with HTTPS:

```bash
git clone --recurse-submodules https://gitlab.com/ska-telescope/sdp/ska-sdp-distributed-self-cal-prototype.git
```

Submodules are required for use with SKA Makefiles and `make` commands.

Navigate to the project directory before continuing with one of the recommended installation methods:

```bash
cd ska-sdp-distributed-self-cal-prototype
```


### 1. Using conda

1. Install miniconda following the instructions [here](https://docs.anaconda.com/miniconda/#quick-command-line-install). 

2. Create a new environment using `conda` and a valid Python version:

    ```bash
    conda create --name self-cal python=3.11
    ```

3. Activate the new environment:

    ```bash
    conda activate self-cal
    ```
    
4. Install `poetry` (v1.5.1 or later is required) using `pip`:

    ```bash
    pip install poetry
    ```

5. Install project dependencies using `poetry`:

    ```bash
    poetry install
    ```

### 2. Using a container

1. Follow instructions to install [podman](https://podman.io/docs/installation), including initialising and starting a podman machine
2. Build the container:

    This uses the Dockerfile in the root of the project directory and will install all necessary dependencies):

    ```bash
    podman build -f Dockerfile.dev -t dev
    ```

3. Start the container
    - For running commands only (e.g. running pipelines or tests: see below) without developing code, start the container with an interactive shell using:

    ```bash
    podman run -it localhost/dev
    ```

    - For development purposes, mount the project directory on your machine into the container so that you can test your code changes:

    ```
    podman run -it -v /path/to/project/ska-sdp-distributed-self-cal-prototype:/ska-sdp-distributed-self-cal-prototype localhost/dev
    ```

    See the [podman documentation](https://docs.podman.io/en/v5.0.2/markdown/podman-run.1.html) for more options when starting the container.

### 3. Using spack

    1. Clone the spack repo on the working directory:

        ```
        git clone https://github.com/spack/spack.git
        ```

    2. Add the following source:

        ```
        source ./spack/share/spack/setup-env.sh
        ```

    3. In the root directory of ska-sdp-distributed-self-cal-prototype add the spack repo by calling:
    
        ```
        spack repo add ./spack
        ```

    4. Install the specified version by using:

        ``` 
        spack install py-ska-sdp-distributed-self-cal-prototype@latest
        ```

    5. Load the spack module:
        ``` 
        spack load py-ska-sdp-distributed-self-cal-prototype@latest
        ```

## Usage

### Scripts
Details of the scripts can be found [here](https://developer.skao.int/projects/ska-sdp-distributed-self-cal-prototype/en/latest/scripts/scripts_index.html).

### API
API documentation can be found [here](https://developer.skao.int/projects/ska-sdp-distributed-self-cal-prototype/en/latest/api.html).

### Tutorials
Tutorials can be found [here](https://developer.skao.int/projects/ska-sdp-distributed-self-cal-prototype/en/latest/tutorials/tutorials_index.html).

## Project Standards

This project uses [Google-style](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) docstrings, [Black](https://black.readthedocs.io/en/stable/) formatting, [flake8](https://flake8.pycqa.org/en/latest/) and 
[pylint](https://pylint.readthedocs.io/en/stable/) for linting, and [pytest](https://docs.pytest.org/en/stable/) for testing.

## Make Commands
For Unix-like operating systems the `make` command can be used to automate development processes.

### Run Tests
To run all tests and produce a coverage report run:

```
make pytest
```

### Build Docs Locally
To build the documentation locally run:

```
make docs
```

### Run All Checks
To run all the checks on the repository, including sorting imports, formatting, linting, tests, and documentation, run:

```
make checks
```

This should ideally be run before any commits because the same checks will be performed on the GitLab CI/CD pipeline.

## Authors
Authors of the project (sorted by surname alphabetically):
- Gemma Danks (@gemmadanks)
- Malcolm MacLeod (@mmacleod_za)
- Max Maunder (@max_m17)
- Sean Stansill (@sstansill)
- Peter Wortmann (@scpmw)

## License
This project is licensed under the BSD 3-Clause "New" or "Revised" License.
