Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.2.0] - 2024-12-05
### Added
- Feature 1: A dirty image, PSF, residual and model can be created from a MSv4 dataset by performing a single major cycle by calling `clean_hogbom`.
- Feature 2: Major and minor cycles can be performed to create a clean image from a MSv4 dataset by calling `continuum_imaging`.

### Notes
- The normalisation factor applied after gridding is incorrect so the resulting images are not scientifically accurate.
- Deconvolution has been applied to each image facet in parallel which introduces inaccuracies for large PSFs.


## [0.1.0] - 2024-09-11
### Added
- Initial release of the project.
- Feature 1: A dirty image can be created from a MSv4 dataset by calling `dirty_image`.

### Notes
- This version is an early release, and API changes are expected in future versions.
