# Test dataset

The test dataset `3C123_xradio_v0-0-28.ps` was converted from an MSv2 using xradio version 0.0.28. It contains a single partition with the key "3C123_xradio_v0-0-28.ps_ddi_32_intent_OBSERVE_TARGET#UNSPECIFIED_field_id_0". The MAIN xarray Dataset can be seen below. Additional datasets are present, for the full schema see the xradio documentation.

<div><svg style="position: absolute; width: 0; height: 0; overflow: hidden">
<defs>
<symbol id="icon-database" viewBox="0 0 32 32">
<path d="M16 0c-8.837 0-16 2.239-16 5v4c0 2.761 7.163 5 16 5s16-2.239 16-5v-4c0-2.761-7.163-5-16-5z"></path>
<path d="M16 17c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
<path d="M16 26c-8.837 0-16-2.239-16-5v6c0 2.761 7.163 5 16 5s16-2.239 16-5v-6c0 2.761-7.163 5-16 5z"></path>
</symbol>
<symbol id="icon-file-text2" viewBox="0 0 32 32">
<path d="M28.681 7.159c-0.694-0.947-1.662-2.053-2.724-3.116s-2.169-2.030-3.116-2.724c-1.612-1.182-2.393-1.319-2.841-1.319h-15.5c-1.378 0-2.5 1.121-2.5 2.5v27c0 1.378 1.122 2.5 2.5 2.5h23c1.378 0 2.5-1.122 2.5-2.5v-19.5c0-0.448-0.137-1.23-1.319-2.841zM24.543 5.457c0.959 0.959 1.712 1.825 2.268 2.543h-4.811v-4.811c0.718 0.556 1.584 1.309 2.543 2.268zM28 29.5c0 0.271-0.229 0.5-0.5 0.5h-23c-0.271 0-0.5-0.229-0.5-0.5v-27c0-0.271 0.229-0.5 0.5-0.5 0 0 15.499-0 15.5 0v7c0 0.552 0.448 1 1 1h7v19.5z"></path>
<path d="M23 26h-14c-0.552 0-1-0.448-1-1s0.448-1 1-1h14c0.552 0 1 0.448 1 1s-0.448 1-1 1z"></path>
<path d="M23 22h-14c-0.552 0-1-0.448-1-1s0.448-1 1-1h14c0.552 0 1 0.448 1 1s-0.448 1-1 1z"></path>
<path d="M23 18h-14c-0.552 0-1-0.448-1-1s0.448-1 1-1h14c0.552 0 1 0.448 1 1s-0.448 1-1 1z"></path>
</symbol>
</defs>
</svg>
<style>/* CSS stylesheet for displaying xarray objects in jupyterlab.
 *
 */

:root {
  --xr-font-color0: var(--jp-content-font-color0, rgba(0, 0, 0, 1));
  --xr-font-color2: var(--jp-content-font-color2, rgba(0, 0, 0, 0.54));
  --xr-font-color3: var(--jp-content-font-color3, rgba(0, 0, 0, 0.38));
  --xr-border-color: var(--jp-border-color2, #e0e0e0);
  --xr-disabled-color: var(--jp-layout-color3, #bdbdbd);
  --xr-background-color: var(--jp-layout-color0, white);
  --xr-background-color-row-even: var(--jp-layout-color1, white);
  --xr-background-color-row-odd: var(--jp-layout-color2, #eeeeee);
}

html[theme=dark],
html[data-theme=dark],
body[data-theme=dark],
body.vscode-dark {
  --xr-font-color0: rgba(255, 255, 255, 1);
  --xr-font-color2: rgba(255, 255, 255, 0.54);
  --xr-font-color3: rgba(255, 255, 255, 0.38);
  --xr-border-color: #1F1F1F;
  --xr-disabled-color: #515151;
  --xr-background-color: #111111;
  --xr-background-color-row-even: #111111;
  --xr-background-color-row-odd: #313131;
}

.xr-wrap {
  display: block !important;
  min-width: 300px;
  max-width: 700px;
}

.xr-text-repr-fallback {
  /* fallback to plain text repr when CSS is not injected (untrusted notebook) */
  display: none;
}

.xr-header {
  padding-top: 6px;
  padding-bottom: 6px;
  margin-bottom: 4px;
  border-bottom: solid 1px var(--xr-border-color);
}

.xr-header > div,
.xr-header > ul {
  display: inline;
  margin-top: 0;
  margin-bottom: 0;
}

.xr-obj-type,
.xr-array-name {
  margin-left: 2px;
  margin-right: 10px;
}

.xr-obj-type {
  color: var(--xr-font-color2);
}

.xr-sections {
  padding-left: 0 !important;
  display: grid;
  grid-template-columns: 150px auto auto 1fr 0 20px 0 20px;
}

.xr-section-item {
  display: contents;
}

.xr-section-item input {
  display: inline-block;
  opacity: 0;
}

.xr-section-item input + label {
  color: var(--xr-disabled-color);
}

.xr-section-item input:enabled + label {
  cursor: pointer;
  color: var(--xr-font-color2);
}

.xr-section-item input:focus + label {
  border: 2px solid var(--xr-font-color0);
}

.xr-section-item input:enabled + label:hover {
  color: var(--xr-font-color0);
}

.xr-section-summary {
  grid-column: 1;
  color: var(--xr-font-color2);
  font-weight: 500;
}

.xr-section-summary > span {
  display: inline-block;
  padding-left: 0.5em;
}

.xr-section-summary-in:disabled + label {
  color: var(--xr-font-color2);
}

.xr-section-summary-in + label:before {
  display: inline-block;
  content: '►';
  font-size: 11px;
  width: 15px;
  text-align: center;
}

.xr-section-summary-in:disabled + label:before {
  color: var(--xr-disabled-color);
}

.xr-section-summary-in:checked + label:before {
  content: '▼';
}

.xr-section-summary-in:checked + label > span {
  display: none;
}

.xr-section-summary,
.xr-section-inline-details {
  padding-top: 4px;
  padding-bottom: 4px;
}

.xr-section-inline-details {
  grid-column: 2 / -1;
}

.xr-section-details {
  display: none;
  grid-column: 1 / -1;
  margin-bottom: 5px;
}

.xr-section-summary-in:checked ~ .xr-section-details {
  display: contents;
}

.xr-array-wrap {
  grid-column: 1 / -1;
  display: grid;
  grid-template-columns: 20px auto;
}

.xr-array-wrap > label {
  grid-column: 1;
  vertical-align: top;
}

.xr-preview {
  color: var(--xr-font-color3);
}

.xr-array-preview,
.xr-array-data {
  padding: 0 5px !important;
  grid-column: 2;
}

.xr-array-data,
.xr-array-in:checked ~ .xr-array-preview {
  display: none;
}

.xr-array-in:checked ~ .xr-array-data,
.xr-array-preview {
  display: inline-block;
}

.xr-dim-list {
  display: inline-block !important;
  list-style: none;
  padding: 0 !important;
  margin: 0;
}

.xr-dim-list li {
  display: inline-block;
  padding: 0;
  margin: 0;
}

.xr-dim-list:before {
  content: '(';
}

.xr-dim-list:after {
  content: ')';
}

.xr-dim-list li:not(:last-child):after {
  content: ',';
  padding-right: 5px;
}

.xr-has-index {
  font-weight: bold;
}

.xr-var-list,
.xr-var-item {
  display: contents;
}

.xr-var-item > div,
.xr-var-item label,
.xr-var-item > .xr-var-name span {
  background-color: var(--xr-background-color-row-even);
  margin-bottom: 0;
}

.xr-var-item > .xr-var-name:hover span {
  padding-right: 5px;
}

.xr-var-list > li:nth-child(odd) > div,
.xr-var-list > li:nth-child(odd) > label,
.xr-var-list > li:nth-child(odd) > .xr-var-name span {
  background-color: var(--xr-background-color-row-odd);
}

.xr-var-name {
  grid-column: 1;
}

.xr-var-dims {
  grid-column: 2;
}

.xr-var-dtype {
  grid-column: 3;
  text-align: right;
  color: var(--xr-font-color2);
}

.xr-var-preview {
  grid-column: 4;
}

.xr-index-preview {
  grid-column: 2 / 5;
  color: var(--xr-font-color2);
}

.xr-var-name,
.xr-var-dims,
.xr-var-dtype,
.xr-preview,
.xr-attrs dt {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-right: 10px;
}

.xr-var-name:hover,
.xr-var-dims:hover,
.xr-var-dtype:hover,
.xr-attrs dt:hover {
  overflow: visible;
  width: auto;
  z-index: 1;
}

.xr-var-attrs,
.xr-var-data,
.xr-index-data {
  display: none;
  background-color: var(--xr-background-color) !important;
  padding-bottom: 5px !important;
}

.xr-var-attrs-in:checked ~ .xr-var-attrs,
.xr-var-data-in:checked ~ .xr-var-data,
.xr-index-data-in:checked ~ .xr-index-data {
  display: block;
}

.xr-var-data > table {
  float: right;
}

.xr-var-name span,
.xr-var-data,
.xr-index-name div,
.xr-index-data,
.xr-attrs {
  padding-left: 25px !important;
}

.xr-attrs,
.xr-var-attrs,
.xr-var-data,
.xr-index-data {
  grid-column: 1 / -1;
}

dl.xr-attrs {
  padding: 0;
  margin: 0;
  display: grid;
  grid-template-columns: 125px auto;
}

.xr-attrs dt,
.xr-attrs dd {
  padding: 0;
  margin: 0;
  float: left;
  padding-right: 10px;
  width: auto;
}

.xr-attrs dt {
  font-weight: normal;
  grid-column: 1;
}

.xr-attrs dt:hover span {
  display: inline-block;
  background: var(--xr-background-color);
  padding-right: 10px;
}

.xr-attrs dd {
  grid-column: 2;
  white-space: pre-wrap;
  word-break: break-all;
}

.xr-icon-database,
.xr-icon-file-text2,
.xr-no-icon {
  display: inline-block;
  vertical-align: middle;
  width: 1em;
  height: 1.5em !important;
  stroke-width: 0;
  stroke: currentColor;
  fill: currentColor;
}
</style><pre class='xr-text-repr-fallback'>&lt;xarray.Dataset&gt; Size: 3MB
Dimensions:                     (time: 50, baseline_id: 325, frequency: 2,
                                 polarization: 4, uvw_label: 3)
Coordinates:
    baseline_antenna1_id        (baseline_id) int32 1kB dask.array&lt;chunksize=(325,), meta=np.ndarray&gt;
    baseline_antenna2_id        (baseline_id) int32 1kB dask.array&lt;chunksize=(325,), meta=np.ndarray&gt;
  * baseline_id                 (baseline_id) int64 3kB 0 1 2 3 ... 322 323 324
  * frequency                   (frequency) float64 16B 1.377e+10 1.377e+10
  * polarization                (polarization) &lt;U2 32B &#x27;RR&#x27; &#x27;RL&#x27; &#x27;LR&#x27; &#x27;LL&#x27;
  * time                        (time) float64 400B 1.61e+09 ... 1.61e+09
  * uvw_label                   (uvw_label) &lt;U1 12B &#x27;u&#x27; &#x27;v&#x27; &#x27;w&#x27;
Data variables:
    EFFECTIVE_INTEGRATION_TIME  (time, baseline_id) float64 130kB dask.array&lt;chunksize=(50, 325), meta=np.ndarray&gt;
    FLAG                        (time, baseline_id, frequency, polarization) bool 130kB dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;
    TIME_CENTROID               (time, baseline_id) float64 130kB dask.array&lt;chunksize=(50, 325), meta=np.ndarray&gt;
    UVW                         (time, baseline_id, uvw_label) float64 390kB dask.array&lt;chunksize=(50, 325, 3), meta=np.ndarray&gt;
    VISIBILITY                  (time, baseline_id, frequency, polarization) complex64 1MB dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;
    VISIBILITY_CORRECTED        (time, baseline_id, frequency, polarization) complex64 1MB dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;
    WEIGHT                      (time, baseline_id, frequency, polarization) float32 520kB dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;
Attributes:
    data_groups:  {&#x27;base&#x27;: {&#x27;flag&#x27;: &#x27;FLAG&#x27;, &#x27;uvw&#x27;: &#x27;UVW&#x27;, &#x27;visibility&#x27;: &#x27;VISI...
    ddi:          32
    intent:       OBSERVE_TARGET#UNSPECIFIED</pre><div class='xr-wrap' style='display:none'><div class='xr-header'><div class='xr-obj-type'>xarray.Dataset</div></div><ul class='xr-sections'><li class='xr-section-item'><input id='section-f91c61fd-879e-4ba6-9de1-135daa92d654' class='xr-section-summary-in' type='checkbox' disabled ><label for='section-f91c61fd-879e-4ba6-9de1-135daa92d654' class='xr-section-summary'  title='Expand/collapse section'>Dimensions:</label><div class='xr-section-inline-details'><ul class='xr-dim-list'><li><span class='xr-has-index'>time</span>: 50</li><li><span class='xr-has-index'>baseline_id</span>: 325</li><li><span class='xr-has-index'>frequency</span>: 2</li><li><span class='xr-has-index'>polarization</span>: 4</li><li><span class='xr-has-index'>uvw_label</span>: 3</li></ul></div><div class='xr-section-details'></div></li><li class='xr-section-item'><input id='section-49353ad4-7d1a-48bb-bd4d-ed9fc2f59c19' class='xr-section-summary-in' type='checkbox'  checked><label for='section-49353ad4-7d1a-48bb-bd4d-ed9fc2f59c19' class='xr-section-summary' >Coordinates: <span>(7)</span></label><div class='xr-section-inline-details'></div><div class='xr-section-details'><ul class='xr-var-list'><li class='xr-var-item'><div class='xr-var-name'><span>baseline_antenna1_id</span></div><div class='xr-var-dims'>(baseline_id)</div><div class='xr-var-dtype'>int32</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(325,), meta=np.ndarray&gt;</div><input id='attrs-0c703322-21f7-4639-b69a-2b9a66d51ea1' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-0c703322-21f7-4639-b69a-2b9a66d51ea1' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-a4d58e11-38cb-446f-baf8-ebea63626d90' class='xr-var-data-in' type='checkbox'><label for='data-a4d58e11-38cb-446f-baf8-ebea63626d90' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 1.27 kiB </td>
                        <td> 1.27 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (325,) </td>
                        <td> (325,) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> int32 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="170" height="75" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="120" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="120" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="120" y1="0" x2="120" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 120.0,0.0 120.0,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="60.000000" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >325</text>
  <text x="140.000000" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,140.000000,12.706308)">1</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>baseline_antenna2_id</span></div><div class='xr-var-dims'>(baseline_id)</div><div class='xr-var-dtype'>int32</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(325,), meta=np.ndarray&gt;</div><input id='attrs-b0068691-467d-45c4-ab51-40413e91fed4' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-b0068691-467d-45c4-ab51-40413e91fed4' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-5236d6e9-879c-4da0-b2b6-73c05fa05544' class='xr-var-data-in' type='checkbox'><label for='data-5236d6e9-879c-4da0-b2b6-73c05fa05544' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 1.27 kiB </td>
                        <td> 1.27 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (325,) </td>
                        <td> (325,) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> int32 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="170" height="75" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="120" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="120" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="120" y1="0" x2="120" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 120.0,0.0 120.0,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="60.000000" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >325</text>
  <text x="140.000000" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,140.000000,12.706308)">1</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span class='xr-has-index'>baseline_id</span></div><div class='xr-var-dims'>(baseline_id)</div><div class='xr-var-dtype'>int64</div><div class='xr-var-preview xr-preview'>0 1 2 3 4 5 ... 320 321 322 323 324</div><input id='attrs-0df029de-1096-41e3-a434-5285332d24ba' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-0df029de-1096-41e3-a434-5285332d24ba' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-5654bbbd-c209-46d1-a8ce-2fdea061e0ae' class='xr-var-data-in' type='checkbox'><label for='data-5654bbbd-c209-46d1-a8ce-2fdea061e0ae' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><pre>array([  0,   1,   2, ..., 322, 323, 324])</pre></div></li><li class='xr-var-item'><div class='xr-var-name'><span class='xr-has-index'>frequency</span></div><div class='xr-var-dims'>(frequency)</div><div class='xr-var-dtype'>float64</div><div class='xr-var-preview xr-preview'>1.377e+10 1.377e+10</div><input id='attrs-a4b7d24c-3bf0-4f87-8999-caf3c0f046d1' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-a4b7d24c-3bf0-4f87-8999-caf3c0f046d1' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-bbe96f11-f772-4c3a-8333-ae7f1b9a9b52' class='xr-var-data-in' type='checkbox'><label for='data-bbe96f11-f772-4c3a-8333-ae7f1b9a9b52' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>channel_width :</span></dt><dd>{&#x27;attrs&#x27;: {&#x27;type&#x27;: &#x27;quantity&#x27;, &#x27;units&#x27;: [&#x27;Hz&#x27;]}, &#x27;data&#x27;: 2000000.0, &#x27;dims&#x27;: &#x27;&#x27;}</dd><dt><span>frame :</span></dt><dd>TOPO</dd><dt><span>reference_frequency :</span></dt><dd>{&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;TOPO&#x27;, &#x27;type&#x27;: &#x27;spectral_coord&#x27;, &#x27;units&#x27;: [&#x27;Hz&#x27;]}, &#x27;data&#x27;: 13768000000.0, &#x27;dims&#x27;: &#x27;&#x27;}</dd><dt><span>spectral_window_name :</span></dt><dd>EVLA_KU#A1C1#32</dd><dt><span>spw_id :</span></dt><dd>32</dd><dt><span>type :</span></dt><dd>spectral_coord</dd><dt><span>units :</span></dt><dd>[&#x27;Hz&#x27;]</dd></dl></div><div class='xr-var-data'><pre>array([1.3768e+10, 1.3770e+10])</pre></div></li><li class='xr-var-item'><div class='xr-var-name'><span class='xr-has-index'>polarization</span></div><div class='xr-var-dims'>(polarization)</div><div class='xr-var-dtype'>&lt;U2</div><div class='xr-var-preview xr-preview'>&#x27;RR&#x27; &#x27;RL&#x27; &#x27;LR&#x27; &#x27;LL&#x27;</div><input id='attrs-34602038-0b26-4705-9216-95f74b9aadbf' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-34602038-0b26-4705-9216-95f74b9aadbf' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-d9a8928a-e7b6-4c81-b3ca-71b8fe325039' class='xr-var-data-in' type='checkbox'><label for='data-d9a8928a-e7b6-4c81-b3ca-71b8fe325039' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><pre>array([&#x27;RR&#x27;, &#x27;RL&#x27;, &#x27;LR&#x27;, &#x27;LL&#x27;], dtype=&#x27;&lt;U2&#x27;)</pre></div></li><li class='xr-var-item'><div class='xr-var-name'><span class='xr-has-index'>time</span></div><div class='xr-var-dims'>(time)</div><div class='xr-var-dtype'>float64</div><div class='xr-var-preview xr-preview'>1.61e+09 1.61e+09 ... 1.61e+09</div><input id='attrs-92bc8a85-3d1d-437a-95ea-d7391703e1fb' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-92bc8a85-3d1d-437a-95ea-d7391703e1fb' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-3b80ae46-9182-4102-9091-c4b28845ff60' class='xr-var-data-in' type='checkbox'><label for='data-3b80ae46-9182-4102-9091-c4b28845ff60' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>effective_integration_time :</span></dt><dd>EFFECTIVE_INTEGRATION_TIME</dd><dt><span>format :</span></dt><dd>unix</dd><dt><span>integration_time :</span></dt><dd>{&#x27;attrs&#x27;: {&#x27;type&#x27;: &#x27;quantity&#x27;, &#x27;units&#x27;: [&#x27;s&#x27;]}, &#x27;data&#x27;: 2.0, &#x27;dims&#x27;: &#x27;&#x27;}</dd><dt><span>scale :</span></dt><dd>UTC</dd><dt><span>type :</span></dt><dd>time</dd><dt><span>units :</span></dt><dd>[&#x27;s&#x27;]</dd></dl></div><div class='xr-var-data'><pre>array([1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09,
       1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09, 1.609743e+09])</pre></div></li><li class='xr-var-item'><div class='xr-var-name'><span class='xr-has-index'>uvw_label</span></div><div class='xr-var-dims'>(uvw_label)</div><div class='xr-var-dtype'>&lt;U1</div><div class='xr-var-preview xr-preview'>&#x27;u&#x27; &#x27;v&#x27; &#x27;w&#x27;</div><input id='attrs-b518f666-3fda-424b-bd68-ff7ef1b64cab' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-b518f666-3fda-424b-bd68-ff7ef1b64cab' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-d5dab6d3-0cc7-42aa-ad1f-7d62c7e3c25f' class='xr-var-data-in' type='checkbox'><label for='data-d5dab6d3-0cc7-42aa-ad1f-7d62c7e3c25f' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><pre>array([&#x27;u&#x27;, &#x27;v&#x27;, &#x27;w&#x27;], dtype=&#x27;&lt;U1&#x27;)</pre></div></li></ul></div></li><li class='xr-section-item'><input id='section-320182bc-91b1-48ed-9809-829e6945465e' class='xr-section-summary-in' type='checkbox'  checked><label for='section-320182bc-91b1-48ed-9809-829e6945465e' class='xr-section-summary' >Data variables: <span>(7)</span></label><div class='xr-section-inline-details'></div><div class='xr-section-details'><ul class='xr-var-list'><li class='xr-var-item'><div class='xr-var-name'><span>EFFECTIVE_INTEGRATION_TIME</span></div><div class='xr-var-dims'>(time, baseline_id)</div><div class='xr-var-dtype'>float64</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325), meta=np.ndarray&gt;</div><input id='attrs-ae343f1a-6f79-4bb3-b8af-009e83868336' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-ae343f1a-6f79-4bb3-b8af-009e83868336' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-9ee22586-3ff2-4c17-917c-cdf380d4be70' class='xr-var-data-in' type='checkbox'><label for='data-9ee22586-3ff2-4c17-917c-cdf380d4be70' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 126.95 kiB </td>
                        <td> 126.95 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325) </td>
                        <td> (50, 325) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> float64 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="170" height="90" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="120" y2="0" style="stroke-width:2" />
  <line x1="0" y1="40" x2="120" y2="40" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="40" style="stroke-width:2" />
  <line x1="120" y1="0" x2="120" y2="40" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 120.0,0.0 120.0,40.82796497161541 0.0,40.82796497161541" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="60.000000" y="60.827965" font-size="1.0rem" font-weight="100" text-anchor="middle" >325</text>
  <text x="140.000000" y="20.413982" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,140.000000,20.413982)">50</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>FLAG</span></div><div class='xr-var-dims'>(time, baseline_id, frequency, polarization)</div><div class='xr-var-dtype'>bool</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;</div><input id='attrs-73b2fa1e-a210-40d6-9af6-06b22441e873' class='xr-var-attrs-in' type='checkbox' disabled><label for='attrs-73b2fa1e-a210-40d6-9af6-06b22441e873' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-b9760cb6-ea3f-41ee-b840-c228ff83ecd3' class='xr-var-data-in' type='checkbox'><label for='data-b9760cb6-ea3f-41ee-b840-c228ff83ecd3' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 126.95 kiB </td>
                        <td> 126.95 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325, 2, 4) </td>
                        <td> (50, 325, 2, 4) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> bool numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="367" height="146" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="40" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="40" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="40" y1="0" x2="40" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 40.82796497161541,0.0 40.82796497161541,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="20.413982" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >50</text>
  <text x="60.827965" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,60.827965,12.706308)">1</text>


  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="110" y1="25" x2="180" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="110" y2="25" style="stroke-width:2" />
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 180.58823529411765,70.58823529411765 180.58823529411765,96.00085180870013 110.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="136" y2="0" style="stroke-width:2" />
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="136" y1="0" x2="207" y2="70" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 136.43425418445696,0.0 207.0224894785746,70.58823529411765 180.58823529411765,70.58823529411765" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />
  <line x1="180" y1="96" x2="207" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />
  <line x1="207" y1="70" x2="207" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="180.58823529411765,70.58823529411765 207.0224894785746,70.58823529411765 207.0224894785746,96.00085180870013 180.58823529411765,96.00085180870013" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="193.805362" y="116.000852" font-size="1.0rem" font-weight="100" text-anchor="middle" >4</text>
  <text x="227.022489" y="83.294544" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,227.022489,83.294544)">2</text>
  <text x="135.294118" y="80.706734" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(45,135.294118,80.706734)">325</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>TIME_CENTROID</span></div><div class='xr-var-dims'>(time, baseline_id)</div><div class='xr-var-dtype'>float64</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325), meta=np.ndarray&gt;</div><input id='attrs-2a1a791f-6342-45ca-a474-7849dcf6bd17' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-2a1a791f-6342-45ca-a474-7849dcf6bd17' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-a2ed9423-2840-4b7d-95f5-c845b225574e' class='xr-var-data-in' type='checkbox'><label for='data-a2ed9423-2840-4b7d-95f5-c845b225574e' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>format :</span></dt><dd>unix</dd><dt><span>scale :</span></dt><dd>UTC</dd><dt><span>type :</span></dt><dd>time</dd><dt><span>units :</span></dt><dd>[&#x27;s&#x27;]</dd></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 126.95 kiB </td>
                        <td> 126.95 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325) </td>
                        <td> (50, 325) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> float64 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="170" height="90" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="120" y2="0" style="stroke-width:2" />
  <line x1="0" y1="40" x2="120" y2="40" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="40" style="stroke-width:2" />
  <line x1="120" y1="0" x2="120" y2="40" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 120.0,0.0 120.0,40.82796497161541 0.0,40.82796497161541" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="60.000000" y="60.827965" font-size="1.0rem" font-weight="100" text-anchor="middle" >325</text>
  <text x="140.000000" y="20.413982" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,140.000000,20.413982)">50</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>UVW</span></div><div class='xr-var-dims'>(time, baseline_id, uvw_label)</div><div class='xr-var-dtype'>float64</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325, 3), meta=np.ndarray&gt;</div><input id='attrs-f8e1033d-23bb-4da9-8b07-714511d6716e' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-f8e1033d-23bb-4da9-8b07-714511d6716e' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-c6146157-a53b-4695-aedf-5c846a7a56a8' class='xr-var-data-in' type='checkbox'><label for='data-c6146157-a53b-4695-aedf-5c846a7a56a8' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>frame :</span></dt><dd>FK5</dd><dt><span>type :</span></dt><dd>uvw</dd><dt><span>units :</span></dt><dd>[&#x27;m&#x27;, &#x27;m&#x27;, &#x27;m&#x27;]</dd></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 380.86 kiB </td>
                        <td> 380.86 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325, 3) </td>
                        <td> (50, 325, 3) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> float64 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="109" height="194" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="10" y1="0" x2="34" y2="24" style="stroke-width:2" />
  <line x1="10" y1="120" x2="34" y2="144" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="10" y1="0" x2="10" y2="120" style="stroke-width:2" />
  <line x1="34" y1="24" x2="34" y2="144" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="10.0,0.0 34.01644998330318,24.016449983303183 34.01644998330318,144.01644998330318 10.0,120.0" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="10" y1="0" x2="35" y2="0" style="stroke-width:2" />
  <line x1="34" y1="24" x2="59" y2="24" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="10" y1="0" x2="34" y2="24" style="stroke-width:2" />
  <line x1="35" y1="0" x2="59" y2="24" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="10.0,0.0 35.41261651458248,0.0 59.429066497885664,24.016449983303183 34.01644998330318,24.016449983303183" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="34" y1="24" x2="59" y2="24" style="stroke-width:2" />
  <line x1="34" y1="144" x2="59" y2="144" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="34" y1="24" x2="34" y2="144" style="stroke-width:2" />
  <line x1="59" y1="24" x2="59" y2="144" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="34.01644998330318,24.016449983303183 59.42906649788567,24.016449983303183 59.42906649788567,144.01644998330318 34.01644998330318,144.01644998330318" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="46.722758" y="164.016450" font-size="1.0rem" font-weight="100" text-anchor="middle" >3</text>
  <text x="79.429066" y="84.016450" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(-90,79.429066,84.016450)">325</text>
  <text x="12.008225" y="152.008225" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(45,12.008225,152.008225)">50</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>VISIBILITY</span></div><div class='xr-var-dims'>(time, baseline_id, frequency, polarization)</div><div class='xr-var-dtype'>complex64</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;</div><input id='attrs-d9fdb549-78f1-4429-87cd-7ea8c90be7b2' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-d9fdb549-78f1-4429-87cd-7ea8c90be7b2' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-e2260a4c-7119-44a5-881c-e2976cfe6cf1' class='xr-var-data-in' type='checkbox'><label for='data-e2260a4c-7119-44a5-881c-e2976cfe6cf1' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>field_info :</span></dt><dd>{&#x27;code&#x27;: &#x27;NONE&#x27;, &#x27;delay_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}, &#x27;field_id&#x27;: 0, &#x27;name&#x27;: &#x27;3C123&#x27;, &#x27;phase_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}, &#x27;reference_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}}</dd><dt><span>type :</span></dt><dd>quanta</dd><dt><span>units :</span></dt><dd>[&#x27;unkown&#x27;]</dd></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 0.99 MiB </td>
                        <td> 0.99 MiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325, 2, 4) </td>
                        <td> (50, 325, 2, 4) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> complex64 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="367" height="146" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="40" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="40" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="40" y1="0" x2="40" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 40.82796497161541,0.0 40.82796497161541,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="20.413982" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >50</text>
  <text x="60.827965" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,60.827965,12.706308)">1</text>


  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="110" y1="25" x2="180" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="110" y2="25" style="stroke-width:2" />
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 180.58823529411765,70.58823529411765 180.58823529411765,96.00085180870013 110.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="136" y2="0" style="stroke-width:2" />
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="136" y1="0" x2="207" y2="70" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 136.43425418445696,0.0 207.0224894785746,70.58823529411765 180.58823529411765,70.58823529411765" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />
  <line x1="180" y1="96" x2="207" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />
  <line x1="207" y1="70" x2="207" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="180.58823529411765,70.58823529411765 207.0224894785746,70.58823529411765 207.0224894785746,96.00085180870013 180.58823529411765,96.00085180870013" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="193.805362" y="116.000852" font-size="1.0rem" font-weight="100" text-anchor="middle" >4</text>
  <text x="227.022489" y="83.294544" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,227.022489,83.294544)">2</text>
  <text x="135.294118" y="80.706734" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(45,135.294118,80.706734)">325</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>VISIBILITY_CORRECTED</span></div><div class='xr-var-dims'>(time, baseline_id, frequency, polarization)</div><div class='xr-var-dtype'>complex64</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;</div><input id='attrs-62501f42-c9a9-4849-aa36-987d632b54ef' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-62501f42-c9a9-4849-aa36-987d632b54ef' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-17b54c2d-3a96-4503-a125-2f55fb9b0d98' class='xr-var-data-in' type='checkbox'><label for='data-17b54c2d-3a96-4503-a125-2f55fb9b0d98' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>field_info :</span></dt><dd>{&#x27;code&#x27;: &#x27;NONE&#x27;, &#x27;delay_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}, &#x27;field_id&#x27;: 0, &#x27;name&#x27;: &#x27;3C123&#x27;, &#x27;phase_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}, &#x27;reference_direction&#x27;: {&#x27;attrs&#x27;: {&#x27;frame&#x27;: &#x27;FK5&#x27;, &#x27;type&#x27;: &#x27;sky_coord&#x27;, &#x27;units&#x27;: [&#x27;rad&#x27;, &#x27;rad&#x27;]}, &#x27;data&#x27;: [1.208958666, 0.517848003], &#x27;dims&#x27;: &#x27;&#x27;}}</dd><dt><span>type :</span></dt><dd>quanta</dd><dt><span>units :</span></dt><dd>[&#x27;unkown&#x27;]</dd></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 0.99 MiB </td>
                        <td> 0.99 MiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325, 2, 4) </td>
                        <td> (50, 325, 2, 4) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> complex64 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="367" height="146" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="40" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="40" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="40" y1="0" x2="40" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 40.82796497161541,0.0 40.82796497161541,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="20.413982" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >50</text>
  <text x="60.827965" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,60.827965,12.706308)">1</text>


  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="110" y1="25" x2="180" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="110" y2="25" style="stroke-width:2" />
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 180.58823529411765,70.58823529411765 180.58823529411765,96.00085180870013 110.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="136" y2="0" style="stroke-width:2" />
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="136" y1="0" x2="207" y2="70" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 136.43425418445696,0.0 207.0224894785746,70.58823529411765 180.58823529411765,70.58823529411765" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />
  <line x1="180" y1="96" x2="207" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />
  <line x1="207" y1="70" x2="207" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="180.58823529411765,70.58823529411765 207.0224894785746,70.58823529411765 207.0224894785746,96.00085180870013 180.58823529411765,96.00085180870013" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="193.805362" y="116.000852" font-size="1.0rem" font-weight="100" text-anchor="middle" >4</text>
  <text x="227.022489" y="83.294544" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,227.022489,83.294544)">2</text>
  <text x="135.294118" y="80.706734" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(45,135.294118,80.706734)">325</text>
</svg>
        </td>
    </tr>
</table></div></li><li class='xr-var-item'><div class='xr-var-name'><span>WEIGHT</span></div><div class='xr-var-dims'>(time, baseline_id, frequency, polarization)</div><div class='xr-var-dtype'>float32</div><div class='xr-var-preview xr-preview'>dask.array&lt;chunksize=(50, 325, 2, 4), meta=np.ndarray&gt;</div><input id='attrs-8d88a986-135e-4242-bd79-0ba151be2bbe' class='xr-var-attrs-in' type='checkbox' ><label for='attrs-8d88a986-135e-4242-bd79-0ba151be2bbe' title='Show/Hide attributes'><svg class='icon xr-icon-file-text2'><use xlink:href='#icon-file-text2'></use></svg></label><input id='data-f41bda57-5bd6-4675-bb80-a616317ef7f8' class='xr-var-data-in' type='checkbox'><label for='data-f41bda57-5bd6-4675-bb80-a616317ef7f8' title='Show/Hide data repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-var-attrs'><dl class='xr-attrs'><dt><span>type :</span></dt><dd>quanta</dd><dt><span>units :</span></dt><dd>[&#x27;unkown&#x27;]</dd></dl></div><div class='xr-var-data'><table>
    <tr>
        <td>
            <table style="border-collapse: collapse;">
                <thead>
                    <tr>
                        <td> </td>
                        <th> Array </th>
                        <th> Chunk </th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <th> Bytes </th>
                        <td> 507.81 kiB </td>
                        <td> 507.81 kiB </td>
                    </tr>
                    
                    <tr>
                        <th> Shape </th>
                        <td> (50, 325, 2, 4) </td>
                        <td> (50, 325, 2, 4) </td>
                    </tr>
                    <tr>
                        <th> Dask graph </th>
                        <td colspan="2"> 1 chunks in 2 graph layers </td>
                    </tr>
                    <tr>
                        <th> Data type </th>
                        <td colspan="2"> float32 numpy.ndarray </td>
                    </tr>
                </tbody>
            </table>
        </td>
        <td>
        <svg width="367" height="146" style="stroke:rgb(0,0,0);stroke-width:1" >

  <!-- Horizontal lines -->
  <line x1="0" y1="0" x2="40" y2="0" style="stroke-width:2" />
  <line x1="0" y1="25" x2="40" y2="25" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="0" y1="0" x2="0" y2="25" style="stroke-width:2" />
  <line x1="40" y1="0" x2="40" y2="25" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="0.0,0.0 40.82796497161541,0.0 40.82796497161541,25.412616514582485 0.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="20.413982" y="45.412617" font-size="1.0rem" font-weight="100" text-anchor="middle" >50</text>
  <text x="60.827965" y="12.706308" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,60.827965,12.706308)">1</text>


  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="110" y1="25" x2="180" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="110" y2="25" style="stroke-width:2" />
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 180.58823529411765,70.58823529411765 180.58823529411765,96.00085180870013 110.0,25.412616514582485" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="110" y1="0" x2="136" y2="0" style="stroke-width:2" />
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="110" y1="0" x2="180" y2="70" style="stroke-width:2" />
  <line x1="136" y1="0" x2="207" y2="70" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="110.0,0.0 136.43425418445696,0.0 207.0224894785746,70.58823529411765 180.58823529411765,70.58823529411765" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Horizontal lines -->
  <line x1="180" y1="70" x2="207" y2="70" style="stroke-width:2" />
  <line x1="180" y1="96" x2="207" y2="96" style="stroke-width:2" />

  <!-- Vertical lines -->
  <line x1="180" y1="70" x2="180" y2="96" style="stroke-width:2" />
  <line x1="207" y1="70" x2="207" y2="96" style="stroke-width:2" />

  <!-- Colored Rectangle -->
  <polygon points="180.58823529411765,70.58823529411765 207.0224894785746,70.58823529411765 207.0224894785746,96.00085180870013 180.58823529411765,96.00085180870013" style="fill:#ECB172A0;stroke-width:0"/>

  <!-- Text -->
  <text x="193.805362" y="116.000852" font-size="1.0rem" font-weight="100" text-anchor="middle" >4</text>
  <text x="227.022489" y="83.294544" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(0,227.022489,83.294544)">2</text>
  <text x="135.294118" y="80.706734" font-size="1.0rem" font-weight="100" text-anchor="middle" transform="rotate(45,135.294118,80.706734)">325</text>
</svg>
        </td>
    </tr>
</table></div></li></ul></div></li><li class='xr-section-item'><input id='section-4ebe6850-f98f-4b04-816d-0b4c431a4d44' class='xr-section-summary-in' type='checkbox'  ><label for='section-4ebe6850-f98f-4b04-816d-0b4c431a4d44' class='xr-section-summary' >Indexes: <span>(5)</span></label><div class='xr-section-inline-details'></div><div class='xr-section-details'><ul class='xr-var-list'><li class='xr-var-item'><div class='xr-index-name'><div>baseline_id</div></div><div class='xr-index-preview'>PandasIndex</div><div></div><input id='index-f2682eab-3d79-4a20-a8bc-b037f4c8fb3c' class='xr-index-data-in' type='checkbox'/><label for='index-f2682eab-3d79-4a20-a8bc-b037f4c8fb3c' title='Show/Hide index repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-index-data'><pre>PandasIndex(Index([  0,   1,   2,   3,   4,   5,   6,   7,   8,   9,
       ...
       315, 316, 317, 318, 319, 320, 321, 322, 323, 324],
      dtype=&#x27;int64&#x27;, name=&#x27;baseline_id&#x27;, length=325))</pre></div></li><li class='xr-var-item'><div class='xr-index-name'><div>frequency</div></div><div class='xr-index-preview'>PandasIndex</div><div></div><input id='index-27d064c8-8c04-4e7d-9f62-813a9f2a492a' class='xr-index-data-in' type='checkbox'/><label for='index-27d064c8-8c04-4e7d-9f62-813a9f2a492a' title='Show/Hide index repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-index-data'><pre>PandasIndex(Index([13768000000.0, 13770000000.0], dtype=&#x27;float64&#x27;, name=&#x27;frequency&#x27;))</pre></div></li><li class='xr-var-item'><div class='xr-index-name'><div>polarization</div></div><div class='xr-index-preview'>PandasIndex</div><div></div><input id='index-2452ad0f-921c-490d-8342-21f591be55fb' class='xr-index-data-in' type='checkbox'/><label for='index-2452ad0f-921c-490d-8342-21f591be55fb' title='Show/Hide index repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-index-data'><pre>PandasIndex(Index([&#x27;RR&#x27;, &#x27;RL&#x27;, &#x27;LR&#x27;, &#x27;LL&#x27;], dtype=&#x27;object&#x27;, name=&#x27;polarization&#x27;))</pre></div></li><li class='xr-var-item'><div class='xr-index-name'><div>time</div></div><div class='xr-index-preview'>PandasIndex</div><div></div><input id='index-41c2ba5e-f0ad-49af-bc35-f6a5b25e3a00' class='xr-index-data-in' type='checkbox'/><label for='index-41c2ba5e-f0ad-49af-bc35-f6a5b25e3a00' title='Show/Hide index repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-index-data'><pre>PandasIndex(Index([     1609742881.0, 1609742883.000001, 1609742884.999999,
            1609742887.0,      1609742889.0, 1609742891.000001,
       1609742892.999999,      1609742895.0,      1609742897.0,
       1609742899.000001, 1609742900.999999,      1609742903.0,
       1609742904.999999, 1609742907.000001, 1609742908.999999,
            1609742911.0, 1609742912.999999, 1609742915.000001,
       1609742916.999999,      1609742919.0, 1609742920.999999,
       1609742923.000001,      1609742925.0,      1609742927.0,
       1609742928.999999, 1609742931.000001,      1609742933.0,
            1609742935.0, 1609742936.999999, 1609742939.000001,
            1609742941.0,      1609742943.0, 1609742944.999999,
       1609742947.000001,      1609742949.0,      1609742951.0,
       1609742952.999999, 1609742955.000001,      1609742957.0,
       1609742959.000001, 1609742960.999999, 1609742963.000001,
            1609742965.0, 1609742967.000001, 1609742968.999999,
       1609742971.000001,      1609742973.0, 1609742975.000001,
       1609742976.999999,      1609742979.0],
      dtype=&#x27;float64&#x27;, name=&#x27;time&#x27;))</pre></div></li><li class='xr-var-item'><div class='xr-index-name'><div>uvw_label</div></div><div class='xr-index-preview'>PandasIndex</div><div></div><input id='index-79bc8e0d-06a8-40d4-9619-a9961490b8b4' class='xr-index-data-in' type='checkbox'/><label for='index-79bc8e0d-06a8-40d4-9619-a9961490b8b4' title='Show/Hide index repr'><svg class='icon xr-icon-database'><use xlink:href='#icon-database'></use></svg></label><div class='xr-index-data'><pre>PandasIndex(Index([&#x27;u&#x27;, &#x27;v&#x27;, &#x27;w&#x27;], dtype=&#x27;object&#x27;, name=&#x27;uvw_label&#x27;))</pre></div></li></ul></div></li><li class='xr-section-item'><input id='section-9e1b392c-5400-4bdc-90dd-a9e715eb4c6e' class='xr-section-summary-in' type='checkbox'  checked><label for='section-9e1b392c-5400-4bdc-90dd-a9e715eb4c6e' class='xr-section-summary' >Attributes: <span>(3)</span></label><div class='xr-section-inline-details'></div><div class='xr-section-details'><dl class='xr-attrs'><dt><span>data_groups :</span></dt><dd>{&#x27;base&#x27;: {&#x27;flag&#x27;: &#x27;FLAG&#x27;, &#x27;uvw&#x27;: &#x27;UVW&#x27;, &#x27;visibility&#x27;: &#x27;VISIBILITY&#x27;, &#x27;weight&#x27;: &#x27;WEIGHT&#x27;}, &#x27;corrected&#x27;: {&#x27;flag&#x27;: &#x27;FLAG&#x27;, &#x27;uvw&#x27;: &#x27;UVW&#x27;, &#x27;visibility&#x27;: &#x27;VISIBILITY_CORRECTED&#x27;, &#x27;weight&#x27;: &#x27;WEIGHT&#x27;}}</dd><dt><span>ddi :</span></dt><dd>32</dd><dt><span>intent :</span></dt><dd>OBSERVE_TARGET#UNSPECIFIED</dd></dl></div></li></ul></div></div>