"""Unit tests for the Swiftly class."""

from typing import Callable

import numpy as np
from ska_sdp_exec_swiftly import SubgridConfig

from ska_sdp_distributed_self_cal_prototype.data_managers.swiftly import Swiftly
from ska_sdp_distributed_self_cal_prototype.processing_tasks.gridding import Gridder
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import ImageInfo, PhaseCenter, PipelineConfig
from ska_sdp_distributed_self_cal_prototype.workflow.utils import save_image


def test_save_image(tmp_path):
    image_info = ImageInfo(
        phase_centre=PhaseCenter(
            phase_centre_ra=-1.5773413114898764, phase_centre_dec=-0.4072434921320101, phase_centre_frame="fk5"
        ),
        pixel_width_arcsec=0.5,
        pixel_height_arcsec=0.5,
        start_frequency=100000000.0,
        end_frequency=300000000.0,
        n_output_channels=1,
    )

    image = np.array(
        [
            [0.40723793, 0.32148627, 0.44059888, 0.41862874, 0.45710954],
            [0.44633975, 0.41411162, 0.53760405, 0.5331245, 0.50010153],
            [0.35326328, 0.45792268, 0.5743321, 0.54547963, 0.56958197],
            [0.44774286, 0.46599295, 0.48399777, 0.43644114, 0.48012128],
            [0.70015507, 0.65790663, 0.61736625, 0.52972877, 0.5023709],
        ]
    )
    saved_image_path_png = tmp_path / "test_save_image.png"
    saved_image_path_fits = tmp_path / "test_save_image.fits"
    save_image(image_info, image, tmp_path, "test_save_image")
    assert saved_image_path_png.exists()
    assert saved_image_path_fits.exists()


def test_get_facet_config_list(config_file_odd_facets: Callable, dask_client):
    config = PipelineConfig(config_file_odd_facets())
    swiftly_manager = Swiftly(config)
    facet_config_list = swiftly_manager._get_facet_config_list()
    expected_facet_config_vars = {"off0": -176, "off1": -176, "_mask0": None, "_mask1": None, "size": 352}
    assert len(facet_config_list) == config.swiftly_info.facet_count**2
    assert vars(facet_config_list[0]) == expected_facet_config_vars


def test_swiftly_init(swiftly_manager_odd_facets, dask_client):
    expected_backward_keys = ["config", "facets_config_list", "MNAF_BMNAFs_persist", "_client", "task_queue", "lru"]
    actual_backward_keys = list(vars(swiftly_manager_odd_facets.backward).keys())
    assert actual_backward_keys == expected_backward_keys
    assert all(MNAF_BMNAF is None for MNAF_BMNAF in swiftly_manager_odd_facets.backward.MNAF_BMNAFs_persist)


def test_swiftly_forward_init(swiftly_manager_odd_facets, dask_client):
    expected_forward_keys = ["config", "facet_tasks", "BF_Fs_persist", "_client", "task_queue", "lru"]
    actual_forward_keys = list(vars(swiftly_manager_odd_facets.forward).keys())
    assert actual_forward_keys == expected_forward_keys
    assert swiftly_manager_odd_facets.forward.BF_Fs_persist is None


def test_get_facet_tasks(swiftly_manager_odd_facets, dask_client):
    facets = [np.zeros((16, 16)) for i in range(0, len(swiftly_manager_odd_facets.facet_config_list))]
    swiftly_manager_odd_facets.get_facet_tasks(facets)
    assert swiftly_manager_odd_facets.forward.facet_tasks[0][0] == swiftly_manager_odd_facets.facet_config_list[0]
    assert isinstance(swiftly_manager_odd_facets.forward.facet_tasks[0][1], np.ndarray)


def test_get_subgrid_tasks(swiftly_manager_odd_facets, dask_client):
    subgrid_config_list = [
        SubgridConfig(subgrid_u_offset, subgrid_v_offset, size=10)
        for subgrid_u_offset, subgrid_v_offset in [(0, 0), (0, 1), (1, 0), (1, 1)]
    ]
    facets = [np.zeros((16, 16)) for i in range(0, len(swiftly_manager_odd_facets.facet_config_list))]
    swiftly_manager_odd_facets.get_facet_tasks(facets)
    subgrid_tasks = swiftly_manager_odd_facets._get_subgrid_tasks(subgrid_config_list)
    subgrids = [subgrid_task.compute() for subgrid_task in subgrid_tasks]
    assert len(subgrid_tasks) == len(subgrid_config_list)
    assert isinstance(subgrids[0], np.ndarray)


def test_join_facets(swiftly_manager_odd_facets, dask_client):
    facets = [
        np.array(
            [
                [0.40723793, 0.32148627, 0.44059888, 0.41862874, 0.45710954],
                [0.44633975, 0.41411162, 0.53760405, 0.5331245, 0.50010153],
                [0.35326328, 0.45792268, 0.5743321, 0.54547963, 0.56958197],
                [0.44774286, 0.46599295, 0.48399777, 0.43644114, 0.48012128],
                [0.70015507, 0.65790663, 0.61736625, 0.52972877, 0.5023709],
            ]
        ),
        np.array(
            [
                [0.50163105, 0.38845825, 0.44522339, 0.55900242, 0.43999151],
                [0.48271767, 0.46100534, 0.43076755, 0.47922685, 0.50259037],
                [0.50606935, 0.41187469, 0.3840164, 0.43128861, 0.58197886],
                [0.46441174, 0.38654637, 0.34544132, 0.31078086, 0.44003814],
                [0.5549345, 0.5705728, 0.51667727, 0.38005771, 0.41436995],
            ]
        ),
        np.array(
            [
                [0.74025627, 0.72649156, 0.67629222, 0.58604405, 0.52954584],
                [0.70895514, 0.64543563, 0.53400304, 0.5750082, 0.53028447],
                [0.58944693, 0.50629185, 0.41160496, 0.50451023, 0.51658256],
                [0.42907739, 0.36082565, 0.3487477, 0.40420075, 0.40039851],
                [0.5878911, 0.37837155, 0.34490991, 0.40226806, 0.45775504],
            ]
        ),
        np.array(
            [
                [0.6339702, 0.72768246, 0.66359554, 0.56449016, 0.52924688],
                [0.54995143, 0.67326947, 0.63640407, 0.55826046, 0.51611304],
                [0.48589153, 0.56788177, 0.54524466, 0.44393862, 0.45953441],
                [0.38754086, 0.51877522, 0.57937828, 0.47678645, 0.41042063],
                [0.36796059, 0.39643847, 0.49760147, 0.51427163, 0.50599965],
            ]
        ),
    ]

    full_image = swiftly_manager_odd_facets.join_facets(facets)
    assert full_image.shape == (10, 10)


def test_apply_degrid_corrections(pipeline_config_odd_facets, dask_client):
    swiftly_manager = Swiftly(pipeline_config_odd_facets)
    gridding_manager = Gridder(pipeline_config_odd_facets)
    facets = [
        np.array(
            [
                [0.40723793, 0.32148627, 0.44059888, 0.41862874, 0.45710954],
                [0.44633975, 0.41411162, 0.53760405, 0.5331245, 0.50010153],
                [0.35326328, 0.45792268, 0.5743321, 0.54547963, 0.56958197],
                [0.44774286, 0.46599295, 0.48399777, 0.43644114, 0.48012128],
                [0.70015507, 0.65790663, 0.61736625, 0.52972877, 0.5023709],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.50163105, 0.38845825, 0.44522339, 0.55900242, 0.43999151],
                [0.48271767, 0.46100534, 0.43076755, 0.47922685, 0.50259037],
                [0.50606935, 0.41187469, 0.3840164, 0.43128861, 0.58197886],
                [0.46441174, 0.38654637, 0.34544132, 0.31078086, 0.44003814],
                [0.5549345, 0.5705728, 0.51667727, 0.38005771, 0.41436995],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.74025627, 0.72649156, 0.67629222, 0.58604405, 0.52954584],
                [0.70895514, 0.64543563, 0.53400304, 0.5750082, 0.53028447],
                [0.58944693, 0.50629185, 0.41160496, 0.50451023, 0.51658256],
                [0.42907739, 0.36082565, 0.3487477, 0.40420075, 0.40039851],
                [0.5878911, 0.37837155, 0.34490991, 0.40226806, 0.45775504],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.6339702, 0.72768246, 0.66359554, 0.56449016, 0.52924688],
                [0.54995143, 0.67326947, 0.63640407, 0.55826046, 0.51611304],
                [0.48589153, 0.56788177, 0.54524466, 0.44393862, 0.45953441],
                [0.38754086, 0.51877522, 0.57937828, 0.47678645, 0.41042063],
                [0.36796059, 0.39643847, 0.49760147, 0.51427163, 0.50599965],
            ]
        ).astype(np.complex128),
    ]
    expected_corrected_facets = [
        np.array(
            [
                [0.40723793, 0.32148627, 0.44059888, 0.41862874, 0.45710954],
                [0.44633975, 0.41411162, 0.53760405, 0.5331245, 0.50010153],
                [0.35326328, 0.45792268, 0.5743321, 0.54547963, 0.56958197],
                [0.44774286, 0.46599295, 0.48399777, 0.43644114, 0.48012128],
                [0.70015507, 0.65790663, 0.61736625, 0.52972877, 0.5023709],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.50163105, 0.38845825, 0.44522339, 0.55900242, 0.43999151],
                [0.48271767, 0.46100534, 0.43076755, 0.47922685, 0.50259037],
                [0.50606935, 0.41187469, 0.3840164, 0.43128861, 0.58197886],
                [0.46441174, 0.38654637, 0.34544132, 0.31078086, 0.44003814],
                [0.5549345, 0.5705728, 0.51667727, 0.38005771, 0.41436995],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.74025627, 0.72649156, 0.67629222, 0.58604405, 0.52954584],
                [0.70895514, 0.64543563, 0.53400304, 0.5750082, 0.53028447],
                [0.58944693, 0.50629185, 0.41160496, 0.50451023, 0.51658256],
                [0.42907739, 0.36082565, 0.3487477, 0.40420075, 0.40039851],
                [0.5878911, 0.37837155, 0.34490991, 0.40226806, 0.45775504],
            ]
        ).astype(np.complex128),
        np.array(
            [
                [0.6339702, 0.72768246, 0.66359554, 0.56449016, 0.52924688],
                [0.54995143, 0.67326947, 0.63640407, 0.55826046, 0.51611304],
                [0.48589153, 0.56788177, 0.54524466, 0.44393862, 0.45953441],
                [0.38754086, 0.51877522, 0.57937828, 0.47678645, 0.41042063],
                [0.36796059, 0.39643847, 0.49760147, 0.51427163, 0.50599965],
            ]
        ).astype(np.complex128),
    ]
    actual_corrected_facets = swiftly_manager.apply_degrid_corrections(
        facets, gridding_manager.get_kernel(), normalisation_factor=1
    )
    for actual, expected in zip(actual_corrected_facets, expected_corrected_facets):
        np.testing.assert_allclose(actual, expected)


def test_compute_normalisation_factor(pipeline_config_odd_facets, dask_client):
    swiftly_manager = Swiftly(pipeline_config_odd_facets)
    total_num_visibilities = 100
    channel_count = 2
    facets = [np.full((10, 10), 2)] * 4
    expected_recomputed_value = 100 / (800 * 1)
    actual_recomputed_value = swiftly_manager.compute_normalisation_factor(
        facets, total_num_visibilities, channel_count
    )
    assert actual_recomputed_value == expected_recomputed_value
