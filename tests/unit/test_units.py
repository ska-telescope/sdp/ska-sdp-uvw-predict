"""Unit tests for unit conversions."""

import numpy as np
import pytest

from ska_sdp_distributed_self_cal_prototype.units import (
    amin2asec,
    asec2amin,
    asec2deg,
    asec2rad,
    asec2sky_fraction,
    deg2asec,
    sky_fraction2asec,
)


@pytest.mark.parametrize(
    "value,expected_output",
    [(0.13, 0.00003611111111111111), (1.0, 0.0002777777777777778), (90.0, 0.025), (3600.0, 1.0)],
)
def test_asec2deg(value: float, expected_output: float) -> float:
    output_value = asec2deg(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [(0.13, 0.0021666666666666666), (1.0, 0.016666666666666666), (90.0, 1.5), (3600.0, 60.0)],
)
def test_asec2amin(value: float, expected_output: float) -> float:
    output_value = asec2amin(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [(0.13, 7.800000000000001), (1, 60.0), (90.0, 5400.0), (3600.0, 216000.0)],
)
def test_amin2asec(value: float, expected_output: float) -> float:
    output_value = amin2asec(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [
        (0.13, 4.0123456790123453e-07),
        (1, 3.0864197530864196e-06),
        (90.0, 0.00027777777777777778),
        (3600.0, 0.01111111111111111154),
    ],
)
def test_asec2sky_fraction(value: float, expected_output: float) -> float:
    output_value = asec2sky_fraction(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [
        (324_000.0, np.pi / 2),
        (60.0, 0.0002908882086657216),
        (90.0, 0.0004363323129985824),
        (3600.0, 0.017453292519943295),
    ],
)
def test_asec2rad(value: float, expected_output: float) -> float:
    output_value = asec2rad(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [
        (0.13, 468.0),
        (60.0, 216000.0),
        (90.0, 324000.0),
        (3600.0, 12960000.0),
    ],
)
def test_deg2asec(value: float, expected_output: float) -> float:
    output_value = deg2asec(value)
    assert output_value == expected_output


@pytest.mark.parametrize(
    "value,expected_output",
    [
        (0.13, 42120.00000000001),
        (0.5, 162000.0),
        (1.25, 405000.0),
        (2.0, 648000.0),
    ],
)
def test_sky_fraction2asec(value: float, expected_output: float) -> float:
    output_value = sky_fraction2asec(value)
    assert output_value == expected_output
