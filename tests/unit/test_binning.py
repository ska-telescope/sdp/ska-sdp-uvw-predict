from typing import Callable

import dask.array as da
import numpy as np
import xarray as xr
from ska_sdp_exec_swiftly import SubgridConfig

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import ProcessingSetManager
from ska_sdp_distributed_self_cal_prototype.processing_tasks.binning import (
    _clamp_channels_multiple,
    _clamp_channels_single,
    get_uvw,
    get_visibilities,
    get_visibility_bin_data,
)
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig


def test_get_subgrid_data(xarray_dataset_single_time_frequency_two_polarizations_uvw):
    dataset = xarray_dataset_single_time_frequency_two_polarizations_uvw
    assert isinstance(dataset, xr.Dataset)
    uvw = get_uvw(dataset, flatten=True)
    visibilities = get_visibilities(dataset=dataset, vis_name="VISIBILITY_CORRECTED", stokes_i=True, flatten=True)

    subgrid_conf1 = {"off0": 0, "off1": 0, "size": 50}
    subgrid_info = {
        "subgrid_size_effective": 16,
        "subgrid_size": 16,
        "image_scale_padded": 0.00001,
        "wtower_size": 100,
        "w_step": 5,
        "channel_count": 1,
        "min_frequency": 1.3856e08,
        "channel_width": 200000,
        "vis_name": "VISIBILITY_CORRECTED",
    }
    subgrid_config = SubgridConfig(**subgrid_conf1)

    expected_obs_vis = da.array(
        [
            [-0.41726318 + 0.78393304j],
            [-2.30116296 - 0.35464814j],
            [0.10012066 + 0.11819659j],
            [-0.36911827 + 0.84281284j],
            [-0.14767398 - 0.01379245j],
            [-0.17631984 + 1.04796135j],
            [0.34144032 + 1.05318165j],
            [0.22764248 - 0.06561253j],
        ]
    )
    expected_uvw = np.array(
        [
            [227.9950696, -3957.274282, 499.7688782],
            [-600.701457, -829.06545, 417.8524994],
            [-3145.252773, 7960.660299, 285.9589219],
            [2435.317579, -6165.584319, -222.8748884],
            [2372.913687, -6013.750012, -221.6493144],
            [1442.007355, -3655.21923, -135.2418139],
            [-828.6965266, 3128.208832, -81.9163788],
            [-3304.032482, 8373.590091, 308.6286711],
        ]
    )
    expected_start_channels = np.zeros((8))
    expected_end_channels = np.ones((8))
    output = get_visibility_bin_data(subgrid_config, visibilities, uvw.compute(), subgrid_info)
    assert isinstance(output, VisibilityBin)
    np.testing.assert_array_almost_equal(output.obs_vis, expected_obs_vis)
    np.testing.assert_array_almost_equal(output.uvw, expected_uvw)
    np.testing.assert_array_almost_equal(output.res_vis, output.obs_vis)
    np.testing.assert_array_almost_equal(output.start_channels, expected_start_channels)
    np.testing.assert_array_almost_equal(output.end_channels, expected_end_channels)


def test_clamp_channels_multiple():
    uvws = np.array(
        [
            [-3299.39602603, -4553.40746604, 2285.12722847],
            [5168.9913904, 7126.85465655, -3563.81936237],
            [-2432.62779951, -10123.00481315, 2725.26768753],
            [-22997.31070262, -2338.60610338, 11524.05342813],
            [1521.53630259, -20124.81975371, 2360.66570943],
            [-13569.00559114, -3985.4635225, 7140.40444633],
            [1528.47483643, 2110.21839705, -1061.14968294],
            [-2434.0334826, -3355.51217799, 1676.33344945],
            [-4596.10108746, -5664.05502286, 3084.86732981],
            [356.91704599, -17180.63979748, 2466.86806338],
        ]
    )
    freq0 = 13866000000.0
    dfreq = 2000000.0
    start_chs = np.zeros(10, dtype=np.int32)
    end_chs = np.full(10, 5, dtype=np.int32)
    min_uvw = [1515355.714285714, 1515355.714285714, -1271309600.0]
    max_uvw = [2121498.0, 2121498.0, 1271309600.0]
    start_chs_expected = np.array([0, 37011, 0, 0, 142355, 0, 141677, 0, 0, 629479], dtype=np.int32)
    end_chs_expected = np.array([0, 37011, 0, 0, 142355, 0, 141677, 0, 0, 629479], dtype=np.int32)

    # Act
    start_chs, end_chs = _clamp_channels_multiple(uvws, freq0, dfreq, start_chs, end_chs, min_uvw, max_uvw)

    # Assert
    np.testing.assert_array_almost_equal(start_chs, start_chs_expected)
    np.testing.assert_array_almost_equal(end_chs, end_chs_expected)


def test_fix_bug_pan_300_clamp_channels_single_handles_uvws_close_to_zero():
    """Test bug in _clamp_channels_single using 1d arrays.

    Bug: numpy cannot do element-wise processing on 1d arrays of different size
    Fix: applyied a mask to this calculation
    """
    eta = 1e-3  # This is used to create a mask in _clamp_channels_single()
    value_below_eta = eta / 10  # This is what breaks the test before the fix
    uvws = np.array(
        [
            [-3299.39602603, value_below_eta, 2285.12722847],
            [5168.9913904, 7126.85465655, -3563.81936237],
            [-2432.62779951, -10123.00481315, 2725.26768753],
            [-22997.31070262, -2338.60610338, 11524.05342813],
            [1521.53630259, -20124.81975371, 2360.66570943],
            [-13569.00559114, -3985.4635225, 7140.40444633],
            [1528.47483643, 2110.21839705, -1061.14968294],
            [-2434.0334826, -3355.51217799, 1676.33344945],
            [-4596.10108746, -5664.05502286, 3084.86732981],
            [356.91704599, -17180.63979748, 2466.86806338],
        ]
    )
    freq0 = 13866000000.0
    dfreq = 2000000.0
    start_chs = np.zeros(10, dtype=np.int32)
    end_chs = np.full(10, 5, dtype=np.int32)
    min_uvw = [1515355.714285714, 1515355.714285714, -1271309600.0]
    max_uvw = [2121498.0, 2121498.0, 1271309600.0]
    start_chs_expected = np.array([0, 37011, 0, 0, 142355, 0, 141677, 0, 0, 629479], dtype=np.int32)
    end_chs_expected = np.array([0, 37011, 0, 0, 142355, 0, 141677, 0, 0, 629479], dtype=np.int32)

    for positions, min_position, max_position in zip(uvws.T, min_uvw, max_uvw):
        start_chs, end_chs = _clamp_channels_single(
            positions, freq0, dfreq, start_chs, end_chs, min_position, max_position
        )

    # Assert
    np.testing.assert_array_almost_equal(start_chs, start_chs_expected)
    np.testing.assert_array_almost_equal(end_chs, end_chs_expected)


def test_get_visibilities(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    vis_name = processing_set_manager.vis_name
    dataset = processing_set_manager.get_dataset()
    visibilities = get_visibilities(dataset, vis_name)
    assert isinstance(visibilities, da.Array)
    assert visibilities.shape == (50, 325, 2, 4)


def test_get_visibilities_flattened(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    vis_name = processing_set_manager.vis_name
    dataset = processing_set_manager.get_dataset()
    visibilities = get_visibilities(dataset, vis_name, flatten=True)
    assert isinstance(visibilities, da.Array)
    assert visibilities.shape == (16250, 2, 4)


def test_get_visibilities_stokes_i(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    vis_name = processing_set_manager.vis_name
    dataset = processing_set_manager.get_dataset()
    visibilities = get_visibilities(dataset, vis_name, stokes_i=True)
    assert isinstance(visibilities, da.Array)
    assert visibilities.shape == (50, 325, 2)


def test_get_visibilities_stokes_i_flattened(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    vis_name = processing_set_manager.vis_name
    dataset = processing_set_manager.get_dataset()
    visibilities = get_visibilities(dataset, vis_name, stokes_i=True, flatten=True)
    assert isinstance(visibilities, da.Array)
    assert visibilities.shape == (16250, 2)


def test_get_uvw(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    dataset = processing_set_manager.get_dataset()
    uvw = get_uvw(dataset)
    assert isinstance(uvw, da.Array)
    assert uvw.shape == (50, 325, 3)


def test_get_uvw_flattened(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    dataset = processing_set_manager.get_dataset()
    uvw = get_uvw(dataset, flatten=True)
    assert isinstance(uvw, da.Array)
    assert uvw.shape == (16250, 3)
