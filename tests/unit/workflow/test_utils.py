"""Unit tests for workflow utils."""
import numpy as np
import pytest

from ska_sdp_distributed_self_cal_prototype.workflow.utils import split_square_array


def test_split_square_array_simple_array(monkeypatch):
    # Arrange
    array = np.array([[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]])
    n_subarray = 4
    monkeypatch.setattr(np, "sqrt", lambda x: 2.0)  # Mock np.sqrt
    expected_subarrays = [
        np.array([[0, 1], [4, 5]]),
        np.array([[2, 3], [6, 7]]),
        np.array([[8, 9], [12, 13]]),
        np.array([[10, 11], [14, 15]]),
    ]

    # Act
    subarrays = split_square_array(array, n_subarray)

    # Assert
    for subarray, expected_subarray in zip(subarrays, expected_subarrays):
        np.testing.assert_array_almost_equal(subarray, expected_subarray)


def test_split_square_array_float_array(monkeypatch):
    # Arrange
    array = np.array(
        [
            [0.41842429, 0.64963491, 0.02978584, 0.23014872, 0.48230767, 0.59081502],
            [0.49829931, 0.99299442, 0.82401678, 0.09258494, 0.63645471, 0.41684872],
            [0.15308627, 0.93563589, 0.21985141, 0.27456828, 0.91432468, 0.38264936],
            [0.18809882, 0.89767928, 0.50031593, 0.19349864, 0.72984955, 0.65861209],
            [0.60432246, 0.76801911, 0.48458194, 0.24963847, 0.79189272, 0.19785485],
            [0.93709718, 0.66749788, 0.67436679, 0.36789535, 0.07978371, 0.89923905],
        ]
    )
    n_subarray = 9
    monkeypatch.setattr(np, "sqrt", lambda x: 3.0)  # Mock np.sqrt
    expected_subarrays = [
        np.array([[0.41842429, 0.64963491], [0.49829931, 0.99299442]]),
        np.array([[0.02978584, 0.23014872], [0.82401678, 0.09258494]]),
        np.array([[0.48230767, 0.59081502], [0.63645471, 0.41684872]]),
        np.array([[0.15308627, 0.93563589], [0.18809882, 0.89767928]]),
        np.array([[0.21985141, 0.27456828], [0.50031593, 0.19349864]]),
        np.array([[0.91432468, 0.38264936], [0.72984955, 0.65861209]]),
        np.array([[0.60432246, 0.76801911], [0.93709718, 0.66749788]]),
        np.array([[0.48458194, 0.24963847], [0.67436679, 0.36789535]]),
        np.array([[0.79189272, 0.19785485], [0.07978371, 0.89923905]]),
    ]

    # Act
    subarrays = split_square_array(array, n_subarray)

    # Assert
    for subarray, expected_subarray in zip(subarrays, expected_subarrays):
        np.testing.assert_array_almost_equal(subarray, expected_subarray)


def test_split_square_array_not_2D(monkeypatch):
    # Arrange
    array = np.array([0, 1, 2, 3, 4, 5, 6, 7])
    n_subarray = 4
    monkeypatch.setattr(np, "sqrt", lambda x: 2.0)  # Mock np.sqrt

    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        split_square_array(array, n_subarray)

    assert str(exc_info.value) == "The array must be 2D."


def test_split_square_array_not_square_array(monkeypatch):
    # Arrange
    array = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8], [9, 10, 11]])
    n_subarray = 4
    monkeypatch.setattr(np, "sqrt", lambda x: 2.0)  # Mock np.sqrt

    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        split_square_array(array, n_subarray)

    assert str(exc_info.value) == "The array must be square (number of rows must equal number of columns)."


def test_split_square_array_not_square_n_subarray(monkeypatch):
    # Arrange
    array = np.array([[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]])
    n_subarray = 12
    monkeypatch.setattr(np, "sqrt", lambda x: 3.46)  # Mock np.sqrt

    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        split_square_array(array, n_subarray)

    assert str(exc_info.value) == "n_subarray (12) must be a square number."


def test_split_square_array_cannot_split_equally(monkeypatch):
    # Arrange
    array = np.array([[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]])
    n_subarray = 9
    monkeypatch.setattr(np, "sqrt", lambda x: 3.0)  # Mock np.sqrt

    # Act & Assert
    with pytest.raises(ValueError) as exc_info:
        split_square_array(array, n_subarray)

    assert str(exc_info.value) == "The input array cannot be evenly split into the specified number of subarrays."
