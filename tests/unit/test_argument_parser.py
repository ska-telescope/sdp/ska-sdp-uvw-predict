"""Unit tests for the argument parser module."""

from pathlib import Path

import pytest

from ska_sdp_distributed_self_cal_prototype.workflow.argument_parser import parse_cli_arguments


def test_parse_cli_arguments():
    cli_args = parse_cli_arguments(["--config-file", "some_config.yaml", "--pipeline-name", "some_pipeline"])

    assert cli_args.config_file == Path("some_config.yaml")
    assert cli_args.pipeline_name == "some_pipeline"


def test_parse_cli_arguments_requires_config():
    with pytest.raises(SystemExit):
        _ = parse_cli_arguments(["--pipeline-name", "some_pipeline"])


def test_parse_cli_arguments_requires_pipeline_name():
    with pytest.raises(SystemExit):
        _ = parse_cli_arguments(["--config-file", "some_config.yaml"])
