"""Unit tests for the deconvolution module."""

from typing import Callable

import numpy as np

from ska_sdp_distributed_self_cal_prototype.data_managers.deconvolution import SelfCalibrationManager
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import ProcessingSetManager, VisibilityPartition
from ska_sdp_distributed_self_cal_prototype.processing_tasks.gridding import Gridder
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig, ProcessingSetInfo
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import bin_data


def test_generate_psf(
    xarray_dataset_first_times_first_baselines_two_polarizations, config_file_odd_facets: Callable, dask_client
):
    # Arrange
    dataset = xarray_dataset_first_times_first_baselines_two_polarizations
    dataset["VISIBILITY_CORRECTED"] = dataset["VISIBILITY_CORRECTED"].isel(uvw_label=0)
    dataset.frequency.attrs["channel_width"] = {"data": 1171875.0}
    pipeline_config = PipelineConfig(config_file_odd_facets())

    # Update pipeline_config
    pipeline_config.processing_set_info = ProcessingSetInfo(
        number_datasets=1,
        number_uvw_partitions=1,
        vis_name="VISIBILITY_CORRECTED",
        min_frequency=1.3856e10,
    )
    pipeline_config.swiftly_info.facet_size_effective = 5

    processing_set_manager = ProcessingSetManager(pipeline_config)

    # Update partitions in the processing_set_manager so small dataset is used
    pipeline_config.dataset_key = "first_dataset"
    processing_set_manager.partitions = [VisibilityPartition({"first_dataset": dataset})]
    visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)

    gridding_manager = Gridder(pipeline_config)
    self_calibration_manager = SelfCalibrationManager(pipeline_config)

    expected_psf = np.array(
        [
            [0.99999967, 0.99999981, 0.99999991, 0.99999997, 1.0],
            [0.99999982, 0.99999992, 0.99999998, 1.0, 0.99999999],
            [0.99999992, 0.99999998, 1.0, 0.99999998, 0.99999992],
            [0.99999999, 1.0, 0.99999998, 0.99999992, 0.99999982],
            [1.0, 0.99999997, 0.99999991, 0.99999981, 0.99999967],
        ]
    )
    psf = self_calibration_manager.generate_psf(visibility_bins, binning_info, gridding_manager)
    np.testing.assert_array_almost_equal(psf, expected_psf)
