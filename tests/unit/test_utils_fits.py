"""Unit tests for the utils_fits module."""

import numpy as np
import pytest
from astropy.coordinates import SkyCoord
from astropy.io import fits

from ska_sdp_distributed_self_cal_prototype.processing_tasks.utils_fits import (
    create_image,
    create_image_with_wcs,
    create_wcs,
    save_fits_image,
)


def test_fits_utils(tmp_path):
    # Create a WCS and do some very basic round trip transformations with it to test validity
    wcs = create_wcs(
        width_in_pixels=10,
        height_in_pixels=10,
        pixel_width_in_radians=0.2,
        pixel_height_in_radians=0.2,
        phase_centre=SkyCoord(0.0033, 0.0236, unit="rad", frame="fk5"),
    )

    wx, wy, wz = wcs.pixel_to_world(1, 1, 275.4873922, 64.34676657)
    pc, pp, px, py = wcs.world_to_pixel(wx, wy, wz)
    assert (px, py) == (275.4873922, 64.34676657)

    # Use the same WCS to create an image with wrong number of dimensions and ensure it throws
    with pytest.raises(Exception):
        image1 = create_image_with_wcs(np.arange(100).reshape(10, 10), wcs)

    # Use the same WCS to create an image with valid sized data and perform some basic tests on it
    image1 = create_image_with_wcs(np.arange(100).reshape(1, 1, 10, 10), wcs)

    # Make image and wcs at the same time instead of seperately
    image2 = create_image(np.arange(100).reshape(1, 1, 10, 10), 10, 10, 0.2, 0.2, SkyCoord(0.0033, 0.0236, unit="rad"))

    assert image1 == image2

    # Save the image to disk, load it back and check that it remains equal to existing images
    tmp_fits_file = tmp_path / "image.fits"
    save_fits_image(np.arange(100).reshape(1, 1, 10, 10), wcs, tmp_fits_file)

    imported_image = fits.open(tmp_fits_file)[0]

    assert (imported_image.data == image1.data_vars["pixels"].data).all()
