"""Tests for VisibilityBin class."""

import numpy as np

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin


def test_subgrid_config_init(random_subgrid_data):
    visibility_bin = VisibilityBin(**random_subgrid_data)

    expected_initial_res_vis = random_subgrid_data["vis_data"]

    assert visibility_bin.subgrid_config == random_subgrid_data["subgrid_config"]

    np.testing.assert_array_equal(visibility_bin.res_vis, expected_initial_res_vis)
    np.testing.assert_array_equal(visibility_bin.obs_vis, random_subgrid_data["vis_data"])
    np.testing.assert_array_equal(visibility_bin.uvw, random_subgrid_data["uvw_data"])
    np.testing.assert_array_equal(visibility_bin.start_channels, random_subgrid_data["start_channels"])
    np.testing.assert_array_equal(visibility_bin.end_channels, random_subgrid_data["end_channels"])


def test_update_residual_visibilities(visibility_bin):
    model_vis = np.ones(visibility_bin.obs_vis.shape)
    expected_new_res_vis = visibility_bin.obs_vis - model_vis

    visibility_bin.update_residual_visibilities(model_vis)
    actual_new_res_vis = visibility_bin.res_vis

    np.testing.assert_array_equal(actual_new_res_vis, expected_new_res_vis)


def test_update_residual_visibilities_dask(visibility_bin_dask_vis_data):
    model_vis = np.ones(visibility_bin_dask_vis_data.obs_vis.shape)
    expected_new_res_vis = visibility_bin_dask_vis_data.obs_vis - model_vis

    visibility_bin_dask_vis_data.update_residual_visibilities(model_vis)
    actual_new_res_vis = visibility_bin_dask_vis_data.res_vis

    np.testing.assert_array_equal(actual_new_res_vis, expected_new_res_vis)
