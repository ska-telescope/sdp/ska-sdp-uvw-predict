"""Unit tests for the utils module."""
from typing import Callable

import pytest
from ska_sdp_exec_swiftly import SwiftlyConfig

from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig


def test_get_yaml_config_value_valid(config_file_odd_facets: Callable):
    config = PipelineConfig(config_file_odd_facets())
    wtower_size = config.gridder_info.wtower_size
    assert wtower_size == 100


def test_get_yaml_config_value_missing(config_file_odd_facets: Callable):
    config = PipelineConfig(config_file_odd_facets())
    with pytest.raises(ValueError):
        PipelineConfig._get_yaml_config_value(config, "absent_key")


def test_get_yaml_config_value_missing_with_default(config_file_odd_facets: Callable):
    config = PipelineConfig(config_file_odd_facets())
    shear_u = config.gridder_info.shear_u
    assert shear_u == 0.0


def test_swiftly_info(config_file_odd_facets: Callable):
    config = PipelineConfig(config_file_odd_facets())
    expected_swiftly_config = dict(
        W=11.0,
        fov=1,
        N=1024,
        Nx=64,
        yB_size=352,
        yN_size=512,
        yP_size=512,
        xA_size=448,
        xM_size=512,
    )
    assert expected_swiftly_config == config.swiftly_info.swiftly_config


def test_swiftly_config(config_file_odd_facets: Callable, dask_client):
    config = PipelineConfig(config_file_odd_facets())
    swiftly_config_class = SwiftlyConfig(**config.swiftly_info.swiftly_config)
    assert swiftly_config_class._W == 11.0
    assert swiftly_config_class._fov == 1
    assert swiftly_config_class._yB_size == 352


@pytest.mark.parametrize(
    "pixel_scale,expected_output",
    [
        ("0.7amin", 0.00012962962962962963),
        ("0.439812345asec", 1.3574455092592591e-06),
        ("1deg", 0.011111111111111112),
        ("180deg", 2.0),
        ("0.0asec", 0.0),
        ("324000.0asec", 1.0),
    ],
)
def test_get_pixel_scale_valid(pixel_scale, expected_output):
    converted_value = PipelineConfig._get_pixel_scale(pixel_scale)

    assert converted_value == expected_output


@pytest.mark.parametrize("pixel_scale", ["invalid_string", "0.76trees", "deg", "7e-5asec"])
def test_get_pixel_scale_invalid(pixel_scale):
    with pytest.raises(ValueError) as _:
        PipelineConfig._get_pixel_scale(pixel_scale)
