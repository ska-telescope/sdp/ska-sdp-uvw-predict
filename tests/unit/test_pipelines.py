"""Unit tests for the Swiftly class."""

from pathlib import Path
from typing import Callable

import pytest

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.clean_hogbom import clean_hogbom
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.clean_hogbom_with_degridding import (
    clean_hogbom_with_degridding,
)
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.continuum_imaging_pipeline import continuum_imaging
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.dirty_image_pipeline import dirty_image_pipeline
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.psf_and_dirty_image import psf_and_dirty_image


@pytest.mark.parametrize("parallel_cleaning", [True, False])
def test_clean_hogbom(config_file_even_facets: Callable, parallel_cleaning, dask_client):
    config = config_file_even_facets(parallel_cleaning)
    clean_hogbom(config)
    pipeline_config = PipelineConfig(config)
    dirty_image_out_png = pipeline_config.output_dir / "test_dirty_image.png"
    dirty_image_out_fits = pipeline_config.output_dir / "test_dirty_image.fits"
    psf_out_png = pipeline_config.output_dir / "test_psf_image.png"
    psf_out_fits = pipeline_config.output_dir / "test_psf_image.fits"
    residual_out_png = pipeline_config.output_dir / "test_residual_image.png"
    residual_out_fits = pipeline_config.output_dir / "test_residual_image.fits"
    model_out_png = pipeline_config.output_dir / "test_model_image.png"
    model_out_fit = pipeline_config.output_dir / "test_model_image.fits"

    assert dirty_image_out_png.exists()
    assert dirty_image_out_fits.exists()
    assert psf_out_png.exists()
    assert psf_out_fits.exists()
    assert residual_out_png.exists()
    assert residual_out_fits.exists()
    assert model_out_png.exists()
    assert model_out_fit.exists()


def test_dirty_image_pipeline(config_file_odd_facets: Callable, dask_client):
    config = config_file_odd_facets()
    dirty_image_pipeline(Path(config))
    pipeline_config = PipelineConfig(config)
    dirty_image_out_png = pipeline_config.output_dir / "test_dirty_image.png"
    dirty_image_out_fits = pipeline_config.output_dir / "test_dirty_image.fits"

    assert dirty_image_out_png.exists()
    assert dirty_image_out_fits.exists()


@pytest.mark.parametrize("config_file", ["config_file_odd_facets", "config_file_even_facets"])
def test_psf_and_dirty_image(config_file: Callable, request, dask_client):
    config_file = request.getfixturevalue(config_file)
    psf_and_dirty_image(Path(config_file()))
    pipeline_config = PipelineConfig(config_file())

    dirty_image_out_png = pipeline_config.output_dir / "test_dirty_image.png"
    dirty_image_out_fits = pipeline_config.output_dir / "test_dirty_image.fits"
    psf_out_png = pipeline_config.output_dir / "test_psf_image.png"
    psf_out_fits = pipeline_config.output_dir / "test_psf_image.fits"

    assert dirty_image_out_png.exists()
    assert dirty_image_out_fits.exists()
    assert psf_out_png.exists()
    assert psf_out_fits.exists()


def test_clean_hogbom_with_degridding_pipeline(config_file_even_facets: Callable, dask_client):
    config = config_file_even_facets()
    visibility_bins = clean_hogbom_with_degridding(Path(config))
    assert isinstance(visibility_bins, list)
    assert len(visibility_bins) == 13  # 12/25 subgrids using config_file_even_facets have 0 visibilities
    for visibility_bin in visibility_bins:
        assert isinstance(visibility_bin, VisibilityBin)
        assert visibility_bin.res_vis is not None

    pipeline_config = PipelineConfig(config)
    dirty_image_out_png = pipeline_config.output_dir / "test_dirty_image.png"
    dirty_image_out_fits = pipeline_config.output_dir / "test_dirty_image.fits"
    psf_out_png = pipeline_config.output_dir / "test_psf_image.png"
    psf_out_fits = pipeline_config.output_dir / "test_psf_image.fits"
    residual_out_png = pipeline_config.output_dir / "test_residual_image.png"
    residual_out_fits = pipeline_config.output_dir / "test_residual_image.fits"
    model_out_png = pipeline_config.output_dir / "test_model_image.png"
    model_out_fit = pipeline_config.output_dir / "test_model_image.fits"

    assert dirty_image_out_png.exists()
    assert dirty_image_out_fits.exists()
    assert psf_out_png.exists()
    assert psf_out_fits.exists()
    assert residual_out_png.exists()
    assert residual_out_fits.exists()
    assert model_out_png.exists()
    assert model_out_fit.exists()


def test_continuum_imaging_pipeline(config_file_even_facets: Callable, dask_client):
    config = config_file_even_facets()
    continuum_imaging(Path(config))

    output_dir = PipelineConfig(config).output_dir
    dirty_image_out_png = output_dir / "test_dirty_image.png"
    dirty_image_out_fits = output_dir / "test_dirty_image.fits"
    psf_out_png = output_dir / "test_psf_image.png"
    psf_out_fits = output_dir / "test_psf_image.fits"
    residual_out_png = output_dir / "test_residual_image0.png"
    residual_out_fits = output_dir / "test_residual_image0.fits"
    model_out_png = output_dir / "test_model_image0.png"
    model_out_fit = output_dir / "test_model_image0.fits"

    assert dirty_image_out_png.exists()
    assert dirty_image_out_fits.exists()
    assert psf_out_png.exists()
    assert psf_out_fits.exists()
    assert residual_out_png.exists()
    assert residual_out_fits.exists()
    assert model_out_png.exists()
    assert model_out_fit.exists()
