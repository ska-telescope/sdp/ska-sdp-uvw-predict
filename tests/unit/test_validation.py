"""Module containing unit tests for validation functions."""

import dask.array as da
import numpy as np

from ska_sdp_distributed_self_cal_prototype.processing_tasks.validation import (
    _array_values_all_ge_zero,
    _array_values_all_non_inf,
    _array_values_all_non_nan,
    _log_inf_counts,
    _log_nan_counts,
    _log_negative_counts,
)


def test_array_values_all_ge_zero_ok():
    vis_data = da.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_ge_zero(vis_data)
    assert actual_output == expected_output


def test_array_values_all_ge_zero_zeros_ok():
    vis_data = da.array([[1, 0, 1], [1, 0, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_ge_zero(vis_data)
    assert actual_output == expected_output


def test_array_values_all_ge_zero_negatives_not_ok():
    vis_data = da.array([[1, -1, 1], [1, np.nan, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = False
    actual_output = _array_values_all_ge_zero(vis_data)
    assert actual_output == expected_output


def test_array_values_all_ge_zero_nans_do_not_count_as_negative():
    vis_data = da.array([[1, 1, 1], [1, np.nan, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_ge_zero(vis_data)
    assert actual_output == expected_output


def test_array_values_all_ge_zero_infs_do_not_count_as_negative():
    vis_data = da.array([[1, 1, 1], [1, np.inf, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_ge_zero(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_nan_no_nans_ok():
    vis_data = da.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_non_nan(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_nan_not_ok():
    vis_data = da.array([[1, np.nan, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = False
    actual_output = _array_values_all_non_nan(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_nan_do_not_count_infs():
    vis_data = da.array([[1, np.inf, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_non_nan(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_nan_when_all_values_are_nans():
    vis_data = da.array(
        [[np.nan, np.nan, np.nan], [np.nan, np.nan, np.nan], [np.nan, np.nan, np.nan]], dtype=np.complex128
    )
    expected_output = False
    actual_output = _array_values_all_non_nan(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_inf_no_infs_ok():
    vis_data = da.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = True
    actual_output = _array_values_all_non_inf(vis_data)
    assert actual_output == expected_output


def test_array_values_all_non_inf_not_ok():
    vis_data = da.array([[1, np.inf, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_output = False
    actual_output = _array_values_all_non_inf(vis_data)
    assert actual_output == expected_output


def test_log_negative_counts():
    vis_data = da.array([[1, -1, 1], [-1, -1, np.nan], [1, np.inf, 1]], dtype=np.complex128)
    expected_total = 9
    expected_total_negatives = 3
    actual_total, actual_total_negatives = _log_negative_counts(vis_data)
    assert actual_total == expected_total
    assert actual_total_negatives == expected_total_negatives


def test_log_nan_counts():
    vis_data = da.array([[1, np.nan, 1], [-1, np.nan, 1], [1, 1, 1]], dtype=np.complex128)
    expected_total = 9
    expected_total_nans = 2
    actual_total, actual_total_nans = _log_nan_counts(vis_data)
    assert actual_total == expected_total
    assert actual_total_nans == expected_total_nans


def test_log_nan_counts_all_nan():
    vis_data = da.array(
        [[np.nan, np.nan, np.nan], [np.nan, np.nan, np.nan], [np.nan, np.nan, np.nan]], dtype=np.complex128
    )
    expected_total = 9
    expected_total_nans = 9
    actual_total, actual_total_nans = _log_nan_counts(vis_data)
    assert actual_total == expected_total
    assert actual_total_nans == expected_total_nans


def test_log_nan_counts_no_nans():
    vis_data = da.array([[1, 1, 1], [1, 1, 1], [1, 1, 1]], dtype=np.complex128)
    expected_total = 9
    expected_total_nans = 0
    actual_total, actual_total_nans = _log_nan_counts(vis_data)
    assert actual_total == expected_total
    assert actual_total_nans == expected_total_nans


def test_log_inf_counts():
    vis_data = da.array([[1, np.inf, 1], [-1, np.nan, 1], [1, 1, 1]], dtype=np.complex128)
    expected_total = 9
    expected_total_infs = 1
    actual_total, actual_total_infs = _log_inf_counts(vis_data)
    assert actual_total == expected_total
    assert actual_total_infs == expected_total_infs
