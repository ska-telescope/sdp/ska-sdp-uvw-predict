"""Unit tests for the visibility_partition module."""

import numpy as np
import pytest
import xarray as xr

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import VisibilityPartition

# TODO test line 61 when some frequencies cut off (close to uvw_boundary)


def test_visibility_partition_constructor(
    xarray_dataset_first_times_first_baselines: xr.Dataset,
    xarray_dataset_last_times_first_baselines: xr.Dataset,
    xarray_dataset_first_times_last_baselines: xr.Dataset,
):
    # Arrange
    xds1 = xarray_dataset_first_times_first_baselines
    xds2 = xarray_dataset_last_times_first_baselines
    xds3 = xarray_dataset_first_times_last_baselines

    datasets = {"xds1": xds1, "xds2": xds2, "xds3": xds3}

    bounds_xds1 = np.array(
        [
            [
                [-55.90187105, -51.86647314],
                [-1.62158715, -1.27713439],
                [-31.44451482, -29.07056024],
            ],
            [
                [113.49211091, 122.57451842],
                [119.43971972, 129.0846649],
                [45.18725202, 48.97679843],
            ],
            [
                [128.8398705, 139.12592896],
                [130.93105362, 141.52500866],
                [52.07365705, 56.43480161],
            ],
            [
                [176.34381906, 190.27339357],
                [150.83291542, 163.16999485],
                [75.7883179, 82.1016265],
            ],
            [
                [165.43454098, 178.39368302],
                [120.71753635, 130.70698417],
                [74.25737806, 80.42084458],
            ],
            [
                [100.07282131, 108.35199537],
                [157.00072232, 169.44515663],
                [31.86959323, 34.60310759],
            ],
            [
                [114.12282912, 123.36255045],
                [140.51784524, 151.77260445],
                [42.31187892, 45.88413498],
            ],
            [
                [81.02985229, 87.42961326],
                [68.99958154, 74.64498286],
                [34.62783653, 37.51379628],
            ],
            [
                [116.60134439, 125.72614441],
                [83.27825894, 90.18135844],
                [52.48147369, 56.83654166],
            ],
            [
                [133.10399762, 143.3018958],
                [33.16776084, 36.32960818],
                [69.48775169, 75.18956893],
            ],
        ]
    )

    bounds_xds2 = np.array(
        [
            [
                [13.15627156, 14.45426748],
                [-62.07740687, -57.6222468],
                [6.68773034, 7.23857131],
            ],
            [
                [-132.24432762, -122.56263937],
                [105.94096686, 114.60109837],
                [54.29525614, 58.80880204],
            ],
            [
                [-146.08416805, -135.36821016],
                [121.09447059, 130.97078876],
                [58.9056417, 63.80986399],
            ],
            [
                [-175.43315752, -162.43319816],
                [170.63590218, 184.41772562],
                [63.87532072, 69.24166029],
            ],
            [
                [-146.69910617, -135.71938998],
                [163.56254725, 176.6777903],
                [47.56974929, 51.6118048],
            ],
            [
                [-161.13405616, -149.56908545],
                [84.67051003, 91.83802963],
                [78.52615064, 84.96841548],
            ],
            [
                [-150.55476366, -139.62403532],
                [103.09185477, 111.61616757],
                [66.71691439, 72.2291377],
            ],
            [
                [-80.46936523, -74.50554885],
                [78.2931791, 84.61660286],
                [29.02973035, 31.47069434],
            ],
            [
                [-101.90881206, -94.27125643],
                [115.49562977, 124.74917575],
                [32.38072421, 35.137975],
            ],
            [
                [-63.01929594, -57.92606748],
                [142.36865061, 153.49734498],
                [0.2488313, 0.44681655],
            ],
        ]
    )

    bounds_xds3 = np.array(
        [
            [
                [-12314.83534313, -11400.06582828],
                [4955.87211233, 5387.96704669],
                [-7829.98977006, -7243.06146759],
            ],
            [
                [-4010.60666268, -3699.14346717],
                [-7377.13300941, -6838.24693681],
                [-1104.32481402, -1015.66106172],
            ],
            [
                [8024.62126872, 8640.00199759],
                [1882.9632389, 2064.69492446],
                [4192.61862653, 4536.62655563],
            ],
            [
                [10614.7170153, 11466.84613161],
                [11639.41745687, 12577.20249105],
                [4083.70292222, 4427.26429964],
            ],
            [
                [7673.49192467, 8333.7498154],
                [-12747.41286347, -11810.55362775],
                [6227.40040587, 6725.66495605],
            ],
            [
                [19424.68709701, 20954.83734072],
                [-3361.48773705, -3037.39970365],
                [11435.68009412, 12366.61632569],
            ],
            [
                [22054.82535592, 23738.58699415],
                [6633.02837948, 7243.60272162],
                [11326.76438981, 12257.2540697],
            ],
            [
                [11727.25708674, 12646.85012874],
                [8721.21017571, 9441.82793387],
                [5208.27968825, 5640.95136965],
            ],
            [
                [14313.86048247, 15477.45279429],
                [18477.66439368, 19954.33550046],
                [5099.36398394, 5531.58911366],
            ],
            [
                [2586.60339574, 2830.60266555],
                [9756.45421797, 10512.50756659],
                [-117.21706336, -101.61717752],
            ],
        ]
    )

    uvw_bounds_per_baseline = {
        "xds1": bounds_xds1,
        "xds2": bounds_xds2,
        "xds3": bounds_xds3,
    }

    uvw_bounds_dataset = {
        "xds1": np.array(
            [
                [-55.90187105, -1.62158715, -31.44451482],
                [190.27339357, 169.44515663, 82.1016265],
            ]
        ),
        "xds2": np.array(
            [
                [-175.43315752, -62.07740687, 0.2488313],
                [14.45426748, 184.41772562, 84.96841548],
            ]
        ),
        "xds3": np.array(
            [
                [-12314.83534313, -12747.41286347, -7829.98977006],
                [23738.58699415, 19954.33550046, 12366.61632569],
            ]
        ),
    }

    partition_bounds = np.array(
        [[-12314.83534313, -12747.41286347, -7829.98977006], [23738.58699415, 19954.33550046, 12366.61632569]]
    )

    # Act
    visibility_partition = VisibilityPartition(datasets)

    # Assert
    # Check attribute type
    assert isinstance(visibility_partition.dataset_dict, dict)

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in visibility_partition.dataset_dict.items():
        # Check 'UVW' variable hasn't been modified
        assert xds["UVW"].identical(datasets[key]["UVW"])

        # Check _calculate_uvw_bounds_per_baseline() method is correct
        np.testing.assert_allclose(xds["UVW_bounds"].values, uvw_bounds_per_baseline[key])

        # Check _calculate_dataset_uvw_bounds() method is correct
        np.testing.assert_allclose(xds.dataset_bounds.values, uvw_bounds_dataset[key])


@pytest.mark.parametrize("use_dataset_check", (False, True))
def test_visibility_partition_uvw_selection(example_visibility_partition: VisibilityPartition, use_dataset_check):
    # Arrange
    uvw_region = (-5e3, 1e4, -1e4, 1e4, -5e3, 1e4)

    # xds1 and xds2 should have the same values
    xds1_uvw = example_visibility_partition.dataset_dict["xds1"]["UVW"].values
    xds2_uvw = example_visibility_partition.dataset_dict["xds2"]["UVW"].values

    # Get xds3 values from fixture
    xds3_uvw = np.array(
        [
            [
                [-8073.56482313, -14814.95640472, -2223.06466869],
                [17385.21807696, 4079.41078376, 9132.47088428],
                [5698.15392807, 21137.20742165, -220.15204611],
            ],
            [
                [-8066.97272159, -14818.92552564, -2220.53960175],
                [17386.08246912, 4087.95687779, 9127.00220887],
                [5687.68189731, 21140.00917429, -221.92215582],
            ],
            [
                [-8060.377873, -14822.89140453, -2218.01661451],
                [17386.94093404, 4096.5034015, 9121.53327113],
                [5677.20793226, 21142.80577496, -223.68896579],
            ],
            [
                [-8053.78027942, -14826.85404015, -2215.49570776],
                [17387.79347144, 4105.05035223, 9116.06407276],
                [5666.73203617, 21145.5972228, -225.45247545],
            ],
            [
                [-8047.17994375, -14830.81343077, -2212.9768826],
                [17388.64008096, 4113.59772622, 9110.59461616],
                [5656.25421366, 21148.38351657, -227.21268404],
            ],
            [
                [-8040.57686759, -14834.76957542, -2210.46013965],
                [17389.48076239, 4122.14552141, 9105.12490267],
                [5645.77446728, 21151.1646556, -228.96959113],
            ],
            [
                [-8033.97105371, -14838.72247243, -2207.94547996],
                [17390.31551538, 4130.69373419, 9099.65493457],
                [5635.29280141, 21153.94063872, -230.72319598],
            ],
            [
                [-8027.36250398, -14842.6721207, -2205.43290425],
                [17391.14433969, 4139.24236216, 9094.18471342],
                [5624.80921902, 21156.71146515, -232.4734981],
            ],
            [
                [-8020.75122053, -14846.61851896, -2202.92241331],
                [17391.96723506, 4147.79140256, 9088.71424097],
                [5614.32372348, 21159.47713399, -234.22049692],
            ],
            [
                [-8014.13720613, -14850.56166554, -2200.41400822],
                [17392.78420113, 4156.34085181, 9083.24351952],
                [5603.8363192, 21162.23764409, -235.96419171],
            ],
        ]
    )

    # Get expected dataset_bounds values
    xds1_dataset = example_visibility_partition.dataset_dict["xds1"].dataset_bounds.values
    xds2_dataset = example_visibility_partition.dataset_dict["xds2"].dataset_bounds.values
    xds3_dataset = np.array(
        [[-4010.60666268, -7377.13300941, -1104.32481402], [8640.00199759, 9933.45203654, 4536.62655563]]
    )

    # Get expected UVW_bounds values
    xds1_uvw_bounds = example_visibility_partition.dataset_dict["xds1"].UVW_bounds.values
    xds2_uvw_bounds = example_visibility_partition.dataset_dict["xds2"].UVW_bounds.values
    xds3_uvw_bounds = np.array(
        [
            [[-4010.60666268, -3699.14346717], [-7377.13300941, -6838.24693681], [-1104.32481402, -1015.66106172]],
            [[8024.62126872, 8640.00199759], [1882.9632389, 2064.69492446], [4192.61862653, 4536.62655563]],
            [[2586.60339574, 2674.68590483], [9756.45421797, 9933.45203654], [-110.76045077, -101.61717752]],
        ]
    )

    selected_uvw_values = {"xds1": xds1_uvw, "xds2": xds2_uvw, "xds3": xds3_uvw}
    dataset_bounds = {"xds1": xds1_dataset, "xds2": xds2_dataset, "xds3": xds3_dataset}
    uvw_bounds = {"xds1": xds1_uvw_bounds, "xds2": xds2_uvw_bounds, "xds3": xds3_uvw_bounds}

    vis_dataarray_shapes = {"xds1": (10, 10, 10, 1, 3), "xds2": (10, 10, 10, 1, 3), "xds3": (10, 3, 10, 1, 3)}

    partition_bounds = np.array(
        [[-4010.60666268, -7377.13300941, -1104.32481402], [8640.00199759, 9933.45203654, 4536.62655563]]
    )

    # Act
    sub_visibility_partition = example_visibility_partition.sel(
        method="uvw", uvw_region=uvw_region, use_dataset_check=use_dataset_check
    )

    # Assert

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(sub_visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in sub_visibility_partition.dataset_dict.items():
        # Check the selected UVW values are correct
        np.testing.assert_allclose(selected_uvw_values[key], xds["UVW"].values)

        # Check the selected dataset_bounds are correct
        np.testing.assert_allclose(dataset_bounds[key], xds.dataset_bounds.values)

        # Check the selected UVW_bounds are correct
        np.testing.assert_allclose(uvw_bounds[key], xds.UVW_bounds.values)

        # Check the VISIBILITY data has the correct shape
        assert vis_dataarray_shapes[key] == xds["VISIBILITY"].shape


def test_visibility_partition_uvw_selection_drop_one_dataset(example_visibility_partition: VisibilityPartition):
    # Arrange
    uvw_region = (-1000, 1000, -1000, 1000, -1000, 1000)

    # xds1 and xds2 should have the same values
    xds1_uvw = example_visibility_partition.dataset_dict.get("xds1")["UVW"].values
    xds2_uvw = example_visibility_partition.dataset_dict.get("xds2")["UVW"].values

    selected_uvw_values = {"xds1": xds1_uvw, "xds2": xds2_uvw}
    vis_dataarray_shapes = {"xds1": (10, 10, 10, 1, 3), "xds2": (10, 10, 10, 1, 3)}

    partition_bounds = np.array(
        [[-175.43315752, -62.07740687, -31.44451482], [190.27339357, 184.41772562, 84.96841548]]
    )

    # Act
    sub_visibility_partition = example_visibility_partition.sel(method="uvw", uvw_region=uvw_region)

    # Assert
    # Check that the empty dataset has been dropped
    assert "xds3" not in sub_visibility_partition.dataset_dict

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(sub_visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in sub_visibility_partition.dataset_dict.items():
        # Check the selected UVW values are correct
        np.testing.assert_allclose(selected_uvw_values[key], xds["UVW"].values)

        # Check the VISIBILITY data has the correct shape
        assert vis_dataarray_shapes[key] == xds["VISIBILITY"].shape


@pytest.mark.parametrize("method", ("", "ivalid argument"))
def test_invalid_visibility_partition_sel_method(example_visibility_partition: VisibilityPartition, method):
    # Act and Assert
    with pytest.raises(ValueError):
        example_visibility_partition.sel(method=method)


def test_visibility_partition_sel_no_method(example_visibility_partition: VisibilityPartition):
    # Act and Assert
    with pytest.raises(TypeError):
        example_visibility_partition.sel()


@pytest.mark.parametrize("frequencies", (np.array([1.3837738037109375e08, 1.3954925537109375e08]), slice(1e8, 1.4e8)))
def test_visibility_partition_frequency_selection(example_visibility_partition: VisibilityPartition, frequencies):
    # Arrange
    expected_frequencies = np.array([1.3837738037109375e08, 1.3954925537109375e08])

    expected_uvw_bounds_xds1 = np.array(
        [
            [[-52.38276639, -51.86647314], [-1.51950587, -1.27713439], [-29.46503655, -29.07056024]],
            [[113.49211091, 114.85827295], [119.43971972, 120.95859618], [45.18725202, 45.89363723]],
            [[128.8398705, 130.36774796], [130.93105362, 132.61580208], [52.07365705, 52.88214819]],
            [[176.34381906, 178.29540476], [150.83291542, 152.89820468], [75.7883179, 76.93320886]],
            [[165.43454098, 167.16353939], [120.71753635, 122.47878807], [74.25737806, 75.35823462]],
            [[100.07282131, 101.53107856], [157.00072232, 158.77833584], [31.86959323, 32.42479128]],
            [[114.12282912, 115.59669722], [140.51784524, 142.2182967], [42.31187892, 42.99566148]],
            [[81.02985229, 81.92579105], [68.99958154, 69.94597186], [34.62783653, 35.15224786]],
            [[116.60134439, 117.81149947], [83.27825894, 84.50430984], [52.48147369, 53.25859812]],
            [[133.10399762, 134.28083157], [33.16776084, 34.04260614], [69.48775169, 70.45627543]],
        ]
    )

    expected_uvw_bounds_xds2 = np.array(
        [
            [[13.15627156, 13.54435017], [-58.16954319, -57.6222468], [6.68773034, 6.78289264]],
            [[-123.9193535, -122.56263937], [105.94096686, 107.38679137], [54.29525614, 55.10670181]],
            [[-136.8879557, -135.36821016], [121.09447059, 122.72598578], [58.9056417, 59.79293959]],
            [[-164.3893833, -162.43319816], [170.63590218, 172.80835968], [63.87532072, 64.88279636]],
            [[-137.46418257, -135.71938998], [163.56254725, 165.55566463], [47.56974929, 48.36276609]],
            [[-150.99043132, -149.56908545], [84.67051003, 86.05669116], [78.52615064, 79.61952927]],
            [[-141.07712078, -139.62403532], [103.09185477, 104.58976634], [66.71691439, 67.68220769]],
            [[-75.40370083, -74.50554885], [78.2931791, 79.28986377], [29.02973035, 29.48956804]],
            [[-95.49350308, -94.27125643], [115.49562977, 116.89603242], [32.38072421, 32.925988]],
            [[-59.05213896, -57.92606748], [142.36865061, 143.83446229], [0.2488313, 0.41868879]],
        ]
    )

    expected_uvw_bounds_xds3 = np.array(
        [
            [[-11539.59842115, -11400.06582828], [4955.87211233, 5048.78662953], [-7337.08044571, -7243.06146759]],
            [[-3758.13309906, -3699.14346717], [-6912.73168142, -6838.24693681], [-1034.80594951, -1015.66106172]],
            [[8024.62126872, 8096.10121712], [1882.9632389, 1934.71935488], [4192.61862653, 4251.0392693]],
            [[10614.7170153, 10744.99137252], [11639.41745687, 11785.44917283], [4083.70292222, 4148.56152751]],
            [[7673.49192467, 7809.12805836], [-11944.944553, -11810.55362775], [6227.40040587, 6302.27449621]],
            [[19424.68709701, 19635.69963827], [-3149.87716054, -3037.39970365], [11435.68009412, 11588.11971502]],
            [[22054.82535592, 22244.20817375], [6633.02837948, 6787.60732083], [11326.76438981, 11485.64197322]],
            [[11727.25708674, 11850.71238971], [8721.21017571, 8847.4510363], [5208.27968825, 5285.84521881]],
            [[14313.86048247, 14503.12447158], [18477.66439368, 18698.18085426], [5099.36398394, 5183.36747702]],
            [[2586.60339574, 2652.41208187], [9756.45421797, 9850.72981796], [-109.83807754, -101.61717752]],
        ]
    )

    expected_dataset_bounds_xds1 = np.array(
        [[-52.38276639, -1.51950587, -29.46503655], [178.29540476, 158.77833584, 76.93320886]]
    )

    expected_dataset_bounds_xds2 = np.array(
        [[-164.3893833, -58.16954319, 0.2488313], [13.54435017, 172.80835968, 79.61952927]]
    )

    expected_dataset_bounds_xds3 = np.array(
        [[-11539.59842115, -11944.944553, -7337.08044571], [22244.20817375, 18698.18085426, 11588.11971502]]
    )

    expected_uvw_bounds = {
        "xds1": expected_uvw_bounds_xds1,
        "xds2": expected_uvw_bounds_xds2,
        "xds3": expected_uvw_bounds_xds3,
    }

    expected_dataset_bounds = {
        "xds1": expected_dataset_bounds_xds1,
        "xds2": expected_dataset_bounds_xds2,
        "xds3": expected_dataset_bounds_xds3,
    }

    partition_bounds = np.array(
        [[-11539.59842115, -11944.944553, -7337.08044571], [22244.20817375, 18698.18085426, 11588.11971502]]
    )

    # Act
    sub_visibility_partition = example_visibility_partition.sel(method="frequency", condition=frequencies)

    # Assert

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(sub_visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in sub_visibility_partition.dataset_dict.items():
        # Check frequency coordinate
        np.testing.assert_allclose(expected_frequencies, xds.frequency.values)

        # Check UVW_bounds DataArray
        np.testing.assert_allclose(expected_uvw_bounds[key], xds["UVW_bounds"].values)

        # Check dataset_bounds attribute
        np.testing.assert_allclose(expected_dataset_bounds[key], xds.dataset_bounds.values)


@pytest.mark.parametrize(
    "times", (np.array([1.5138402640055599e9, 1.5138402720166807e9]), slice(1.51384020e9, 1.51384028e9))
)
def test_visibility_partition_time_selection(example_visibility_partition: VisibilityPartition, times):
    # Arrange
    expected_times = np.array([1.5138402640055599e9, 1.5138402720166807e9])

    uvw_values_xds1 = np.array(
        [
            [
                [-112.36791324, -2.76689194, -63.29948311],
                [246.74903409, 258.7643086, 98.59290382],
                [280.06790506, 283.66010606, 113.60626145],
                [383.03047552, 326.77718236, 165.27494701],
                [359.11598545, 261.5326786, 161.89144347],
                [218.11833768, 340.13964079, 69.65789859],
                [248.33538454, 304.42974208, 92.36720758],
                [176.00046812, 149.48652804, 75.51726995],
                [253.09342506, 180.42106216, 114.41498557],
                [288.36775569, 71.85744174, 151.36060696],
            ],
            [
                [-112.38645887, -2.82212765, -63.26411019],
                [246.65268903, 258.88560918, 98.51551281],
                [279.96402908, 283.79778561, 113.51840891],
                [382.92162886, 326.9654755, 165.15472799],
                [359.038185, 261.70921442, 161.77867984],
                [217.97284357, 340.24687002, 69.5896128],
                [248.21460714, 304.55182378, 92.28936856],
                [175.95064732, 149.57304823, 75.46202922],
                [253.04061286, 180.54547911, 114.335509],
                [288.37999265, 71.99919499, 151.26990301],
            ],
        ]
    )

    uvw_values_xds3 = np.array(
        [
            [
                [-24698.06659772, 10846.26463652, -15762.1864446],
                [-8073.56482313, -14814.95640472, -2223.06466869],
                [17385.21807696, 4079.41078376, 9132.47088428],
                [23083.37200503, 25216.61820541, 8912.31883817],
                [16624.50177458, -25661.22104124, 13539.12177592],
                [42083.28467468, -6766.85385276, 24894.65732888],
                [47781.43860275, 14370.35356889, 24674.50528277],
                [25458.7829001, 18894.36718848, 11355.53555296],
                [31156.93682816, 40031.57461013, 11135.38350686],
                [5698.15392807, 21137.20742165, -220.15204611],
            ],
            [
                [-24708.361769, 10834.12441797, -15754.39870428],
                [-8066.97272159, -14818.92552564, -2220.53960175],
                [17386.08246912, 4087.95687779, 9127.00220887],
                [23073.76436643, 25227.96605208, 8905.08005305],
                [16641.38904741, -25653.04994361, 13533.85910253],
                [42094.44423812, -6746.16754018, 24881.40091315],
                [47782.12613543, 14393.84163411, 24659.47875733],
                [25453.05519071, 18906.88240343, 11347.54181062],
                [31140.73708802, 40046.89157771, 11125.6196548],
                [5687.68189731, 21140.00917429, -221.92215582],
            ],
        ]
    )

    expected_uvw_bounds_xds1 = np.array(
        [
            [[-55.82885511, -51.86647314], [-1.40191405, -1.27713439], [-31.44451482, -29.20127443]],
            [[113.84927158, 122.57451842], [119.43971972, 128.60345731], [45.47252015, 48.97679843]],
            [[129.22502854, 139.12592896], [130.93105362, 140.97877638], [52.39751578, 56.43480161]],
            [[176.74791502, 190.27339357], [150.83291542, 162.42266499], [76.23166629, 82.1016265]],
            [[165.72386052, 178.39368302], [120.71753635, 130.00610536], [74.67335925, 80.42084458]],
            [[100.61130719, 108.35199537], [157.00072232, 169.02030191], [32.12098258, 34.60310759]],
            [[114.57021746, 123.36255045], [140.51784524, 151.28850766], [42.59867357, 45.88413498]],
            [[81.21481712, 87.42961326], [68.99958154, 74.3015851], [34.83155644, 37.51379628]],
            [[116.79779194, 125.72614441], [83.27825894, 89.68738312], [52.7746706, 56.83654166]],
            [[133.10399762, 143.25502368], [33.16776084, 35.76616494], [69.82274687, 75.18956893]],
        ]
    )

    expected_uvw_bounds_xds3 = np.array(
        [
            [[-12274.07254484, -11400.06582828], [5000.78542861, 5387.96704669], [-7829.98977006, -7271.87213636]],
            [[-4010.60666268, -3723.53113946], [-7361.41750873, -6838.24693681], [-1104.32481402, -1024.95057798]],
            [[8024.62126872, 8636.67285964], [1882.9632389, 2030.7246489], [4212.81664232, 4536.62655563]],
            [[10650.32486016, 11466.84613161], [11639.41745687, 12532.19004873], [4110.38242242, 4427.26429964]],
            [[7673.49192467, 8266.74056032], [-12747.41286347, -11840.86442136], [6246.92155838, 6725.66495605]],
            [[19424.68709701, 20910.74540448], [-3361.48773705, -3113.87750707], [11484.68877868, 12366.61632569]],
            [[22054.82535592, 23736.1460066], [6633.02837948, 7150.25375084], [11382.25455877, 12257.2540697]],
            [[11748.55139195, 12646.85012874], [8721.21017571, 9392.14215762], [5237.7672203, 5640.95136965]],
            [[14373.85599962, 15477.45279429], [18477.66439368, 19893.60755746], [5135.33300039, 5531.58911366]],
            [[2625.30460768, 2830.60266555], [9756.45421797, 10501.46539984], [-110.24157187, -101.61717752]],
        ]
    )

    expected_dataset_bounds_xds1 = np.array(
        [[-55.82885511, -1.40191405, -31.44451482], [190.27339357, 169.02030191, 82.1016265]]
    )

    expected_dataset_bounds_xds3 = np.array(
        [[-12274.07254484, -12747.41286347, -7829.98977006], [23736.1460066, 19893.60755746, 12366.61632569]]
    )

    expected_uvw_values = {"xds1": uvw_values_xds1, "xds3": uvw_values_xds3}

    expected_uvw_bounds = {
        "xds1": expected_uvw_bounds_xds1,
        "xds3": expected_uvw_bounds_xds3,
    }

    expected_dataset_bounds = {
        "xds1": expected_dataset_bounds_xds1,
        "xds3": expected_dataset_bounds_xds3,
    }

    partition_bounds = np.array(
        [[-12274.07254484, -12747.41286347, -7829.98977006], [23736.1460066, 19893.60755746, 12366.61632569]]
    )

    # Act
    sub_visibility_partition = example_visibility_partition.sel(method="time", condition=times)

    # Assert
    # Check xds2 has been dropped
    assert "xds2" not in sub_visibility_partition.dataset_dict

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(sub_visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in sub_visibility_partition.dataset_dict.items():
        # Check time coordinate
        np.testing.assert_allclose(expected_times, xds.time.values)

        # Check UVW DataArray
        np.testing.assert_allclose(expected_uvw_values[key], xds["UVW"].values)

        # Check UVW_bounds DataArray
        np.testing.assert_allclose(expected_uvw_bounds[key], xds["UVW_bounds"].values)

        # Check dataset_bounds attribute
        np.testing.assert_allclose(expected_dataset_bounds[key], xds.dataset_bounds.values)


@pytest.mark.parametrize("baseline_ids", (np.array([0, 2]), slice(None, 3, 2)))
def test_visibility_partition_baseline_id_selection(example_visibility_partition: VisibilityPartition, baseline_ids):
    # Arrange
    expected_baseline_ids = np.array([0, 2])

    uvw_values_xds1 = np.array(
        [
            [[-112.36791324, -2.76689194, -63.29948311], [280.06790506, 283.66010606, 113.60626145]],
            [[-112.38645887, -2.82212765, -63.26411019], [279.96402908, 283.79778561, 113.51840891]],
            [[-112.40496618, -2.87737252, -63.22873149], [279.86005771, 283.93541411, 113.43058922]],
            [[-112.42343516, -2.93262652, -63.19334702], [279.75599097, 284.07299152, 113.34280242]],
            [[-112.44186581, -2.98788964, -63.15795681], [279.65182893, 284.21051778, 113.25504855]],
            [[-112.46025812, -3.04316186, -63.12256086], [279.54757159, 284.34799285, 113.16732762]],
            [[-112.47861209, -3.09844315, -63.08715918], [279.44321901, 284.48541667, 113.07963966]],
            [[-112.4969277, -3.1537335, -63.05175179], [279.33877122, 284.62278922, 112.99198472]],
            [[-112.51520495, -3.20903289, -63.01633869], [279.23422824, 284.76011044, 112.90436281]],
            [[-112.53344384, -3.26434131, -62.98091991], [279.12959012, 284.89738027, 112.81677397]],
        ]
    )

    uvw_values_xds2 = np.array(
        [
            [[28.50285921, -124.96512637, 14.57162958], [-293.27313719, 263.65117348, 127.61816322]],
            [[28.56893756, -124.95109882, 14.56251658], [-293.36265785, 263.506697, 127.71077274]],
            [[28.63500613, -124.93703876, 14.55338278], [-293.45207824, 263.3621764, 127.80341029]],
            [[28.7010649, -124.92294617, 14.54422821], [-293.54139833, 263.21761172, 127.89607581]],
            [[28.76711383, -124.90882107, 14.53505286], [-293.63061809, 263.07300301, 127.98876928]],
            [[28.83315291, -124.89466345, 14.52585673], [-293.7197375, 262.92835033, 128.08149067]],
            [[28.89918212, -124.88047334, 14.51663983], [-293.80875652, 262.78365372, 128.17423996]],
            [[28.96520144, -124.86625072, 14.50740216], [-293.89767512, 262.63891323, 128.2670171]],
            [[29.03121083, -124.8519956, 14.49814372], [-293.98649327, 262.49412891, 128.35982206]],
            [[29.09721029, -124.837708, 14.48886452], [-294.07521095, 262.34930081, 128.45265482]],
        ]
    )

    expected_uvw_bounds_xds1 = np.array(
        [
            [[-55.90187105, -51.86647314], [-1.62158715, -1.27713439], [-31.44451482, -29.07056024]],
            [[128.8398705, 139.12592896], [130.93105362, 141.52500866], [52.07365705, 56.43480161]],
        ]
    )

    expected_uvw_bounds_xds2 = np.array(
        [
            [[13.15627156, 14.45426748], [-62.07740687, -57.6222468], [6.68773034, 7.23857131]],
            [[-146.08416805, -135.36821016], [121.09447059, 130.97078876], [58.9056417, 63.80986399]],
        ]
    )

    expected_dataset_bounds_xds1 = np.array(
        [[-55.90187105, -1.62158715, -31.44451482], [139.12592896, 141.52500866, 56.43480161]]
    )

    expected_dataset_bounds_xds2 = np.array(
        [[-146.08416805, -62.07740687, 6.68773034], [14.45426748, 130.97078876, 63.80986399]]
    )

    expected_uvw_values = {"xds1": uvw_values_xds1, "xds2": uvw_values_xds2}

    expected_uvw_bounds = {
        "xds1": expected_uvw_bounds_xds1,
        "xds2": expected_uvw_bounds_xds2,
    }

    expected_dataset_bounds = {
        "xds1": expected_dataset_bounds_xds1,
        "xds2": expected_dataset_bounds_xds2,
    }

    partition_bounds = np.array(
        [[-146.08416805, -62.07740687, -31.44451482], [139.12592896, 141.52500866, 63.80986399]]
    )

    # Act
    sub_visibility_partition = example_visibility_partition.sel(method="baseline_id", condition=baseline_ids)

    # Assert
    # Check xds3 has been dropped
    assert "xds3" not in sub_visibility_partition.dataset_dict

    # Check _calculate_partition_uvw_bounds() method is correct
    np.testing.assert_allclose(sub_visibility_partition.partition_bounds.values, partition_bounds)

    for key, xds in sub_visibility_partition.dataset_dict.items():
        # Check baseline_id coordinate
        np.testing.assert_allclose(expected_baseline_ids, xds.baseline_id.values)

        # Check UVW DataArray
        np.testing.assert_allclose(expected_uvw_values[key], xds["UVW"].values)

        # Check UVW_bounds DataArray
        np.testing.assert_allclose(expected_uvw_bounds[key], xds["UVW_bounds"].values)

        # Check dataset_bounds attribute
        np.testing.assert_allclose(expected_dataset_bounds[key], xds.dataset_bounds.values)


def test_visibility_partition_uvw_region_outside_dataset_bounds(example_visibility_partition: VisibilityPartition):
    # Arrange
    uvw_region = (-12500, 10000, 10000, 20000, 7500, 12500)

    # Act
    sub_visibility_partition = example_visibility_partition.sel(method="uvw", uvw_region=uvw_region)

    # Assert
    # Check all datasets dropped
    assert len(sub_visibility_partition.dataset_dict) == 0
