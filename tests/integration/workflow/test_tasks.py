from typing import Callable

import numpy as np
import pytest
from ska_sdp_func_python.image.cleaners import hogbom as hogbom_original

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import (
    bin_data,
    configure_and_setup_pipeline,
    distributed_hogbom,
    generate_clean_beam_array,
    generate_clean_beam_parameters,
    generate_facets_with_corrections,
    grid_visibilities,
    hogbom,
    initialise_self_calibration,
    load_data,
)


def test_bin_data(pipeline_config_odd_facets, dask_client):
    processing_set_manager, _ = load_data(pipeline_config_odd_facets)
    visibility_bins, _ = bin_data(processing_set_manager, dask_client)
    assert len(visibility_bins) == 7
    for item in visibility_bins:
        assert isinstance(item, VisibilityBin)


def test_generate_clean_beam_parameters_3C123(pipeline_config_odd_facets_with_image_info, psf_3C123):
    # Arrange
    pipeline_config = pipeline_config_odd_facets_with_image_info
    expected_clean_beam_parameters = {
        "sigma_x": 11.71303035318,
        "sigma_y": 11.30181207353,
        "position_angle": -1645.32249207562,
    }

    # Act
    clean_beam_parameters = generate_clean_beam_parameters(pipeline_config, psf_3C123)

    # Assert
    for key in clean_beam_parameters:
        assert (
            pytest.approx(clean_beam_parameters[key], rel=1e-12) == expected_clean_beam_parameters[key]
        )  # relative tolerance for ~3 decimals


def test_generate_clean_beam_array_3C123(pipeline_config_odd_facets_with_image_info, psf_3C123):
    # Arrange
    pipeline_config = pipeline_config_odd_facets_with_image_info
    expected_clean_beam = np.array(
        [
            [
                0.60945375,
                0.64770506,
                0.68254402,
                0.71318287,
                0.73890394,
                0.75908763,
                0.77323712,
                0.78099873,
                0.78217658,
                0.77674084,
                0.76482896,
                0.7467399,
                0.72292166,
                0.69395286,
                0.66051937,
            ],
            [
                0.64948147,
                0.69007961,
                0.72702356,
                0.75947699,
                0.78667908,
                0.8079741,
                0.82283759,
                0.8308979,
                0.83195157,
                0.82597192,
                0.81311012,
                0.79368889,
                0.76818901,
                0.73722953,
                0.70154286,
            ],
            [
                0.68602332,
                0.72873096,
                0.76756013,
                0.8016309,
                0.83014381,
                0.85241106,
                0.86788396,
                0.8761755,
                0.87707633,
                0.87056365,
                0.8568021,
                0.83613684,
                0.80907923,
                0.77628571,
                0.73853142,
            ],
            [
                0.71821937,
                0.76274849,
                0.80319768,
                0.8386493,
                0.86827077,
                0.89134703,
                0.9073092,
                0.91575787,
                0.9164797,
                0.90945642,
                0.89486554,
                0.87307294,
                0.84461759,
                0.81018942,
                0.77060152,
            ],
            [
                0.74528341,
                0.79130079,
                0.83306444,
                0.86962585,
                0.90012565,
                0.92382707,
                0.94014552,
                0.94867253,
                0.94919277,
                0.94169306,
                0.92636292,
                0.90358666,
                0.8739273,
                0.8381035,
                0.7969606,
            ],
            [
                0.76653486,
                0.81366935,
                0.85640828,
                0.89377995,
                0.92490517,
                0.94903156,
                0.96556376,
                0.9740878,
                0.9743884,
                0.96645793,
                0.95049679,
                0.92690496,
                0.89626535,
                0.85931987,
                0.81693961,
            ],
            [
                0.78142711,
                0.82927854,
                0.87262818,
                0.91048938,
                0.94197069,
                0.96631059,
                0.98290817,
                0.99134768,
                0.99141595,
                0.98311122,
                0.96664333,
                0.94242481,
                0.91105378,
                0.87328936,
                0.83002117,
            ],
            [
                0.78957094,
                0.83772025,
                0.88129989,
                0.91931696,
                0.95087555,
                0.97521178,
                0.9917245,
                1.0,
                0.99982918,
                0.99121636,
                0.97437912,
                0.94973912,
                0.91790457,
                0.87964531,
                0.83586184,
            ],
            [
                0.79075136,
                0.83877158,
                0.88219444,
                0.92002955,
                0.95138453,
                0.97549994,
                0.99177979,
                0.99981608,
                0.99940571,
                0.99055909,
                0.97349964,
                0.94865447,
                0.91663654,
                0.87821961,
                0.8343071,
            ],
            [
                0.78493709,
                0.83240468,
                0.8752881,
                0.91260825,
                0.94348414,
                0.96716744,
                0.98307257,
                0.99080079,
                0.99015676,
                0.98115681,
                0.96402819,
                0.9391996,
                0.90728329,
                0.86905003,
                0.82539815,
            ],
            [
                0.77228191,
                0.81878793,
                0.86076351,
                0.89724927,
                0.92738322,
                0.95043451,
                0.96583294,
                0.97319234,
                0.97232667,
                0.96325787,
                0.9462149,
                0.92162416,
                0.89009174,
                0.8523786,
                0.8093701,
            ],
            [
                0.75311794,
                0.79827855,
                0.83900159,
                0.87435531,
                0.90350378,
                0.9257396,
                0.94051248,
                0.94745182,
                0.94638218,
                0.93733066,
                0.92052576,
                0.89638776,
                0.86551129,
                0.82864098,
                0.78664164,
            ],
            [
                0.72794108,
                0.77140705,
                0.81056496,
                0.84451798,
                0.87246261,
                0.89372025,
                0.90776456,
                0.91424311,
                0.9129921,
                0.90404322,
                0.88762233,
                0.86413997,
                0.83417433,
                0.79844755,
                0.75779689,
            ],
            [
                0.69738978,
                0.73885439,
                0.77617381,
                0.80849244,
                0.83504483,
                0.85518577,
                0.86841636,
                0.87440448,
                0.87299871,
                0.86423466,
                0.84833348,
                0.82569258,
                0.79686915,
                0.76255731,
                0.72356045,
            ],
            [
                0.6622181,
                0.70142336,
                0.73667555,
                0.76716563,
                0.79217087,
                0.81108328,
                0.82343416,
                0.8289134,
                0.82738242,
                0.81888002,
                0.80362068,
                0.78198565,
                0.75450708,
                0.72184624,
                0.68476717,
            ],
        ]
    )

    # Act
    clean_beam = generate_clean_beam_array(pipeline_config, psf_3C123)

    # Assert
    np.testing.assert_array_almost_equal(clean_beam, expected_clean_beam)


# This test doesn't follow best practices so it needs to be changed.
@pytest.mark.parametrize("parallel_cleaning", [True, False])
def test_refactored_distributed_hogbom(config_file_even_facets: Callable, parallel_cleaning, dask_client):
    config = config_file_even_facets(parallel_cleaning)
    pipeline_config = PipelineConfig(config)

    (
        pipeline_config,
        processing_set_manager,
        swiftly_manager,
        gridding_manager,
        dask_client,
    ) = configure_and_setup_pipeline(config.absolute())
    visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)
    deconvolution_config = pipeline_config.get_deconvolution_parameters()
    self_calibration_manager = initialise_self_calibration(pipeline_config)
    psf_image = self_calibration_manager.generate_psf(
        visibility_bins,
        binning_info,
        gridding_manager,
    )
    swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)

    residual_image_facets, _ = generate_facets_with_corrections(
        swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
    )
    residual_image = swiftly_manager.join_facets(residual_image_facets)

    if deconvolution_config.parallel_cleaning:
        model_facets = len(residual_image_facets) * [
            np.zeros(residual_image_facets[0].shape, residual_image_facets[0].dtype)
        ]

        residual_image_facets_first_run, model_facets_first_run = distributed_hogbom(
            dask_client,
            residual_image_facets,
            psf_image,
            model_facets,
            gain=deconvolution_config.gain,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )

        model_image_first_run = swiftly_manager.join_facets(model_facets_first_run)

        residual_image_facets_second_run, model_facets_second_run = distributed_hogbom(
            dask_client,
            residual_image_facets_first_run,
            psf_image,
            model_facets_first_run,
            gain=deconvolution_config.gain,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )
        model_image_second_run = swiftly_manager.join_facets(model_facets_second_run)
        np.testing.assert_equal(np.any(np.not_equal(model_image_first_run, model_image_second_run)), True)

    else:
        model_image = np.zeros(residual_image.shape)

        residual_first_run, model_first_run = hogbom(
            residual_image,
            psf_image,
            model_image,
            window=True,
            gain=deconvolution_config.gain,
            thresh=0,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )

        residual_second_run, model_second_run = hogbom(
            residual_first_run,
            psf_image,
            model_first_run,
            window=True,
            gain=deconvolution_config.gain,
            thresh=0,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )

        np.testing.assert_equal(np.any(np.not_equal(model_first_run, model_second_run)), True)


# This test doesn't follow best practices so it needs to be changed.
def test_modified_hogbom_func_against_original(config_file_even_facets: Callable, dask_client):
    config = config_file_even_facets(False)
    pipeline_config = PipelineConfig(config)

    (
        pipeline_config,
        processing_set_manager,
        swiftly_manager,
        gridding_manager,
        dask_client,
    ) = configure_and_setup_pipeline(config.absolute())
    visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)

    deconvolution_config = pipeline_config.get_deconvolution_parameters()
    self_calibration_manager = initialise_self_calibration(pipeline_config)
    psf_image = self_calibration_manager.generate_psf(
        visibility_bins,
        binning_info,
        gridding_manager,
    )
    swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)
    residual_image_facets, _ = generate_facets_with_corrections(
        swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
    )
    residual_image = swiftly_manager.join_facets(residual_image_facets)

    model_image_refactored = np.zeros(residual_image.shape)
    model_image_original = np.zeros(residual_image.shape)

    residual_image_refactored, model_image_refactored = hogbom(
        residual_image,
        psf_image,
        model_image_refactored,
        window=True,
        gain=deconvolution_config.gain,
        thresh=0,
        niter=deconvolution_config.niter,
        fracthresh=deconvolution_config.fracthresh,
    )

    model_image_original, residual_image_original = hogbom_original(
        residual_image,
        psf_image,
        window=True,
        gain=deconvolution_config.gain,
        thresh=0,
        niter=deconvolution_config.niter,
        fracthresh=deconvolution_config.fracthresh,
    )

    np.testing.assert_array_equal(model_image_original, model_image_refactored)
    np.testing.assert_array_equal(residual_image_original, residual_image_refactored)
