"Tests for ProcessingSetManager class"

from typing import Callable

import numpy as np
import xarray as xr

from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import ProcessingSetManager
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import load_data


def test_ProcessingSetManager(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    assert processing_set_manager._vis_name == "VISIBILITY_CORRECTED"


def test_get_dataset(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    dataset = processing_set_manager.get_dataset()
    assert isinstance(dataset, xr.Dataset)
    assert list(dataset.keys()) == [
        "EFFECTIVE_INTEGRATION_TIME",
        "FLAG",
        "TIME_CENTROID",
        "UVW",
        "VISIBILITY",
        "VISIBILITY_CORRECTED",
        "VISIBILITY_MODEL",
        "WEIGHT",
        "UVW_bounds",
    ]


def test_get_frequencies(config_file_odd_facets: Callable):
    pipeline_config = PipelineConfig(config_file_odd_facets())
    processing_set_manager = ProcessingSetManager(pipeline_config)
    frequencies = processing_set_manager.get_frequencies()
    assert isinstance(frequencies, np.ndarray)
    assert frequencies.shape == (2,)


def test_add_subgrid_config_list(config_file_odd_facets: Callable, dask_client):
    config = PipelineConfig(config_file_odd_facets())
    processing_set_manager, config = load_data(config)

    actual_subgrid_config_list = processing_set_manager._subgrid_config_list
    assert len(actual_subgrid_config_list) == 9
    expected_subgrid_config_list = [
        {"off0": -448, "off1": -448, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": -448, "off1": 0, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": -448, "off1": 448, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 0, "off1": -448, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 0, "off1": 0, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 0, "off1": 448, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 448, "off1": -448, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 448, "off1": 0, "_mask0": None, "_mask1": None, "size": 448},
        {"off0": 448, "off1": 448, "_mask0": None, "_mask1": None, "size": 448},
    ]

    for actual_subgrid_config, expected_subgrid_config in zip(actual_subgrid_config_list, expected_subgrid_config_list):
        assert vars(actual_subgrid_config) == expected_subgrid_config
