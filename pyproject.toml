[tool.poetry]
name = "ska-sdp-distributed-self-cal-prototype"
version = "0.2.0"
description = "Prototype self-calibration pipeline to distribute processing across HPC cluster nodes, allowing scalability for large datasets and a reduction in overall computation time."
authors = ["Gemma Danks <gemma.danks@skao.int>", "Malcolm MacLeod <malcolm@vivosa.co.za>", "Max Maunder <max.maunder@nag.com>", "Sean Stansill <sean.stansill@nag.com>"]
readme = "README.md"

[[tool.poetry.source]]
name = "ska"
url = "https://artefact.skao.int/repository/pypi-internal/simple"
priority = "primary"

[[tool.poetry.source]]
name = "PyPI"
priority = "supplemental"

[tool.poetry.dependencies]
python = ">=3.10,<3.12"
numpy = ">=1.25.0"
xarray = ">=2023.10.1"
zarr = "2.16.1"
matplotlib = "^3.8.3"
xradio = "0.0.41"
pylru = "^1.2.1"
ska_sdp_func = "^1.2.0"
ska_sdp_func_python = "^0.5.1"
ska-sdp-exec-swiftly = "^1.0.0"
ska-sdp-datamodels = "^0.3.0"
nbqa = "^1.9.0"
nbmake = "^1.5.4"
pyyaml = "^6.0.2"
dask = "^2024.11.2"
distributed = "^2024.11.2"

[tool.poetry.group.dev.dependencies]
pytest = "^7.4.4"
pytest-cov = "^4.1.0"
isort = "^5.13.2"
black = "^23.12.1"
flake8 = "^7.0.0"
pylint = "^2.17.5"
pylint-junit = "^0.3.2"
pytest-lazy-fixture = "^0.6.3"

[tool.poetry.group.docs]
optional = false

[tool.poetry.group.docs.dependencies]
docutils = "0.18.1"
imagesize = "1.4.1"
Jinja2 = "3.1.2"
MarkupSafe = "2.1.2"
Pygments = "2.15.1"
Sphinx = "6.2.1"
sphinx-autobuild = "2021.3.14"
sphinx-copybutton = "0.5.2"
sphinx-gitstamp = "0.3.3"
sphinx-notfound-page = "0.8.3"
sphinxcontrib-websupport = "1.2.4"
myst-parser = "1.0.0"
ska-ser-sphinx-theme = "^0.1.2"

[tool.poetry.scripts]
ska_sdp_distributed_self_cal_prototype = "ska_sdp_distributed_self_cal_prototype.__main__:main"

[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
