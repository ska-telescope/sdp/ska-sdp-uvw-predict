# Copyright 2013-2024 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack.package import *
import os
import glob

class PySkaSdpDistributedSelfCalPrototype(PythonPackage):
    
    homepage = "https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-distributed-self-cal-prototype"
    git = "https://gitlab.com/ska-telescope/sdp/science-pipeline-workflows/ska-sdp-distributed-self-cal-prototype.git"

    license("BSD-3-Clause", checked_by="sstansill")
    maintainers("milhazes", "sstansill", "max_m17", "mmacleod_za", "gemmadanks")

    version('0.1.0', commit='v0.1.0', submodules=True)
    version('latest', branch='main', submodules=True, preferred=True)

    depends_on("python@3.11", type=("build", "run"))
    depends_on('py-poetry', type=("build", "run"))
    depends_on('py-tomlkit', type=("build", "run"))
    depends_on('py-yaml', type=("build", "run"))
    depends_on("py-numpy@1.25:", type=("build", "run"))
    depends_on("py-dask@2024.2.1:", type=("build", "run"))

    def install(self, spec, prefix):
        # Get the python and poetry executables
        poetry = Executable(spec['py-poetry'].prefix.bin.poetry)

        # Change into the source directory
        with working_dir(self.stage.source_path):
            # Use poetry to install directly into the Spack prefix
            poetry("install", "--no-root")
            poetry("run", "pip", "install", "--prefix=" + prefix, ".")
