# Include core make support
include .make/base.mk

# Include Python support
PYTHON_LINE_LENGTH = 120
include .make/python.mk

PYTHON_SWITCHES_FOR_NBMAKE = --ignore=notebooks/

.PHONY: checks
checks: isort black flake8 pylint pytest docs

.PHONY: isort
isort:
	poetry run isort --profile black --line-length $(PYTHON_LINE_LENGTH) src/ tests/
	poetry run nbqa isort --profile=black --line-length=$(PYTHON_LINE_LENGTH) notebooks/

.PHONY: black
black:
	poetry run black --exclude .+\.ipynb --line-length $(PYTHON_LINE_LENGTH) src/ tests/
	poetry run nbqa black --line-length=$(PYTHON_LINE_LENGTH) notebooks/

.PHONY: flake8
flake8:
	poetry run flake8 --show-source --statistics --max-line-length $(PYTHON_LINE_LENGTH) src/ tests/
	poetry run nbqa flake8 --show-source --statistics --max-line-length=$(PYTHON_LINE_LENGTH) notebooks/

.PHONY: pylint
pylint:
	poetry run pylint --max-line-length $(PYTHON_LINE_LENGTH) src/ tests/
	poetry run nbqa pylint --max-line-length=$(PYTHON_LINE_LENGTH) notebooks/

.PHONY: pytest
pytest:
	poetry run pytest --cov=src/ --cov-report=term-missing tests/

.PHONY: docs
docs:
	rm -rf docs/build
	$(MAKE) -f .make/docs.mk docs-build html
