# flake8: noqa
# pylint: skip-file
# flake8: noqa
# pylint: skip-file
# fmt: off
# isort: skip
"""Helper functions for constructing the pipeline"""

from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np

from ska_sdp_distributed_self_cal_prototype.data_managers.swiftly import Swiftly
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import ProcessingSetManager
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.processing_tasks.gridding import Gridder
from ska_sdp_distributed_self_cal_prototype.processing_tasks.utils_fits import get_wcs_from_image_info, save_fits_image
from ska_sdp_distributed_self_cal_prototype.units import sky_fraction2asec
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import ImageInfo, PipelineConfig

logger = setup_logger(__name__)


def calculate_observation_info(
    pipeline_config: PipelineConfig,
    processing_set_manager: ProcessingSetManager,
    swiftly_manager: Swiftly,
    gridding_manager: Gridder,
) -> PipelineConfig:
    """Calculates observation metadata.

    Args:
        pipeline_config: Configuration for the pipeline.
        processing_set_manager: Manager for visibility data.
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.

    Returns:
        Updated pipeline_config.
    """

    pixel_scale_sky_fractions = pipeline_config.gridder_info.pixel_scale
    pixel_scale_asec = sky_fraction2asec(pixel_scale_sky_fractions)

    pipeline_config.image_info = ImageInfo(processing_set_manager.phase_center, pixel_scale_asec, pixel_scale_asec)
    return pipeline_config


def save_image(
        image_info: ImageInfo,
        image_data: np.ndarray,
        output_dir: Path,
        filename: str = "dirty_image",
        save_fits: bool = True,
        save_png: bool = True,
) -> None:
    """Save 2D array of image data as FITS and/or PNG images.

        Args:
            image_info: Information on phase centre, pixel size and frequency required to construct the WCS.
            image_data: Data to save as FITS/PNG.
            output_dir: The directory where the image will be saved.
            filename: The filename the image will be saved into.
            save_fits: Controls whether a FITS file is output.
            save_png: Controls whether a PNG file is output.

        Returns:
            None

        Notes:
            - image_info is only required if saving to FITS
            - Image is rotated by 90 degrees for correct display
            - PNG image is displayed with RA/DEC axis and colour bar
        """

    assert (save_fits or save_png), "Image must be saved as FITS or PNG."

    output_dir.mkdir(parents=True, exist_ok=True)
    output_file = output_dir / filename

    # Rotate by 90 degrees to get correct orientation for output (x and y are swapped by the gridder)
    image_data = np.rot90(image_data, 1, (1, 0))

    image_width = image_data.shape[0]
    image_height = image_data.shape[1]

    wcs = get_wcs_from_image_info(
        image_info,
        width_in_pixels=image_width,
        height_in_pixels=image_height
    )

    if save_png:
        png_output_name = output_file.with_suffix(".png")
        # WCS is 4d; slice away 2 dimensions so that it will work with matplotlib
        wcs_2d = wcs[0, 0, :]
        ax = plt.subplot(projection=wcs_2d)
        im = ax.imshow(image_data, cmap="viridis")
        ax.set_xlabel('RA')
        ax.set_ylabel('Dec')
        cbar = plt.colorbar(im, pad=0.1)
        plt.savefig(png_output_name, bbox_inches="tight")
        logger.info(f"PNG image saved in {str(png_output_name)}.")
        plt.close()

    if save_fits:
        fits_output_name = output_file.with_suffix(".fits")
        # Currently our output images will always be 1 polarisation
        # In future when we want to support multiple polarisations this should become a parameter
        n_polarisations = 1
        # WCS is 4d; Add two dimensions to match
        image_data = np.reshape(
            image_data,
            (
                image_info.n_output_channels,
                n_polarisations,
                image_width,
                image_height
            )
        )
        save_fits_image(
            image_data,
            wcs,
            fits_output_name
        )
        logger.info(f"Fits image saved in {str(fits_output_name)}.")


def split_square_array(array: np.ndarray, n_subarray: int) -> list[np.ndarray]:
    """
    Split a 2D square array into `n_subarray` square subarrays, where `n_subarray` is a square number.

    Args:
        array: The input 2D square array to split.
        n_subarray: The number of square subarrays (must be a square number).

    Returns:
        subarrays: A list containing the square subarrays.
    """
    # Check array is 2D.
    if array.ndim != 2:
        raise ValueError("The array must be 2D.")

    # Check array is square.
    rows, cols = array.shape
    if rows != cols:
        raise ValueError("The array must be square (number of rows must equal number of columns).")

    # Check if n_subarray is a square number.
    sqrt_n = int(np.sqrt(n_subarray))
    if sqrt_n * sqrt_n != n_subarray:
        raise ValueError(f"n_subarray ({n_subarray}) must be a square number.")

    # Get the size of each subarray.
    array_side_pixels = len(array)
    subarray_side_pixels = array_side_pixels // sqrt_n

    # Ensure the original array can be evenly split.
    if array_side_pixels % sqrt_n != 0:
        raise ValueError("The input array cannot be evenly split into the specified number of subarrays.")

    # Split into subarrays by reshaping and swapping axes.
    array_reshaped = array.reshape(sqrt_n, subarray_side_pixels, sqrt_n, subarray_side_pixels)
    array_swapped = array_reshaped.swapaxes(1, 2)
    subarrays = array_swapped.reshape(-1, subarray_side_pixels, subarray_side_pixels)

    return subarrays
