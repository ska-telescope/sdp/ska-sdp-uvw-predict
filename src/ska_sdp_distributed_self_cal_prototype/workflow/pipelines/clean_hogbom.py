"""Pipeline to do a single major iteration. Generate dirty image, generate PSF, run a single pass of 'hogbom' clean with
full minor iterations, output dirty, psf, residual and model .

1. Load configuration parameters and parse CLI args
2. Read processing set from directory
3. Initialise gridding
    - Initialise gridding kernel
    - Initialise dask client
    - Add tasks to dask client
4. Save psf image
5. Save dirty image
6. Run hogbom on dirty image and psf
7. Save model image
8. Save residual image
"""

# Ignore todos for now
# pylint: disable=W0511

import logging
from pathlib import Path

import numpy as np

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import (
    bin_data,
    configure_and_setup_pipeline,
    distributed_hogbom,
    generate_clean_beam_parameters,
    generate_facets_with_corrections,
    grid_visibilities,
    hogbom,
    initialise_self_calibration,
    restore_model,
    save_image,
    split_image,
)

log = logging.getLogger("func-python-logger")

logger = setup_logger(__name__)


def clean_hogbom(config_filepath: Path) -> None:
    """Pipeline to generate psf, dirty, model and residual images.

    Args:
        config_filepath: Path to the configuration file used to set up the pipeline parameters.

    Returns:
        None
    """

    # Set up pipeline config
    # pylint:disable=duplicate-code
    (
        pipeline_config,
        processing_set_manager,
        swiftly_manager,
        gridding_manager,
        dask_client,
    ) = configure_and_setup_pipeline(config_filepath)

    logger.info("Bin visibilities for subgrids")
    visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)

    # Get deconvolution specific config options
    deconvolution_config = pipeline_config.get_deconvolution_parameters()

    # Set up self-calibration
    self_calibration_manager = initialise_self_calibration(pipeline_config)

    logger.info("Generating psf")
    psf_image = self_calibration_manager.generate_psf(
        visibility_bins,
        binning_info,
        gridding_manager,
    )

    logger.info("Saving PSF image")
    save_image(pipeline_config, psf_image, pipeline_config.output_filenames["psf_image"])

    logger.info("Grid visibilities")
    swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)

    logger.info("Generating dirty image")
    dirty_image_facets, _ = generate_facets_with_corrections(
        swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
    )

    logger.info("Saving dirty image")
    dirty_image = swiftly_manager.join_facets(dirty_image_facets)
    save_image(pipeline_config, dirty_image, pipeline_config.output_filenames["dirty_image"])

    # Do a single pass of hogbom cleaning with multiple minor iterations to produce model and residual
    if deconvolution_config.parallel_cleaning:
        model_facets = len(dirty_image_facets) * [np.zeros(dirty_image_facets[0].shape, dirty_image_facets[0].dtype)]

        logger.info("Deconvolve PSF with dirty facets (distributed_hogbom)")
        residual_facets, model_facets = distributed_hogbom(
            dask_client,
            dirty_image_facets,
            psf_image,
            model_facets,
            gain=deconvolution_config.gain,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )
        model_image = swiftly_manager.join_facets(model_facets)
        residual_image = swiftly_manager.join_facets(residual_facets)
    else:
        model_image = np.zeros(dirty_image.shape, dtype=dirty_image.dtype)

        logger.info("Deconvolve PSF with dirty image (hogbom)")
        residual_image, model_image = hogbom(
            dirty_image,
            psf_image,
            model_image,
            window=True,
            gain=deconvolution_config.gain,
            thresh=0,
            niter=deconvolution_config.niter,
            fracthresh=deconvolution_config.fracthresh,
        )
        model_facets = split_image(model_image, swiftly_manager.facet_count)
        residual_facets = split_image(residual_image, swiftly_manager.facet_count)

    logger.info("Generating clean beam parameters")
    clean_beam_parameters = generate_clean_beam_parameters(pipeline_config, psf_image)

    logger.info("Restoring the model")
    clean_facets = restore_model(model_facets, residual_facets, clean_beam_parameters, dask_client)
    clean_image = swiftly_manager.join_facets(clean_facets)

    logger.info("Saving clean image")
    save_image(pipeline_config, clean_image, pipeline_config.output_filenames["clean_image"])

    logger.info("Saving model image")
    save_image(pipeline_config, model_image, pipeline_config.output_filenames["model_image"])

    logger.info("Saving residual image")
    save_image(pipeline_config, residual_image, pipeline_config.output_filenames["residual_image"])

    logger.info("Done!")
    dask_client.close()
