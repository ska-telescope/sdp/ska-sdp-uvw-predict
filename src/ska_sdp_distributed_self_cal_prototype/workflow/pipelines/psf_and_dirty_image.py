"""Pipeline to apply self calibration and generate a clean image."""

from pathlib import Path

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import (
    bin_data,
    configure_and_setup_pipeline,
    generate_facets_with_corrections,
    grid_visibilities,
    initialise_self_calibration,
    save_image,
)

logger = setup_logger(__name__)


def psf_and_dirty_image(config_filepath: Path) -> None:
    """Pipeline to apply self-calibration and output a clean image.

    Args:
        config_filepath: Path to the configuration file used to set up the pipeline parameters.

    Returns:
        None
    """
    # pylint:disable=duplicate-code
    (
        pipeline_config,
        processing_set_manager,
        swiftly_manager,
        gridding_manager,
        dask_client,
    ) = configure_and_setup_pipeline(config_filepath)

    logger.info("Bin visibilities for subgrids")
    visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)

    self_calibration_manager = initialise_self_calibration(pipeline_config)

    logger.info("Grid visibilities")
    swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)

    logger.info("Generating psf")
    psf_image = self_calibration_manager.generate_psf(
        visibility_bins,
        binning_info,
        gridding_manager,
    )

    logger.info("Saving PSF image")
    save_image(pipeline_config, psf_image, pipeline_config.output_filenames["psf_image"])

    logger.info("Generate facets with corrections")
    dirty_image_facets, _ = generate_facets_with_corrections(
        swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
    )

    logger.info("Combine facets into single image")
    dirty_image = swiftly_manager.join_facets(dirty_image_facets)

    logger.info("Saving dirty image")
    save_image(pipeline_config, dirty_image, pipeline_config.output_filenames["dirty_image"])

    logger.info("Done!")
    dask_client.close()
