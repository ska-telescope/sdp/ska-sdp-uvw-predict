"""Pipeline to do continuum imaging with major cycles."""

from pathlib import Path

import numpy as np
from dask.distributed import wait

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.workflow.tasks import (
    bin_data,
    configure_and_setup_pipeline,
    distributed_hogbom,
    generate_clean_beam_parameters,
    generate_facets_with_corrections,
    grid_visibilities,
    initialise_self_calibration,
    predict_residual_visibilities,
    restore_model,
    save_image,
)

logger = setup_logger(__name__)


def continuum_imaging(config_filepath: Path) -> None:
    """Pipeline for performing continuum imaging, including major cycles to update the model.

    This function orchestrates a continuum imaging pipeline, which involves generating and saving the PSF image,
    executing a series of major cycles to update residual image, perform deconvolution, and restoring the final clean
    image. Each major cycle computes and saves intermediate results, including residual and model images.

    Args:
        config_filepath: Path to the configuration file used to set up the pipeline parameters.

    Returns:
        None
    """
    # pylint:disable=duplicate-code, too-many-statements
    (
        pipeline_config,
        processing_set_manager,
        swiftly_manager,
        gridding_manager,
        dask_client,
    ) = configure_and_setup_pipeline(config_filepath)
    with dask_client as dask_client:
        deconvolution_config = pipeline_config.get_deconvolution_parameters()
        self_calibration_manager = initialise_self_calibration(pipeline_config)
        logger.info("Pipeline configured.")

        logger.info("Binning visibilities for subgrids.")
        visibility_bins, binning_info = bin_data(processing_set_manager, dask_client)
        logger.info("Visibilities binned.")

        logger.info("Generating psf")
        psf_image = self_calibration_manager.generate_psf(
            visibility_bins,
            binning_info,
            gridding_manager,
        )

        logger.info("Saving PSF image")
        save_image(pipeline_config, psf_image, pipeline_config.output_filenames["psf_image"])

        logger.info("Grid visibilities")
        swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)

        logger.info("Generate facets with corrections")
        residual_facets, normalisation_factor = generate_facets_with_corrections(
            swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
        )

        logger.info("Saving dirty image")
        dirty_image = swiftly_manager.join_facets(residual_facets)
        save_image(pipeline_config, dirty_image, pipeline_config.output_filenames["dirty_image"])
        model_facets = len(residual_facets) * [np.zeros(residual_facets[0].shape, residual_facets[0].dtype)]
        for major_cycle in range(pipeline_config.major_cycles):
            logger.info(f"Starting major cycle {major_cycle}...")
            logger.info("Deconvolving the residual image")
            _, model_facets = distributed_hogbom(
                dask_client=dask_client,
                residual_facets_to_clean=residual_facets,
                psf_image=psf_image,
                model_facets_to_update=model_facets,
                gain=deconvolution_config.gain,
                niter=deconvolution_config.niter,
                fracthresh=deconvolution_config.fracthresh,
            )
            logger.info(f"Sum of model facet brightness after cleaning: {np.sum(model_facets)}")
            logger.info("Saving model image")
            model_image = swiftly_manager.join_facets(model_facets)
            save_image(pipeline_config, model_image, f"{pipeline_config.output_filenames['model_image']}{major_cycle}")

            logger.info("Predicting visibilities from model facets")
            residual_vis_sum_before = np.sum(
                [np.sum(visibility_bin.res_vis.compute()) for visibility_bin in visibility_bins]
            )
            logger.info(f"Sum of residual visibilities before subtraction: {residual_vis_sum_before}")
            visibility_bins = predict_residual_visibilities(
                facets=model_facets,
                visibility_bins_list=visibility_bins,
                binning_info=binning_info,
                swiftly_manager=swiftly_manager,
                gridding_manager=gridding_manager,
                normalisation_factor=normalisation_factor,
            )
            residual_vis_sum_after = np.sum(
                [np.sum(visibility_bin.res_vis.compute()) for visibility_bin in visibility_bins]
            )
            logger.info(f"Sum of residual visibilities after subtraction: {residual_vis_sum_after}")
            logger.info(
                f"Difference in sum of residual visibilities before and after subtraction:"
                f"{residual_vis_sum_before - residual_vis_sum_after}"
            )

            logger.info("Grid visibilities")
            swiftly_manager = grid_visibilities(visibility_bins, binning_info, swiftly_manager, gridding_manager)

            logger.info("Generating residual image")
            residual_facets, _ = generate_facets_with_corrections(
                swiftly_manager, gridding_manager, binning_info["total_num_visibilities"], binning_info["channel_count"]
            )

            logger.info("Saving residual image")
            residual_image = swiftly_manager.join_facets(residual_facets)
            residual_image_future = dask_client.submit(
                save_image,
                pipeline_config,
                residual_image,
                f"{pipeline_config.output_filenames['residual_image']}{major_cycle}",
            )
            wait(residual_image_future)
            logger.info(f"Finished major cycle {major_cycle}!")

        logger.info("Generating clean beam parameters")
        clean_beam_parameters = generate_clean_beam_parameters(pipeline_config, psf_image)

        logger.info("Restoring the model")
        clean_facets = restore_model(model_facets, residual_facets, clean_beam_parameters, dask_client)
        logger.info("Saving clean image")
        clean_image = swiftly_manager.join_facets(clean_facets)
        clean_image_future = dask_client.submit(
            save_image, pipeline_config, clean_image, pipeline_config.output_filenames["clean_image"]
        )
        wait(clean_image_future)
        logger.info("Done!")
