"""Module for constructing the pipeline from inputs"""

# Ignore todos and too-many-attributes
# pylint: disable=W0511, R0902

import re
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Optional

import yaml
from ska_sdp_exec_swiftly import SWIFT_CONFIGS

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.processing_tasks._gridding_utils import _determine_w_step
from ska_sdp_distributed_self_cal_prototype.units import amin2asec, asec2sky_fraction, deg2asec

logger = setup_logger(__name__)


@dataclass
class ProcessingSetInfo:
    """Class for storing information about the processing set."""

    number_datasets: int
    number_uvw_partitions: int
    vis_name: str
    min_frequency: float


@dataclass
class PhaseCenter:
    """Class for storing phase centre information."""

    phase_centre_ra: float
    phase_centre_dec: float
    phase_centre_frame: str


@dataclass
class ImageInfo:
    """Class for storing image information."""

    phase_centre: PhaseCenter
    pixel_width_arcsec: float
    pixel_height_arcsec: float
    start_frequency: Optional[float] = 1.0e8
    end_frequency: Optional[float] = 3.0e8
    n_output_channels: Optional[int] = 1


# pylint: disable=C0103
@dataclass
class SwiftlyInfo:
    """Class for storing swiftly config information."""

    config_name: str
    image_size: int
    facet_size: int
    facet_size_effective: int
    subgrid_size: int
    subgrid_size_effective: int
    Nx: int
    facet_count: int
    backwards_steps: int
    backward_queue_size: int
    forwards_steps: int
    forward_queue_size: int
    swiftly_config: dict
    subgrid_count: Optional[int] = 0


@dataclass
class DeconvolutionInfo:
    """Class for storing deconvolution information."""

    fracthresh: float
    gain: float
    niter: int
    parallel_cleaning: bool


@dataclass
class GridderInfo:
    """Class for storing gridding information."""

    name: str
    shear_u: float
    shear_v: float
    support: int
    oversampling: int
    w_support: int
    w_oversampling: int
    wtower_size: int
    pixel_scale: float
    image_scale: float
    image_scale_padded: float
    w_step: Optional[float] = 0


@dataclass
class SelfCalInfo:
    """Class for storing information about the self-calibration step."""

    major_cycles: Optional[int] = 2


class PipelineConfig:
    """Class to store base pipeline config parameters.

    Attributes:
        config: Dictionary created from the YAML file.
        ps_dir: Path to the MSv4.
        dataset_key: MSv4 partition key.
        swiftly_info: SwiftlyInfo object containing information used by swiftly.
        gridder_info: GridderInfo object containing information used for gridding.
        output_dir: Path where output data products are saved.
        dask_address: Address for the dask cluster.
    """

    def __init__(self, config_file):
        self.config = self._load_yaml_config(config_file)
        self.ps_dir = Path(self._get_yaml_config_value("ps_dir"))
        self.dataset_key = self._get_yaml_config_value("dataset_key")
        self.swiftly_info = self._get_swiftly_parameters()
        self.gridder_info = self._get_gridding_parameters()
        self.output_dir = Path(self._get_yaml_config_value("output_dir"))
        self.output_filenames = self._get_yaml_config_value("output_filenames", "use_default")
        self._parse_output_filenames()
        self.dask_address = self.config.get("dask_address", None)  # TODO fix this
        self.major_cycles = self._get_yaml_config_value("major_cycles", 1)
        # Below config options are set during pipeline execution
        self.image_info = None

    def _parse_output_filenames(self) -> None:
        """Checks that `output_filenames` is valid."""
        if self.output_filenames == "use_default":
            self.output_filenames = {
                "psf_image": "psf_image",
                "dirty_image": "dirty_image",
                "clean_image": "clean_image",
                "intermediate_images": "cleaned_cycle_",
                "residual_image": "residual_image",
                "model_image": "model_image",
            }

        else:
            assert isinstance(
                self.output_filenames, dict
            ), "output_filenames must be a dictionary in the pipeline config."

            # Loop over filenames to check they're strings and don't contain extensions like ".png"
            for key, value in self.output_filenames.items():
                assert isinstance(value, str), f"'{key}' must be a string in the pipeline_config."

    def _load_yaml_config(self, file_path: Path) -> dict:  # pragma: no cover
        """Load and parse a YAML configuration file.

        This function reads a YAML file from the given file path, parses its content,
        and returns the resulting dictionary.

        Args:
            file_path: The path to the YAML configuration file.

        Returns:
            config: A dictionary containing the parsed configuration data.

        Raises:
            FileNotFoundError: If the specified file does not exist.
            yaml.YAMLError: If there is an error parsing the YAML file.
        """
        with open(file_path, "r", encoding="utf-8") as file:
            config = yaml.safe_load(file)
        return config

    def _get_yaml_config_value(self, key: str, default_value: Any = None) -> Any:
        """Retrieve a value from the YAML configuration dictionary.

        This function attempts to retrieve the value associated with the specified
        key from the given config dictionary. If the key is not found, an error is
        logged and a ValueError is raised.

        Args:
            key: The key whose associated value is to be retrieved.
            default_value: Optional default value to return if the key doesn't exist in the YAML config. If omitted,
                the function will raise an error.

        Returns:
            The value associated with the specified key.

        Raises:
            ValueError: If the specified key is not found in the configuration dictionary.
        """
        value = None
        try:
            value = self.config[key]
        except KeyError:
            value = default_value
        finally:
            # Only raise an error if key is missing from YAML and doesn't have a default value
            if (value is None) and (default_value is None):
                error_message = f"The YAML config file is missing '{key}'."
                logger.error(error_message)
                raise ValueError(error_message)
        return value

    def _get_swiftly_parameters(self):
        """Extract and store swiftly parameters from SWIFT_CONFIGS and YAML."""
        kwargs = {}
        kwargs["config_name"] = self._get_yaml_config_value("swiftly_config")

        swiftly_config = SWIFT_CONFIGS[kwargs["config_name"]]
        kwargs["swiftly_config"] = swiftly_config

        kwargs["image_size"] = swiftly_config["N"]
        kwargs["facet_size"] = swiftly_config["yN_size"]
        kwargs["facet_size_effective"] = swiftly_config["yB_size"]
        kwargs["subgrid_size"] = swiftly_config["xM_size"]
        kwargs["subgrid_size_effective"] = swiftly_config["xA_size"]
        # TODO below should use yB_size not yN_size
        kwargs["facet_count"] = swiftly_config["N"] // swiftly_config["yN_size"]
        kwargs["Nx"] = swiftly_config["Nx"]

        # TODO Update subgrid_count in finish_setup()?
        # kwargs["subgrid_count"] = 0

        # TODO Set to 1 if running dirty_image_pipeline(). If self-cal, calculate from number of major cycles
        kwargs["backwards_steps"] = 1

        # TODO Check with Vijay if this is the expected behaviour
        kwargs["forwards_steps"] = kwargs["backwards_steps"] - 1

        kwargs["backward_queue_size"] = self._get_yaml_config_value("backward_queue_size", default_value=20)
        kwargs["forward_queue_size"] = self._get_yaml_config_value("forward_queue_size", default_value=20)

        return SwiftlyInfo(**kwargs)

    def _get_gridding_parameters(self) -> GridderInfo:
        """Extract and store gridding parameters from YAML.

        Returns:
            Dataclass object containing information required by the gridder.
        """
        _gridder_names = ["wtowers"]
        name = self._get_yaml_config_value("gridder", default_value="wtowers")
        assert name in _gridder_names

        pixel_scale = self._get_pixel_scale(self._get_yaml_config_value("pixel_scale"))
        image_scale = pixel_scale * self.swiftly_info.image_size
        # TODO: image_scale_padded is the scale of the image produced by the padded grid (all padded subgrids)
        image_scale_padded = image_scale
        support = self._get_yaml_config_value("grid_support", default_value=8)
        oversampling = self._get_yaml_config_value("grid_oversampling", default_value=16384)
        shear_u = self._get_yaml_config_value("shear_u", default_value=0.0)
        shear_v = self._get_yaml_config_value("shear_v", default_value=0.0)
        wtower_size = self._get_yaml_config_value("wtower_size", 100)
        w_step = round(_determine_w_step(image_scale_padded, image_scale, shear_u=0, shear_v=0))

        return GridderInfo(
            name=name,
            shear_u=shear_u,
            shear_v=shear_v,
            support=support,
            oversampling=oversampling,
            w_support=support,
            w_oversampling=oversampling,
            pixel_scale=pixel_scale,
            image_scale=image_scale,
            image_scale_padded=image_scale_padded,
            wtower_size=wtower_size,
            w_step=w_step,
        )

    @staticmethod
    def _get_pixel_scale(pixel_scale: str) -> float:
        """Extracts the pixel scale in sky fractions from input string.

        Args:
            pixel_scale: A string that contains a numeric value followed by the unit without spaces. Example valid
                inputs:
                - '1deg'
                - '0.01asec'
                - '0.22amin'

        Returns:
            The pixel scale in sky fractions.
        """
        unit_conversions = {"deg": deg2asec, "amin": amin2asec, "asec": lambda x: x}

        if not isinstance(pixel_scale, str):
            raise ValueError(f"'pixel_scale' ({pixel_scale}) must be a string.")

        match = re.match(r"(\d*\.?\d+)([a-z]+)", pixel_scale.strip())
        if not match:
            raise ValueError("Invalid 'pixel_scale' format: must contain both a number and a unit")

        value, unit_str = match.groups()
        if unit_str not in unit_conversions:
            raise ValueError(f"pixel_scale unit '{unit_str}' not recognised. Use 'deg', 'amin' or 'asec'.")

        try:
            value = float(value)
        except ValueError as err:
            raise ValueError("The numeric part of 'pixel_scale' is invalid.") from err

        pixel_scale_asec = unit_conversions[unit_str](value)

        return asec2sky_fraction(pixel_scale_asec)

    def get_deconvolution_parameters(self) -> DeconvolutionInfo:
        """Extract and store deconvolution parameters from the YAML config.

        Returns:
            deconvolution_info: An object containing all deconvolution specific configuration settings.
        """

        fracthresh = self._get_yaml_config_value("fracthresh", 0.5)
        gain = self._get_yaml_config_value("gain", 0.1)
        niter = self._get_yaml_config_value("niter", 50)
        parallel_cleaning = self._get_yaml_config_value("parallel_cleaning", False)

        deconvolution_info = DeconvolutionInfo(
            fracthresh,
            gain,
            niter,
            parallel_cleaning,
        )
        return deconvolution_info
