"""Module for highlevel workflow tasks to be imported into pipelines."""

# Ignore todos for now
# pylint: disable=W0511

import time
from pathlib import Path
from typing import Union

import dask
import dask.array as da
import numpy as np
import xarray as xr
from astropy.convolution import Gaussian2DKernel, convolve_fft
from dask.distributed import Client, LocalCluster
from ska_sdp_func_python.image.cleaners import overlapIndices
from ska_sdp_func_python.image.deconvolution import fit_psf

from ska_sdp_distributed_self_cal_prototype.data_managers.deconvolution import SelfCalibrationManager
from ska_sdp_distributed_self_cal_prototype.data_managers.swiftly import Swiftly
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_io import ProcessingSetManager
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.logging_utils import log_imaging_parameters
from ska_sdp_distributed_self_cal_prototype.processing_tasks.binning import bin_visibilities
from ska_sdp_distributed_self_cal_prototype.processing_tasks.clean_beam import gaussian_2d
from ska_sdp_distributed_self_cal_prototype.processing_tasks.gridding import Gridder
from ska_sdp_distributed_self_cal_prototype.processing_tasks.utils_fits import (
    create_image_with_wcs,
    get_wcs_from_image_info,
    load_image_data_from_fits,
)
from ska_sdp_distributed_self_cal_prototype.processing_tasks.validation import validate_data
from ska_sdp_distributed_self_cal_prototype.workflow import utils
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig

logger = setup_logger(__name__)


def setup_pipeline(pipeline_config: PipelineConfig) -> tuple[PipelineConfig, ProcessingSetManager, Swiftly, Gridder]:
    """Setup required managers for all pipelines.

    Args:
        pipeline_config: Initial configuration for pipeline.

    Returns:
        pipeline_config: Updated configuration for pipeline.
        processing_set_manager: Manager for visibility data.
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.
    """

    processing_set_manager, pipeline_config = load_data(pipeline_config)
    gridding_manager, pipeline_config = initialise_gridder(pipeline_config)
    swiftly_manager, pipeline_config = initialise_swiftly(pipeline_config)
    pipeline_config, processing_set_manager, swiftly_manager, gridding_manager = finish_setup(
        pipeline_config, processing_set_manager, swiftly_manager, gridding_manager
    )

    log_imaging_parameters(logger, pipeline_config)

    return pipeline_config, processing_set_manager, swiftly_manager, gridding_manager


def configure_pipeline(config_file: Path) -> PipelineConfig:
    """Load configuration and parameterise workflow.

    Args:
        config_file: Path to YAML config file.

    Returns:
        pipeline_config: Configuration for the pipeline.
    """
    logger.info("Loading configuration...")
    pipeline_config = PipelineConfig(config_file)
    logger.info(f"Configuration: {vars(pipeline_config)}")
    return pipeline_config


def initialise_dask_client(dask_address: str) -> Client:
    """Initialise and return a Dask client.

    This function creates a Dask client connected to the specified Dask
    scheduler address. If no address is provided, it initializes a local Dask
    cluster and returns a client connected to it.

    Args:
        dask_address: The address of the Dask scheduler. If not provided,
            a local Dask cluster will be used.

    Returns:
        Client: A Dask client connected to either the specified scheduler or a
            local cluster.
    """
    logger.info("Initialising Dask client...")
    if dask_address:
        client = Client(dask_address)
    else:
        cluster = LocalCluster()
        client = Client(cluster)
    logger.info("Dask dashboard available at: %s.", client.dashboard_link)
    return client


def initialise_self_calibration(pipeline_config: PipelineConfig) -> SelfCalibrationManager:
    """Initialize self-calibration.

    Args:
        pipeline_config: Configuration for the pipeline.
        processing_set_manager: Manager for visibility data.

    Returns:
        self_calibration_manager: Manager for applying self-calibration.
    """

    self_calibration_manager = SelfCalibrationManager(pipeline_config)

    return self_calibration_manager


def configure_and_setup_pipeline(
    config_filepath: Path,
) -> tuple[PipelineConfig, ProcessingSetManager, Swiftly, Gridder, Client]:
    """Perform all common setup that is across every pipeline.

    1. configure_pipeline
    2. initialise_dask_client
    3. setup_pipeline

    Args:
        config_filepath: Configuration for the pipeline.

    Returns:
        pipeline_config: Updated configuration for pipeline.
        processing_set_manager: Manager for visibility data.
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.
        dask_client: A Dask client connected to either the specified scheduler or a local cluster.
    """
    pipeline_config = configure_pipeline(config_filepath)
    dask_client = initialise_dask_client(pipeline_config.dask_address)
    pipeline_config, processing_set_manager, swiftly_manager, gridding_manager = setup_pipeline(pipeline_config)

    return pipeline_config, processing_set_manager, swiftly_manager, gridding_manager, dask_client


def load_data(pipeline_config: PipelineConfig) -> tuple[ProcessingSetManager, PipelineConfig]:
    """Loads measurement set from file.

    Updates configuration using observation info.

    Args:
        pipeline_config: Configuration for pipeline.

    Returns:
        processing_set_manager: Manager for visibility data.
        pipeline_config: Configuration for pipeline.
    """
    logger.info(f"Loading data from {pipeline_config.ps_dir}...")
    processing_set_manager = ProcessingSetManager(pipeline_config)
    logger.info("Updating the config")
    pipeline_config = processing_set_manager.config
    logger.info("Done!")
    return processing_set_manager, pipeline_config


def get_dataset_info(dataset: xr.Dataset, vis_name: str):
    """Get metadata from dataset.

    Args:
        dataset: dataset containing visibility data
        vis_name: name of visibility data column

    Returns:
        metadata: dictionary containing information about dataset:
            channel_width: float
            min_frequency: float
            channel_count: int
            time_count: int
            baseline_count: int
            total_num_visibilities: int
    """
    time_count, baseline_count, channel_count, polarisation_count = dataset[vis_name].shape
    total_num_visibilities = time_count * baseline_count * channel_count * polarisation_count
    logger.info(f"Dataset contains {total_num_visibilities} visibilities")
    metadata = {
        "channel_width": dataset.frequency.attrs["channel_width"]["data"],
        "min_frequency": dataset.frequency.data[0],
        "channel_count": channel_count,
        "time_count": time_count,
        "baseline_count": baseline_count,
        "total_num_visibilities": total_num_visibilities,
    }
    return metadata


def bin_data(processing_set_manager: ProcessingSetManager, dask_client: Client) -> tuple[list[VisibilityBin], dict]:
    """Bin visibility data for subgrids.

    Args:
        processing_set_manager: Manager for visibility data.
        dask_client: Dask client.

    Returns:
        tuple:
            - visibility_bins_pruned: List of VisibilityBin objects containing binned data with empty bins removed.
            - binning_info: Dictionary containing global parameters for binning data for each subgrid:
                - channel_width (float): The width of each channel in Hz.
                - min_frequency (float): The minimum frequency in Hz.
                - channel_count (int): The number of channels.
                - time_count (int): The number of times.
                - baseline_count (int): The number of baselines.
                - normalisation_factor (int): The factor for normalisation.
                - image_scale_padded (float): Padded scale for the image.
                - subgrid_size_effective (int): Effective size of the subgrid in pixels.
                - subgrid_size (int): Size of the subgrid in pixels.
                - wtower_size (int): Size of the wtower.
                - w_step (int): Step size for W-coordinate.
                - vis_name (str): Name of the visibility column, either "VISIBILITY" or "VISIBILITY_CORRECTED".
    """
    subgrid_config_list = processing_set_manager.subgrid_config_list
    vis_name = processing_set_manager.vis_name
    dataset = processing_set_manager.get_dataset()
    dataset_info = get_dataset_info(dataset, vis_name)
    subgrid_info = {
        "image_scale_padded": processing_set_manager.config.gridder_info.image_scale_padded,
        "subgrid_size_effective": processing_set_manager.config.swiftly_info.subgrid_size_effective,
        "subgrid_size": processing_set_manager.config.swiftly_info.subgrid_size,
        "wtower_size": processing_set_manager.config.gridder_info.wtower_size,
        "w_step": processing_set_manager.config.gridder_info.w_step,
        "vis_name": vis_name,
    }
    binning_info = subgrid_info | dataset_info
    visibility_bins_delayed = bin_visibilities(dataset, subgrid_config_list, binning_info)
    visibility_bins = dask_client.compute(visibility_bins_delayed, sync=True)
    visibility_bins_pruned = [visibility_bin for visibility_bin in visibility_bins if visibility_bin]
    logger.warning(f"Removed {len(visibility_bins) - len(visibility_bins_pruned)} empty visibility bins")
    logger.info(f"Visibilities binned into {len(visibility_bins_pruned)} visibility bins")
    return visibility_bins_pruned, binning_info


def initialise_gridder(pipeline_config: PipelineConfig) -> tuple[Gridder, PipelineConfig]:
    """Constructs the gridding manager and updates the pipeline config.

    Args:
        pipeline_config: Configuration for pipeline.

    Returns:
        gridding_manager: Gridding manager for the pipeline.
        pipeline_config: Configuration for the pipeline.
    """
    logger.info("Making the gridder...")
    gridding_manager = Gridder(pipeline_config)
    logger.info("Updating the config")
    pipeline_config = gridding_manager.config
    logger.info("Done!")
    return gridding_manager, pipeline_config


def initialise_swiftly(pipeline_config: PipelineConfig) -> tuple[Swiftly, PipelineConfig]:
    """Constructs the swiftly manager and updates the pipeline config.

    Args:
        pipeline_config: Configuration for pipeline.

    Returns:
        swiftly_manager: Swiftly manager for the pipeline.
        pipeline_config: Configuration for the pipeline.
    """
    logger.info("Setting up swiftly...")
    swiftly_manager = Swiftly(pipeline_config)
    logger.info("Updating the config")
    pipeline_config = swiftly_manager.config
    logger.info("Done!")
    return swiftly_manager, pipeline_config


def finish_setup(
    pipeline_config: PipelineConfig,
    processing_set_manager: ProcessingSetManager,
    swiftly_manager: Swiftly,
    gridding_manager: Gridder,
) -> tuple[PipelineConfig, ProcessingSetManager, Swiftly, Gridder]:
    """Calculates and stores key observation parameters.

    This includes pixel size in arcsec, image phase centre, etc.

    Args:
        pipeline_config: PipelineConfig instance that stores all information about the observation
            and pipeline.
        processing_set_manager: Manager for visibility data.
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.

    Returns:
        pipeline_config: An updated PipelineConfig instance.
        processing_set_manager: Manager for visibility data.
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.
    """
    logger.info("Calculating observation information")
    pipeline_config = utils.calculate_observation_info(
        pipeline_config, processing_set_manager, swiftly_manager, gridding_manager
    )
    logger.info("Done!")
    # TODO Streamline config changes/updates
    return pipeline_config, processing_set_manager, swiftly_manager, gridding_manager


def grid_visibilities(
    visibility_bins: list[VisibilityBin],
    binning_info: dict,
    swiftly_manager: Swiftly,
    gridding_manager: Gridder,
) -> Swiftly:
    """Grid visibilities.

    Args:
        visibility_bins: list of VisibilityBin objects containing binned visibility data.
        binning_info: parameters for gridding
        swiftly_manager: Swiftly manager for the pipeline.
        gridding_manager: Gridding manager for the pipeline.

    Returns:
        swiftly_manager: Swiftly manager with tasks for gridding.
    """
    gridding_manager.grid_visibilities(
        swiftly_manager=swiftly_manager,
        visibility_bins=visibility_bins,
        subgrid_size=binning_info["subgrid_size"],
        min_frequency=binning_info["min_frequency"],
        channel_width=binning_info["channel_width"],
    )
    return swiftly_manager


def predict_residual_visibilities(
    facets: list[np.ndarray],
    visibility_bins_list: list[VisibilityBin],
    binning_info: dict,
    swiftly_manager: Swiftly,
    gridding_manager: Gridder,
    normalisation_factor: float,
) -> None:
    """Degrid (predict) visibilities.

    Args:
        processing_set_manager (ProcessingSetManager): Processing manager for original data set.
        swiftly_manager (Swiftly): SwiFTly manager object.
        gridding_manager (Gridder): Gridder manager for the pipeline.
        facets (np.ndarray): list of image facets to degrid

    Returns:
        visibilities (list[np.ndarray]): list of arrays containing degridded visibilities
    """
    logger.info("Applying degrid corrections to facets...")
    corrected_facets = swiftly_manager.apply_degrid_corrections(
        facets, gridding_manager.get_kernel(), normalisation_factor=normalisation_factor
    )
    swiftly_manager.get_facet_tasks(corrected_facets)
    logger.info("Degridding visibilities...")

    visibility_bins_updated = gridding_manager.predict_residual_visibilities(
        swiftly_manager=swiftly_manager,
        visibility_bins_to_update=visibility_bins_list,
        channel_count=binning_info["channel_count"],
        min_frequency=binning_info["min_frequency"],
        channel_width=binning_info["channel_width"],
    )
    logger.info("Done!")
    return visibility_bins_updated


def save_image(
    pipeline_config: PipelineConfig,
    image_data: np.ndarray,
    image_name: str,
):
    """Constructs an image from full (non facted) imaged ata and exports to file.
    If you are working with facets then first join them using join_facets.

    Args:
        pipeline_config: Configuration for pipeline.
        image_data: Pixel data for the image.
        image_name: Name (prefix) for the output image; .fits, .png extensions will be added automatically.

    Returns:
        None
    """
    utils.save_image(pipeline_config.image_info, image_data, pipeline_config.output_dir, image_name)


def generate_facets_with_corrections(
    swiftly_manager: Swiftly,
    gridding_manager: Gridder,
    total_num_visibilities: int,
    channel_count: int,
):
    """Generate facets with grid corrections applied.

    Args:
        swiftly_manager: SwiFTly object.
        gridding_manager: Gridder object.
        total_num_visibilities: factor used with pixel sum to normalise pixels


    Returns:
        corrected_facets (list[np.ndarray]): list of facets after grid corrections
        normalisation_factor: factor used to normalise pixels
    """

    logger.info("Generating facets with grid corrections...")
    corrected_facets, normalisation_factor = swiftly_manager.generate_grid_corrected_facets(
        gridding_manager.get_kernel(), total_num_visibilities, channel_count
    )
    logger.info("Done!")
    return corrected_facets, normalisation_factor


def generate_clean_beam_parameters(pipeline_config: PipelineConfig, psf: np.ndarray) -> np.ndarray:
    """
    Generates clean beam parameters based on the provided point spread function (PSF) and the pixel size.

    Args:
        pipeline_config: Configuration object containing image information and output directory.
        psf: 2D array representing the point spread function (PSF) of the image.

    Returns:
        dict: A dictionary containing the clean beam parameters:
            - "sigma_x": Standard deviation of the clean beam along the x-axis in pixel units.
            - "sigma_y": Standard deviation of the clean beam along the y-axis in pixel units.
            - "position_angle": Position angle of the clean beam in degrees.
    """
    # Get ImageInfo object and the image width/ height
    image_info = pipeline_config.image_info
    image_width = psf.shape[0]
    image_height = psf.shape[1]

    # Transform the psf array to 4 dimensions
    psf_4d = psf[None, None, :, :]

    wcs = get_wcs_from_image_info(image_info, width_in_pixels=image_width, height_in_pixels=image_height)

    # Get Image object using the transformed psf array and WCS object
    image = create_image_with_wcs(
        psf_4d,
        wcs,
    )

    # Fit the Gaussian parameters to the PSF Image
    clean_beam_parameters = fit_psf(image)

    # Convert to pixel units.
    degrees_per_pixel = image_info.pixel_width_arcsec / 3600
    sigma_x = clean_beam_parameters["bmaj"] / degrees_per_pixel
    sigma_y = clean_beam_parameters["bmin"] / degrees_per_pixel
    position_angle = clean_beam_parameters["bpa"]

    return {"sigma_x": sigma_x, "sigma_y": sigma_y, "position_angle": position_angle}


def generate_clean_beam_array(pipeline_config: PipelineConfig, psf: np.ndarray) -> np.ndarray:
    """
    Generates a clean beam array based on the provided point spread function (PSF) and the pixel size.

    The function calculates the clean beam parameters, then generates the clean beam array.

    Args:
        pipeline_config: Configuration object containing image information and output directory.
        psf: 2D array representing the point spread function (PSF) of the image.

    Returns:
        clean_beam: A 2D array representing the clean beam image generated from the PSF.
    """
    # Find clean beam parameters
    sigma_x, sigma_y, position_angle = generate_clean_beam_parameters(pipeline_config, psf).values()

    image_width = psf.shape[0]
    image_height = psf.shape[1]

    # Generate the clean beam array
    clean_beam = gaussian_2d(
        sigma_x,
        sigma_y,
        position_angle,
        image_width,
        image_height,
    )

    return clean_beam


def restore_model(
    model_facets: list[np.ndarray],
    residual_facets: list[np.ndarray],
    clean_beam_parameters: dict[str, float],
    dask_client: Client,
) -> list[np.ndarray]:
    """
    Convolves model facets with a clean beam then adds the corresponding residual facets.

    Args:
        model_facets: A list of 2D arrays representing the model facets.
        residual_facets: A list of 2D arrays representing the residual facets.
        clean_beam_parameters: A dictionary containing the clean beam parameters:
            - "sigma_x": Standard deviation of the clean beam along the x-axis.
            - "sigma_y": Standard deviation of the clean beam along the y-axis.
            - "position_angle": Position angle of the clean beam in degrees.
        dask_client: The dask client used to distribute the work.

    Returns:
        cleaned_facets: A list of 2D numpy arrays representing the cleaned facets (convolved model + residual).
    """
    gaussian_kernel = Gaussian2DKernel(
        x_stddev=clean_beam_parameters["sigma_x"],
        y_stddev=clean_beam_parameters["sigma_y"],
        theta=clean_beam_parameters["position_angle"],
    )

    cleaned_facet_tasks = []
    for model_facet, residual_facet in zip(model_facets, residual_facets):
        cleaned_facet_task = dask.delayed(
            convolve_fft(
                model_facet,
                gaussian_kernel,
                normalize_kernel=False,
                allow_huge=True,
                boundary="wrap",
            )
            + residual_facet
        )
        cleaned_facet_tasks.append(cleaned_facet_task)

    cleaned_facets = dask_client.compute(cleaned_facet_tasks, sync=True)
    return cleaned_facets


def distributed_hogbom(
    dask_client: Client,
    residual_facets_to_clean: list[np.ndarray],
    psf_image: np.ndarray,
    model_facets_to_update: list[np.ndarray],
    gain: float,
    niter: int,
    fracthresh: float,
) -> list[np.ndarray]:
    """Distribute hogbom over facets via dask.

    Args:
        dask_client: Dask client to distribute tasks on
        residual_facets: residual facets to clean
        psf_image: Single central PSF image of same dimensions as the facets
        model_facets: List of Images (per facet) containing the clean components
        gain: The "loop gain", i.e. the fraction of the brightest pixel that is removed in each iteration
        niter: Maximum number of minor cycles to clean for.
        fracthresh: Fractional threshold at which to stop cleaning. (Computed over entire PSF not single facet)

    Returns:
        residual_facets: List of Images (per facet) containing the residual
    """

    # Calculate a single matching threshold for the entire image
    absolutethresh = fracthresh * np.absolute(residual_facets_to_clean).max()
    logger.info(f"distributed_hogbom: calculated thresh={absolutethresh} from fracthresh={fracthresh}")
    delayed_hogbom_tasks = []
    # Launch a dask delayed task per facet; hogbom is run individually on each facet
    _ = [
        logger.info(f"Sum of model brightness for facet {i} before hogbom: {np.sum(model_facets_to_update[i])}")
        for i in range(len(model_facets_to_update))
    ]

    for residual, model in zip(residual_facets_to_clean, model_facets_to_update):
        hogbom_task = dask.delayed(
            hogbom(
                residual, psf_image, model, window=True, gain=gain, thresh=absolutethresh, niter=niter, fracthresh=0
            ),
            nout=2,
        )
        delayed_hogbom_tasks.append(hogbom_task)
    # Return the computed dask results as regular numpy arrays
    # TODO: These should be delayed but currently join_facets doesn't support that
    hogbom_results = dask_client.compute(delayed_hogbom_tasks, sync=True)
    residual_facets_cleaned = [hogbom_result[0] for hogbom_result in hogbom_results]
    model_facets_updated = [hogbom_result[1] for hogbom_result in hogbom_results]
    _ = [
        logger.info(f"Sum of model brightness for facet {i} after hogbom: {np.sum(model_facets_updated[i])}")
        for i in range(len(model_facets_updated))
    ]
    _ = [
        logger.info(f"Sum of residual brightness for facet {i} after hogbom: {np.sum(residual_facets_cleaned[i])}")
        for i in range(len(residual_facets_cleaned))
    ]
    logger.info(f"Sum of residual facet brightness after cleaning: {np.sum(residual_facets_cleaned)}")
    return residual_facets_cleaned, model_facets_updated


# pylint: disable=unbalanced-tuple-unpacking,C0103
def hogbom(
    residual: np.ndarray,
    psf: np.ndarray,
    model: np.ndarray,
    window: bool,
    gain: float,
    thresh: float,
    niter: int,
    fracthresh: float,
    prefix="",
):
    """Modified function from ska_sdp_func_python/image/cleaners.py

    Clean the point spread function from a residual image.

    See Hogbom CLEAN (1974A&AS...15..417H).

    This version operates on numpy arrays.
    :py:func:'ska_sdp_func_python.image.deconvolution.deconvolve_cube'
    provides a version for Images.

    :param residual: The residual Image, i.e., the Image to be deconvolved
    :param psf: The point spread-function
    :param model: List of Images (per facet) containing the clean components
    :param window: Regions where clean components are allowed.
                    If True, entire residual Image is allowed
    :param gain: The "loop gain", i.e., the fraction of the brightest pixel
                    that is removed in each iteration
    :param thresh: Cleaning stops when the maximum of the absolute deviation
                    of the residual is less than this value
    :param niter: Maximum number of components to make if the
                  threshold `thresh` is not hit
    :param fracthresh: The predefined fractional threshold
                       at which to stop cleaning
    :param prefix: Informational prefix for log messages
    :return: res: array of the updated residual Image
    """

    start_time = time.time()
    assert 0.0 < gain < 2.0
    assert niter > 0

    logger.info(
        "hogbom %s Max abs in residual image = %.6f Jy/beam",
        prefix,
        np.max(np.abs(residual)),
    )
    absolutethresh = max(thresh, fracthresh * np.absolute(residual).max())
    logger.info(f"hogbom {prefix} Start of minor cycle")
    logger.info(
        f"hogbom {prefix} This minor cycle will stop at {niter} iterations or peak < {absolutethresh:.6f} (Jy/beam)"
    )

    res = np.array(residual, copy=True)
    model = np.array(model, copy=True)
    pmax = psf.max()
    np.testing.assert_approx_equal(pmax, 1.0, err_msg=f"PSF does not have unit peak {pmax}")
    logger.info(
        f"hogbom {prefix}: Timing for setup: {time.time()-start_time:.3f}"
        + f" (s) for residual shape {residual.shape}, PSF shape {psf.shape}"
    )
    starttime = time.time()
    a_iter = 0
    for i in range(niter):
        a_iter = i + 1
        if window is not None:
            mx, my = np.unravel_index((np.absolute(res * window)).argmax(), residual.shape)
        else:
            mx, my = np.unravel_index((np.absolute(res)).argmax(), residual.shape)
        mval = res[mx, my] * gain / pmax
        model[mx, my] += mval
        a1o, a2o = overlapIndices(residual, psf, mx, my)
        if niter < 10 or i % (niter // 10) == 0:
            logger.info(
                "hogbom %s Minor cycle %d, peak %s at [%s, %s]",
                prefix,
                i,
                res[mx, my],
                mx,
                my,
            )
        res[a1o[0] : a1o[1], a1o[2] : a1o[3]] -= psf[a2o[0] : a2o[1], a2o[2] : a2o[3]] * mval
        if np.abs(res[mx, my]) < 0.9 * absolutethresh:
            logger.info(
                "hogbom %s Stopped at iteration %d, peak %s at [%s, %s]",
                prefix,
                i,
                res[mx, my],
                mx,
                my,
            )
            break
    logger.info("hogbom %s End of minor cycle", prefix)

    dtime = time.time() - starttime
    logger.info(
        f"{prefix} Timing for clean: {dtime:.3f} (s) for residual {residual.shape}, PSF {psf.shape} , "
        + f"{a_iter} iterations, time per clean {1000.0 * dtime / a_iter:.3f} (ms)"
    )

    return res, model


def split_image(image_data: np.ndarray, facet_count: int) -> list[np.ndarray]:
    """
    Split an image into a specified number of square facets.

    Args:
        image_data: A 2D square array representing the image to be split.
        facet_count: The number of square facets to split the image into.

    Returns:
        facets: A list of 2D arrays, each representing a facet of the original image.
    """
    facets = utils.split_square_array(image_data, facet_count)
    facets = [facet.astype(np.complex128) for facet in facets]

    return facets


def generate_facets_from_image_data(
    fits_path: Union[Path, str], image_size: int, facet_count: int
) -> list[np.ndarray]:  # pragma: no cover
    """Generate facets from a FITS image file.

    Args:
        fits_path: The file path to the FITS file.
        image_size: The size of the image length in pixels.
        facet_count: Total number of facets.

    Returns:
        facets: List of image facets.
    """
    # Get the image data from FITS file.
    image_data = load_image_data_from_fits(fits_path, image_size)

    # Split the image data into facets.
    facets = split_image(image_data, facet_count)

    return facets


def validate_visibility_bins(visibility_bins: list[VisibilityBin]) -> bool:
    """Task to validate visibility data.

    Checks for negative values and NaNs.

    Args:
        data: array of data to validate
    Return:
        data_ok: True if all validation checks pass, otherwise False
    """
    for visibility_bin in visibility_bins:
        logger.info("Validating observed visibility data...")
        validate_data(da.from_array(visibility_bin.obs_vis).astype(np.complex128))
        logger.info("Validating observed visibility data...DONE!")

        logger.info("Validating model visibility data...")
        validate_data(da.from_array(visibility_bin.res_vis).astype(np.complex128))
        logger.info("Validating model visibility data...DONE!")
