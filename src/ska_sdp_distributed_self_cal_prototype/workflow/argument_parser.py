"""Module for parsing commandline arguments."""

import argparse
from pathlib import Path
from typing import Optional

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger

logger = setup_logger(__name__)


def parse_cli_arguments(arg_list: Optional[list[str]] = None) -> argparse.Namespace:  # pragma: no cover
    """Parse command-line arguments.

    This function defines and parses the command-line arguments needed for a pipeline.

    Returns:
        args: An object containing the parsed command-line arguments.

    Command-line Arguments:
        --config-file: Path to the configuration file.
        --dask-address: Address of the existing Dask scheduler.
        --pipeline-name: Name of pipeline to run, e.g. "dirty_image".
    """
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--config-file", required=True, type=Path, help="Path to the configuration file")
    parser.add_argument("--pipeline-name", required=True, type=str, help="Name of pipeline to run")
    args = parser.parse_args(arg_list)
    return args
