"""Module for logging."""

import logging

from ska_sdp_distributed_self_cal_prototype.units import sky_fraction2asec
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig


def log_imaging_parameters(logger: logging.Logger, pipeline_config: PipelineConfig) -> None:
    """Log information about image and subgrid sizes.

    Args:
        logger: Construct using setup_logger.
        pipeline_config: Must be already populated with SwiFTly, gridder and image info.

    Returns:
        None.
    """

    swiftly_info = pipeline_config.swiftly_info
    gridder_info = pipeline_config.gridder_info

    # Image sizes (image_scale_padded, image_scale) are in a planar projection, so we need some trigonometry to derive
    # sizes in radians
    # The SKA is going to have large fields of view - this matters!
    image_size = swiftly_info.image_size
    image_scale = gridder_info.image_scale
    pixel_scale = gridder_info.pixel_scale
    subgrid_size_effective = swiftly_info.subgrid_size_effective
    facet_size_effective = swiftly_info.facet_size_effective

    grid_width = 1 / pixel_scale

    logger.info("Image info:")
    logger.info(f"Image/grid dimensions:   ({image_size}, {image_size}) pixels.")
    logger.info(f"Image size:              {image_scale:.2e} sky fractions.")
    logger.info(f"                         {sky_fraction2asec(image_scale):.3f} arcseconds.")
    logger.info(f"Pixel size:              {pixel_scale:.2e} sky fraction per pixel.")
    logger.info(f"Grid size:               {grid_width:.2e} wavelengths.")
    logger.info(f"Grid point size:         {grid_width / image_size:.2e} wavelengths per grid point.")
    logger.info(f"Facet dimensions:        ({facet_size_effective}, {facet_size_effective}) pixels.")
    logger.info(f"Facet size:              {(facet_size_effective/image_size) * image_scale:.2e} sky fractions.")
    logger.info(f"Subgrid dimensions:      ({subgrid_size_effective}, {subgrid_size_effective}) grid points.")
    logger.info(f"Subgrid size:            {(subgrid_size_effective/image_size) * grid_width:.2e} wavelengths.")
    logger.info(f"                         {sky_fraction2asec(pixel_scale):.3f} arcseconds per pixel.")
    logger.info(f"Facet count:             {swiftly_info.facet_count}.")
    logger.info(f"Subgrid count:           {swiftly_info.subgrid_count}.")
    logger.info(f"w-tower size:            {gridder_info.wtower_size}.")


def log_elapsed_time(logger: logging.Logger, start_time: float, end_time: float) -> None:
    """Log the time taken to run the pipeline.

    Args:
        logger: Logger object.
        start_time: Time at the start of the pipeline.
        end_time: Time once the pipeline has finished.

    Returns:
        None.
    """
    time_taken = end_time - start_time
    hours, remaining_seconds = divmod(time_taken, 3600)
    minutes, seconds = divmod(remaining_seconds, 60)
    logger.info("Elapsed time: %d hours, %d minutes, %d seconds.", int(hours), int(minutes), int(seconds))
