"""Module for unit conversions.

Internal Units:
All variables are stored as the sky fraction. Functions from this module should be used to convert to other units
like arcseconds.

Sky Fraction Definition:
The sky fraction units used here are often called "direction cosines" elsewhere in the SKAO. This is incorrect. The
sky fraction is defined by the following. The angle, in degrees, between the centre of the image and its left edge is
`x_1`. The angle between the centre of the image and its right edge is `x_2`. The sky fraction of the image is then
defined as `(x_2 - x_1) / 90.0`. An image of the whole sky has sky fraction of 2 and corresponds to the range
[-90º, 90º].

"""

import numpy as np


def deg2asec(value: float = 1.0) -> float:
    """Converts a float in degrees to arcseconds.

    Args:
        value: Number in degrees.

    Returns:
        Value in arcseconds.
    """
    return value * 3600.0


def asec2deg(value: float = 1.0) -> float:
    """Converts a float in arcseconds to degrees.

    Args:
        value: Number in arcseconds.

    Returns:
        Value in degrees.
    """
    return value / 3600.0


def asec2rad(value: float = 1.0) -> float:
    """Converts a float in arcseconds to radians.

    Args:
        value: Number in arcseconds.

    Returns:
        Value in radians.
    """
    return np.deg2rad(asec2deg(value))


def amin2asec(value: float = 1.0) -> float:
    """Converts a float in arcminutes to arcseconds.

    Args:
        value: Number in arcminutes.

    Returns:
        Value in arcseconds.
    """
    return value * 60.0


def asec2amin(value: float = 1.0) -> float:
    """Converts a float in arcseconds to arcminutes.

    Args:
        value: Number in arcseconds.

    Returns:
        Value in arcminutes.
    """
    return value / 60.0


def asec2sky_fraction(value: float = 1.0) -> float:
    """Converts a float in arcseconds to the sky fraction.

    Args:
        value: Number in arcseconds.

    Returns:
        Value in sky fractions.
    """
    return asec2deg(value) / 90.0


def sky_fraction2asec(value: float = 1.0) -> float:
    """Converts the sky fraction float to an angle in arcseconds.

    Args:
        value: Sky fraction.

    Returns:
        Value in arcseconds.
    """
    return deg2asec(value * 90.0)


def rad2asec(value: float = 1.0) -> float:
    """Converts a float in radians to arcseconds.

    Args:
        value: Number in radians.

    Returns:
        Value in arcseconds.
    """
    return deg2asec(np.rad2deg(value))
