# flake8: noqa
# pylint: skip-file
# flake8: noqa
# pylint: skip-file
# fmt: off
# isort: skip
"""Entry point to the pipeline"""

import time
from typing import Callable

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.logging_utils import log_elapsed_time
from ska_sdp_distributed_self_cal_prototype.workflow.argument_parser import parse_cli_arguments
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.clean_hogbom import clean_hogbom
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.clean_hogbom_with_degridding import (
    clean_hogbom_with_degridding,
)
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.continuum_imaging_pipeline import continuum_imaging
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.dirty_image_pipeline import dirty_image_pipeline
from ska_sdp_distributed_self_cal_prototype.workflow.pipelines.psf_and_dirty_image import psf_and_dirty_image

logger = setup_logger(__name__)

defined_pipelines: dict[str, Callable] = {
                                          "dirty_image": dirty_image_pipeline,
                                          "dirty_beam_and_psf": psf_and_dirty_image,
                                          "clean_hogbom": clean_hogbom,
                                          "clean_hogbom_with_degridding": clean_hogbom_with_degridding,
                                          "continuum_imaging": continuum_imaging,
                                         }


def main(arg_list: list[str] | None = None) -> None:
    """Execute pipeline specified on CLI.

    Args:
        None.

    Returns:
        None.

    The expected commandline parameters include:
        - config-file: Directory path for the YAML config file
        - pipeline-name: name of pipeline to run
    """
    start_time = time.time()
    cli_args = parse_cli_arguments(arg_list)

    if hasattr(cli_args, "pipeline_name"):
        try:
            run_pipeline = defined_pipelines[cli_args.pipeline_name]
            run_pipeline(cli_args.config_file)
        except KeyError:
            logger.error(f"Pipeline {cli_args.pipeline_name} not recognised!")
    else:
        # TODO: Enable custom pipelines here
        logger.error("pipeline_name must be defined")

    end_time = time.time()
    log_elapsed_time(logger, start_time, end_time)



if __name__ == "__main__":
    main()
