"""Module for orchestrating gridding tasks."""

# Ignore todos for now
# pylint: disable=W0511

import dask.array as da
import numpy as np
from ska_sdp_func.grid_data.gridder_wtower_uvw import GridderWtowerUVW

from ska_sdp_distributed_self_cal_prototype.data_managers.swiftly import Swiftly
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.processing_tasks._gridding_utils import _fft, _ifft
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import GridderInfo, PipelineConfig, SwiftlyInfo

logger = setup_logger(__name__)


class Gridder:
    """Class that stores the gridding kernel and orchestrates gridding tasks.

    Args:
        config: Config describing the pipeline.
    """

    def __init__(self, config: PipelineConfig):
        self._config: PipelineConfig = config

    def __getstate__(self):
        return {"_config": self._config}

    def __setstate__(self, attrs):
        self._config = attrs["_config"]

    def get_kernel(self) -> GridderWtowerUVW:
        """Creates a new instance of the gridding kernel.

        Returns:
            kernel: The gridding kernel used for correcting facet images.
        """

        # Mapping of valid gridder names to class constructors
        names_to_objects = {"wtowers": GridderWtowerUVW}

        # Get config dataclasses
        gridder_info: GridderInfo = self._config.gridder_info
        swiftly_info: SwiftlyInfo = self._config.swiftly_info

        # Get gridding class
        gridder_name = gridder_info.name
        gridder = names_to_objects[gridder_name]
        kernel = gridder(
            image_size=swiftly_info.image_size,
            subgrid_size=swiftly_info.subgrid_size,
            theta=gridder_info.image_scale_padded,
            w_step=gridder_info.w_step,
            shear_u=gridder_info.shear_u,
            shear_v=gridder_info.shear_v,
            support=gridder_info.support,
            oversampling=gridder_info.oversampling,
            w_support=gridder_info.w_support,
            w_oversampling=gridder_info.w_oversampling,
        )
        return kernel

    def grid_psf(
        self,
        swiftly_manager: Swiftly,
        visibility_bins: list[VisibilityBin],
        subgrid_size: int,
        min_frequency: float,
        channel_width: float,
    ) -> None:
        """Add gridding tasks for psf for multiple subgrids to SwiFTly.

        This function generates and adds tasks for gridding psf data onto subgrids,
        where each task processes a portion of the UVW coordinates and psf
        data.

        Args:
            swiftly_manager: Swiftly manager for the pipeline.
            visibility_bins: List of VisibilityBin objects holding binned visibility data.
            subgrid_size: Size of subgrids.
            min_frequency: Minimum frequency in Hz across dataset.
            channel_width: Channel width in Hz.

        Returns:
            None.
        """
        for visibility_bin in visibility_bins:
            new_subgrid_task = self._gridding_task_psf(
                visibility_bin,
                subgrid_size,
                min_frequency,
                channel_width,
            )
            swiftly_manager.backward.add_new_subgrid_task(visibility_bin.subgrid_config, new_subgrid_task)

    def grid_visibilities(
        self,
        swiftly_manager: Swiftly,
        visibility_bins: list[VisibilityBin],
        subgrid_size: int,
        min_frequency: float,
        channel_width: float,
    ) -> None:
        """Add gridding tasks for multiple subgrids to SwiFTly.

        This function generates and adds tasks for gridding data onto subgrids,
        where each task processes a portion of the UVW coordinates and visibility
        data.

        Args:
            swiftly_manager: Swiftly manager for the pipeline.
            visibility_bins: List of VisibilityBin objects holding binned visibility data.
            subgrid_size: Size of subgrids.
            min_frequency: Minimum frequency in Hz across dataset.
            channel_width: Channel width in Hz.

        Returns:
            None.
        """
        for visibility_bin in visibility_bins:
            new_subgrid_task = self._gridding_task_visibilities(
                visibility_bin,
                subgrid_size,
                min_frequency,
                channel_width,
            )
            swiftly_manager.backward.add_new_subgrid_task(visibility_bin.subgrid_config, new_subgrid_task)

    def _gridding_task_visibilities(
        self, visibility_bin: VisibilityBin, subgrid_size: int, min_frequency: float, channel_width: float
    ):
        """Perform gridding and FFT on the visibility data for a subgrid.

        This function loads visibility data from the dataset, grids it onto a
        subgrid, and performs a Fourier transform to produce an image.

        Args:
            visibility_bin: VisibilityBin object containing binned visibility data for gridding.
            subgrid_size: The side length of the subgrid.
            min_frequency: The minimum frequency.
            channel_width: The width of each frequency channel.

        Returns:
            The Fourier transformed subgrid image.
        """
        logger.info(
            f"Gridding visibilities for subgrid "
            f"({visibility_bin.subgrid_config.off0}/{visibility_bin.subgrid_config.off1})..."
        )
        kernel = self.get_kernel()
        subgrid_image = np.zeros((subgrid_size, subgrid_size), dtype=np.complex128)
        kernel.grid_subgrid(
            vis=visibility_bin.res_vis.compute(),
            uvw=visibility_bin.uvw,
            start_chs=visibility_bin.start_channels,
            end_chs=visibility_bin.end_channels,
            ch_count=visibility_bin.channel_count,
            freq0_hz=min_frequency,
            dfreq_hz=channel_width,
            subgrid_image=subgrid_image,
            subgrid_offset=(visibility_bin.subgrid_config.off0, visibility_bin.subgrid_config.off1, 0),
        )
        return _fft(subgrid_image)

    def _gridding_task_psf(
        self, visibility_bin: VisibilityBin, subgrid_size: int, min_frequency: float, channel_width: float
    ):
        """Perform gridding and FFT on the psf for a subgrid.

        This function generates psf visibilities for the subgrid, grids it,
        and performs a Fourier transform to produce an image.

        Args:
            visibility_bin: VisibilityBin object containing binned visibility data for gridding.
            subgrid_size: The side length of the subgrid.
            min_frequency: The minimum frequency.
            channel_width: The width of each frequency channel.

        Returns:
            The Fourier transformed subgrid image.
        """
        logger.info(
            f"Gridding PSF for subgrid ({visibility_bin.subgrid_config.off0}/{visibility_bin.subgrid_config.off1})..."
        )
        kernel = self.get_kernel()
        psf_vis = da.ones(np.shape(visibility_bin.obs_vis), dtype=np.complex128).compute()
        subgrid_image = np.zeros((subgrid_size, subgrid_size), dtype=np.complex128)
        kernel.grid_subgrid(
            vis=psf_vis,
            uvw=visibility_bin.uvw,
            start_chs=visibility_bin.start_channels,
            end_chs=visibility_bin.end_channels,
            ch_count=visibility_bin.channel_count,
            freq0_hz=min_frequency,
            dfreq_hz=channel_width,
            subgrid_image=subgrid_image,
            subgrid_offset=(visibility_bin.subgrid_config.off0, visibility_bin.subgrid_config.off1, 0),
        )
        return _fft(subgrid_image)

    def predict_residual_visibilities(
        self,
        swiftly_manager: Swiftly,
        visibility_bins_to_update: list[VisibilityBin],
        channel_count: int,
        min_frequency: float,
        channel_width: float,
    ) -> list[VisibilityBin]:
        """Add degridding tasks for multiple subgrids from SwiFTly.

        This function generates and adds tasks for degridding data from subgrids,
        via swiftly, where each task produces a portion of the visibility data.
        Uses subgrid config stored in swiftly manager to define subgrids.

        Args:
            swiftly_manager: Swiftly manager with facet tasks
            visibility_bins: List of VisibilityBin objects containing binned visibility data.
            channel_count: Number of channels.
            min_frequency: Minimum frequency (Hz).
            channel_width: Channel width (Hz).

        Returns:
            visibility_bins: List of VisibilityBin objects containing tasks to predict model visibilities.
        """
        updated_visibility_bins = []
        for visibility_bin in visibility_bins_to_update:
            subgrid_image = swiftly_manager.forward.get_subgrid_task(visibility_bin.subgrid_config).compute()
            visibility_bin.update_residual_visibilities(
                self._degridding_task(
                    visibility_bin,
                    subgrid_image,
                    channel_count,
                    min_frequency,
                    channel_width,
                )
            )
            updated_visibility_bins.append(visibility_bin)
        return updated_visibility_bins

    def _degridding_task(
        self,
        visibility_bin: VisibilityBin,
        subgrid_image: np.ndarray,
        channel_count: int,
        min_frequency: float,
        channel_width: float,
    ) -> np.ndarray:
        """Degrid a subgrid image to generate visibilities.

        This function degrids a subgrid image into
        visibility (u, v, w) space.

        Args:
            visibility_bin: VisibilityBin object containing visibility data.
            subgrid_image: 2D subgrid image to degrid.
            channel_count: The number of frequency channels.
            min_frequency: The minimum frequency.
            channel_width: The width of each frequency channel.

        Returns:
            visibilities: Degridded visibilities.
        """
        logger.info(
            f"Degridding subgrid image from subgrid "
            f"({visibility_bin.subgrid_config.off0} / {visibility_bin.subgrid_config.off1})..."
        )
        kernel = self.get_kernel()
        visibilities = kernel.degrid_subgrid(
            subgrid_image=_ifft(subgrid_image),
            subgrid_offset=(visibility_bin.subgrid_config.off0, visibility_bin.subgrid_config.off1, 0),
            ch_count=channel_count,
            freq0_hz=min_frequency,
            dfreq_hz=channel_width,
            uvws=visibility_bin.uvw,
            start_chs=visibility_bin.start_channels,
            end_chs=visibility_bin.end_channels,
            vis=None,
        )
        return visibilities

    @property
    def config(self):
        """Returns the config attribute."""
        return self._config
