"""Module for utility functions used for gridding."""

from typing import Optional

import numpy as np


def _lm_to_n(l_sky_coord: float, m_sky_coord: float, shear_u: float, shear_v: float) -> float:  # pragma: no cover
    """
    Calculate the coordinate on the sky sphere towards the phase center, given l & m coordinates and shear factors.

    This function computes the coordinate on the sky sphere, `n`, based on the horizontal and vertical sky coordinates
    (`l` and `m`), and shear factors (`u` and `v`). If the shear factors are zero, it handles the simplest case where
    the calculation is straightforward. If shear factors are non-zero, it adjusts the coordinates accordingly.

    Args:
        l_sky_coord: The horizontal sky coordinate.
        m_sky_coord: The vertical sky coordinate.
        shear_u: The horizontal shear factor.
        shear_v: The vertical shear factor.

    Returns:
        n_coord: The coordinate towards the phase center on the sky sphere.
    """
    if shear_u == 0 and shear_v == 0:
        # Easy case - no shear factors
        n_coord = np.sqrt(1 - l_sky_coord**2 - m_sky_coord**2) - 1
    else:
        # Sheared case
        hul_hvm_1 = shear_u * l_sky_coord + shear_v * m_sky_coord - 1
        hu2_hv2_1 = shear_u**2 + shear_v**2 + 1
        n_coord = (
            np.sqrt(hul_hvm_1 * hul_hvm_1 - hu2_hv2_1 * (l_sky_coord**2 + m_sky_coord**2)) + hul_hvm_1
        ) / hu2_hv2_1
    return n_coord


def _determine_w_step(
    image_scale_padded: float,
    image_scale: float,
    shear_u: float,
    shear_v: float,
    window_function_fraction: Optional[float] = None,
) -> float:
    """
    Determine the step size along the w-axis for a given field of view and shear factors.

    This function calculates the step size in grid space for the w-axis based on the given field of view (image_scale),
    image size in sky fractions (image_scale_padded), and shear factors. If a window function fraction is
    provided, it adjusts the calculation accordingly. If not provided, it defaults to using the ratio of image_scale
    to image_scale_padded.

    Args:
        image_scale_padded: Size of the image in sky fractions.
        image_scale: Field of view.
        shear_u: Horizontal shear factor.
        shear_v: Vertical shear factor.
        window_function_fraction: Fraction of the window function used along the w-axis relative
            to the uv-axes. Defaults to image_scale/image_scale_padded if not provided.

    Returns:
        The step size along the w-axis in grid space.
    """
    # Default window function usage fraction to image_scale/image_scale_padded
    # (i.e., we utilize as much of the PSWF along the w-axis
    # as along the uv-axes. Appropriate if we utilize the
    # same window function along both axes)
    if window_function_fraction is None:
        window_function_fraction = image_scale / image_scale_padded

    # Determine maximum used n, deduce image_scale_padded along n-axis
    image_scale_n = 2 * -min(
        _lm_to_n(-image_scale / 2, -image_scale / 2, shear_u, shear_v),
        _lm_to_n(image_scale / 2, -image_scale / 2, shear_u, shear_v),
        _lm_to_n(-image_scale / 2, image_scale / 2, shear_u, shear_v),
        _lm_to_n(image_scale / 2, image_scale / 2, shear_u, shear_v),
    )
    image_scale_padded_n = image_scale_n / window_function_fraction

    # image_scale_padded_n is our size in image space; therefore, 1/image_scale_padded_n is our step length in
    # grid space
    return 1 / image_scale_padded_n


def _fft(array: np.ndarray) -> np.ndarray:  # pragma: no cover
    """Computes the Fourier Transform of a given array, with appropriate shifts applied.

    This function computes the Fourier Transform of an input array using the Fast Fourier Transform (FFT). If the input
    array is 2-dimensional, it computes the 2D FFT; otherwise, it computes the 1D FFT. The function applies a shift such
    that the zero-frequency component is centered in the spectrum.

    Args:
        array: The input array for which the Fourier Transform is to be computed. It can be either a 1D or
            2D numpy array.

    Returns:
        The Fourier Transformed array, with the zero-frequency component shifted to the center of the
            spectrum.
    """
    if len(array.shape) == 2:
        return np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(array)))
    return np.fft.fftshift(np.fft.fft(np.fft.ifftshift(array)))


def _ifft(array: np.ndarray) -> np.ndarray:  # pragma: no cover
    """Computes the Inverse Fourier Transform of a given array, with appropriate shifts applied.

    This function computes the Inverse Fourier Transform of an input array using the Inverse Fast Fourier Transform
    (IFFT). If the input array is 2-dimensional, it computes the 2D IFFT; otherwise, it computes the 1D IFFT.

    The function applies a shift such that the zero-frequency component is centered in the spectrum.

    Args:
        array: The input array for which the Inverse Fourier Transform is to be computed. It can be either
            a 1D or 2D numpy array.

    Returns:
        The Inverse Fourier Transformed array, with the zero-frequency component shifted to the center of
            the spectrum.
    """
    if len(array.shape) == 2:
        return np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(array)))
    return np.fft.fftshift(np.fft.ifft(np.fft.ifftshift(array)))
