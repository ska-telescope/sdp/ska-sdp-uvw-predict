"""Module containing functions for binning visibility data."""

import dask
import dask.array as da
import numpy as np
import xarray as xr
from ska_sdp_exec_swiftly import SubgridConfig

from ska_sdp_distributed_self_cal_prototype.constants import SPEED_OF_LIGHT
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger

logger = setup_logger(__name__)


def bin_visibilities(dataset: xr.Dataset, subgrid_config_list: list[SubgridConfig], binning_info: dict) -> list:
    """Bin visibility data.

    Takes a subgrid configuration and a dataset and extracts visibilities, uvw and start and end
    channels.

    Args:
        dataset: xarray dataset containing visibilities and uvw
        subgrid_config_list: list of configurations for subgrids
        binning_info: dictionary containing global parameters for subgridding
    Returns:
        visibility_bins_delayed: list of dask tasks to generate VisibilityBin instances populated
                                 with binned visibility data
    """

    vis_name = binning_info["vis_name"]
    uvw = get_uvw(dataset, flatten=True)
    visibilities = get_visibilities(dataset=dataset, vis_name=vis_name, stokes_i=True, flatten=True)
    visibility_bins_delayed = []
    for subgrid_config in subgrid_config_list:
        visibility_bin_delayed = dask.delayed(get_visibility_bin_data)(subgrid_config, visibilities, uvw, binning_info)
        visibility_bins_delayed.append(visibility_bin_delayed)
    return visibility_bins_delayed


def get_visibility_bin_data(
    subgrid_config: list[SubgridConfig], visibilities: da.Array, uvw: da.Array, binning_info: dict
) -> VisibilityBin:
    """Extract visibility data for a subgrid configuration.

    Takes a subgrid configuration and a dataset and extracts visibilities, uvw and start and end
    channels. Returns a VisibilityBin instance containing binned data.

    Args:
        subgrid_config: Configuration for subgrid.
        visibilities: Unrolled visibility data to mask.
        uvw: Unrolled uvw data to mask.
        binning_info: Dictionary containing global parameters for subgridding.
    Returns:
        visibility_bin: VisibilityBin instance populated with binned visibility data.
    """
    image_scale_padded = binning_info["image_scale_padded"]
    wtower_size_uv = binning_info["subgrid_size_effective"] / image_scale_padded
    wtower_size_w = binning_info["wtower_size"] * binning_info["w_step"]
    channel_count = binning_info["channel_count"]
    min_frequency = binning_info["min_frequency"]
    channel_width = binning_info["channel_width"]
    min_uvw, max_uvw = _wtower_bounding_box(
        subgrid_offset_u_px=subgrid_config.off0,
        subgrid_offset_v_px=subgrid_config.off1,
        wtower_size_uv=wtower_size_uv,
        wtower_size_w=wtower_size_w,
        image_scale_padded=image_scale_padded,
    )
    start_channels, end_channels, n_visibilities = _get_channels(
        uvw, min_uvw, max_uvw, min_frequency, channel_width, channel_count
    )
    logger.info(f"Subgrid ({subgrid_config.off0}/{subgrid_config.off1}): {n_visibilities} visibilities.")
    if n_visibilities == 0:
        logger.warning("No visibilities to grid")
        visibility_bin = None
    else:
        mask = end_channels > start_channels
        visibility_bin = VisibilityBin(
            subgrid_config=subgrid_config,
            vis_data=visibilities[mask],
            uvw_data=uvw[mask],
            channel_count=channel_count,
            start_channels=start_channels[mask],
            end_channels=end_channels[mask],
        )
    return visibility_bin


def get_uvw(dataset: xr.Dataset, flatten: bool = False) -> np.ndarray:
    """Get UVW data from visibility partition.

    Args:
        dataset: Xarray dataset with uvw data.
        flatten: If True flatten times and baselines into one dimension, default = False.

    Returns:
        uvw_values: Numpy array containing uvw values.
    """
    uvw_values = dataset.UVW.data
    if isinstance(uvw_values, np.ndarray):
        uvw_values = da.from_array(uvw_values)
    if flatten:
        uvw_values = da.reshape(uvw_values, (-1, 3)).astype(np.float64)
    return uvw_values


def get_visibilities(dataset: xr.Dataset, vis_name: str, stokes_i: bool = False, flatten: bool = False) -> da.Array:
    """Get visibility data from visibility partition.

    Args:
        dataset: Xarray dataset with visibility data.
        vis_name: Name of column containing visibility data.
        stokes_i: If True return stokes i instead of raw visibilities.
        flatten: If True flatten times and baselines.

    Returns:
        visibilities: Dask array containing visibility values.
    """
    num_frequencies = dataset.sizes["frequency"]
    num_polarizations = dataset.sizes["polarization"]
    flatten_shape = (-1, num_frequencies, num_polarizations)
    if stokes_i and (num_polarizations > 1):
        flatten_shape = (-1, num_frequencies)
        visibilities = _compute_stokes_i(dataset, vis_name)
    else:
        visibilities = dataset[vis_name].data
    if flatten:
        visibilities = da.reshape(visibilities, flatten_shape)
    return visibilities


def _get_channels(
    uvw: np.ndarray,
    min_uvw: np.ndarray,
    max_uvw: np.ndarray,
    min_frequency: float,
    channel_width: float,
    channel_count: int,
) -> tuple[np.ndarray, np.ndarray, int]:
    """Get channels for generating subgrids from visibility data.

    Args:
        uvw: UVW values in units of metres, second dimension is (u, v, w).
        min_uvw: Minimum UVW value.
        max_uvw: Maximum UVW value.
        min_frequency: Minimum frequency (Hz).
        channel_width: Width of frequency channel (Hz).
        channel_count: Number of frequency channels.

    Returns:
        start_channels: Array of indices for the first channel in the subgrid.
        end_channels: Array of indices for the final channel in the subgrid.
        n_visibilities: The number of visibilities in the subgrid.
    """
    uvw_count = len(uvw)
    start_channels = np.zeros(uvw_count, dtype=int)
    end_channels = channel_count * np.ones(uvw_count, dtype=int)
    start_channels, end_channels = _clamp_channels_multiple(
        uvw,
        min_frequency,
        channel_width,
        start_channels,
        end_channels,
        min_uvw,
        max_uvw,
    )
    start_channels = start_channels.astype(np.int32)
    end_channels = end_channels.astype(np.int32)
    n_visibilities = np.sum(end_channels - start_channels)
    return start_channels, end_channels, n_visibilities


def _wtower_bounding_box(
    subgrid_offset_u_px: int,
    subgrid_offset_v_px: int,
    wtower_size_uv: int,
    wtower_size_w: int,
    image_scale_padded: float,
) -> tuple[list[float], list[float]]:  # pragma: no cover
    """Calculate the bounding box of a tower in the w direction.

    Computes the minimum and maximum coordinates of the bounding box for a tower in the w direction
    given its size, subgrid offset, and the size of the image in sky fractions.

    Args:
        subgrid_offset_u_px: The offset of the subgrid from the center in the u direction, in pixels.
        subgrid_offset_v_px: The offset of the subgrid from the center in the v direction, in pixels.
        wtower_size_uv: The size of the weight tower in the uv-plane.
        wtower_size_w: The size of the weight tower in the w direction.
        image_scale_padded: The size of the image in sky fractions.

    Returns:
        A tuple (min_uvw, max_uvw) where min_uvw is a list containing the minimum coordinates `[min_u, min_v, min_w]`
            of the bounding box, and max_uvw is a list containing the maximum coordinates `[max_u, max_v, max_w]`
            of the bounding box.
    """
    return (
        [
            subgrid_offset_u_px / image_scale_padded - wtower_size_uv / 2,
            subgrid_offset_v_px / image_scale_padded - wtower_size_uv / 2,
            -wtower_size_w / 2,
        ],
        [
            subgrid_offset_u_px / image_scale_padded + wtower_size_uv / 2,
            subgrid_offset_v_px / image_scale_padded + wtower_size_uv / 2,
            wtower_size_w / 2,
        ],
    )


def _clamp_channels_multiple(
    uvws: np.ndarray,
    freq0: float,
    dfreq: float,
    start_chs: np.ndarray,
    end_chs: np.ndarray,
    min_uvw: np.ndarray,
    max_uvw: np.ndarray,
) -> tuple[np.ndarray, np.ndarray]:
    """
    Clamp channel ranges for an array of UVW coordinates.

    This function adjusts the start and end channels for each set of UVW coordinates to ensure they fall within the
    specified bounds. It iterates over the UVW coordinates and applies the clamping operation to restrict the channel
    range.

    Args:
        uvws: A 2D array where each column represents UVW coordinates.
        freq0: Frequency of the first channel.
        dfreq: Channel width.
        start_chs: Array of starting channels to be clamped.
        end_chs: Array of ending channels to be clamped.
        min_uvw: Minimum values for UVW coordinates (inclusive).
        max_uvw: Maximum values for UVW coordinates (exclusive).

    Returns:
        start_channels: Array of indices for the first channel in the subgrid.
        end_channels: Array of indices for the final channel in the subgrid.
    """
    for positions, min_position, max_position in zip(uvws.T, min_uvw, max_uvw):
        start_chs, end_chs = _clamp_channels_single(
            positions, freq0, dfreq, start_chs, end_chs, min_position, max_position
        )
    return start_chs, end_chs


def _compute_stokes_i(
    dataset: xr.Dataset,
    vis_name: str,
) -> da.Array:
    """Compute Stokes I from visibility data.

    I = XX + YY
    OR
    I = RR + LL

    See https://www.atnf.csiro.au/computing/software/atca_aips/node11.html for more detail.

    Args:
        dataset: An xarray DataSet containing polarizations XX and YY or RR and LL.
        vis_name: The name of the DataArray containing visibilities. Must be "VISIBILITY" or "VISIBILITY_CORRECTED".

    Returns:
        stokes_i: A dask array containing Stokes I parameters.
    """
    polarizations = dataset.coords["polarization"].data
    if _is_circularly_polarized(polarizations):
        xx_name, yy_name = ("RR", "LL")
    else:
        xx_name, yy_name = ("XX", "YY")
    xx_visibilities = dataset[vis_name].sel(polarization=xx_name).data
    yy_visibilities = dataset[vis_name].sel(polarization=yy_name).data
    stokes_i = xx_visibilities + yy_visibilities
    return stokes_i


def _is_circularly_polarized(
    polarizations: xr.DataArray,
) -> list[str]:
    circular_polarization = ["RR", "LL"]
    return any(polarization in polarizations for polarization in circular_polarization)


def _clamp_channels_single(
    positions: np.ndarray,
    freq0: float,
    dfreq: float,
    start_chs: np.ndarray,
    end_chs: np.ndarray,
    min_position: float,
    max_position: float,
) -> tuple[np.ndarray, np.ndarray]:
    """
    Clamp channel ranges for an array of positions.

    This function restricts the channel range such that all visibilities lie within the given bounds. It adjusts the
    start and end channels for each position to ensure they fall within the specified minimum and maximum position
    bounds.

    Args:
        positions: Array of positions in meters.
        freq0: Frequency of the first channel.
        dfreq: Channel width.
        start_chs: Array of starting channels to be clamped.
        end_chs: Array of ending channels to be clamped.
        min_position: Minimum values for position (inclusive).
        max_position: Maximum values for position (exclusive).

    Returns:
        start_chs, end_chs: Clamped arrays for the start and end channels. Returns (0, 0) if no channels overlap.
    """
    # Determine positions far away from zero
    eta = 1e-3
    mask = np.abs(positions) > eta

    # Clamp non-zero positions
    initial_position = positions[mask] * (freq0 / SPEED_OF_LIGHT)
    position_step_wavelengths = positions[mask] * (dfreq / SPEED_OF_LIGHT)
    mins = np.ceil((min_position - initial_position) / position_step_wavelengths).astype(int)
    maxs = np.ceil((max_position - initial_position) / position_step_wavelengths).astype(int)
    positive_mask = position_step_wavelengths > 0
    start_chs[mask] = np.maximum(start_chs[mask], np.where(positive_mask, mins, maxs))
    end_chs[mask] = np.minimum(end_chs[mask], np.where(positive_mask, maxs, mins))

    # Clamp zero positions if range doesn't include them
    if min_position > 0 or max_position <= 0:
        start_chs[~mask] = 0
        end_chs[~mask] = 0

    # Normalise
    end_chs = np.maximum(end_chs, start_chs)
    return (start_chs, end_chs)
