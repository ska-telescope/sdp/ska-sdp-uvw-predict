"""Module to store functions for validating data."""

import dask.array as da
import numpy as np

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger

logger = setup_logger(__name__)


def validate_data(data: da.Array) -> bool:
    """Task to validate an array.

    Checks for negative values, NaNs and Infs.

    Args:
        data: Array of data to validate.

    Returns:
        data_ok: True if all validation checks pass, otherwise False.
    """
    logger.info(f"Validating data array with shape {np.shape(data)}")
    all_ge_zero = _array_values_all_ge_zero(data)
    no_nans = _array_values_all_non_nan(data)
    no_infs = _array_values_all_non_inf(data)

    data_ok = all_ge_zero & no_nans & no_infs
    return data_ok


def _array_values_all_ge_zero(array: da.Array) -> bool:
    """Check all values in dask array are >= 0.

    Args:
        array: Array of data to validate.

    Returns:
        all_ge_zero: True if all values >= 0, otherwise False.
    """
    all_ge_zero = (array[~da.isnan(array)] >= 0).all().compute()
    if not all_ge_zero:
        logger.warning("Negative values detected!")
        _log_negative_counts(array)
    return all_ge_zero


def _log_negative_counts(array: da.Array) -> tuple[int, int]:
    """Log counts of negative values.

    Args:
        array: Array of data to validate.

    Returns:
        total: Total number of values.
        total_negatives: Total number of negative values.
    """
    total_negatives = da.sum(array < 0).compute()
    total = array.size
    logger.warning(f"Found {total_negatives}/{total} negative values")
    return total, total_negatives


def _log_nan_counts(array: da.Array) -> tuple[int, int]:
    """Log counts of NaNs.

    Args:
        array: Array of data to validate.

    Returns:
        total: Total number of values.
        total_nans: Total number of NaNs.
    """
    total_nans = da.isnan(array).sum().compute()
    total = array.size
    logger.warning(f"Found {total_nans}/{total} NaNs")
    return total, total_nans


def _array_values_all_non_nan(array: da.Array) -> int:
    """Check there are no NaNs in dask array.

    Args:
        array: Array of data to validate.

    Returns:
        no_nans: True if there are no NaNs, otherwise False.
    """
    nans = da.isnan(array).any().compute()
    if nans:
        logger.warning("NaNs detected!")
        _log_nan_counts(array)
    return not nans


def _array_values_all_non_inf(array: da.Array) -> int:
    """Check there are no Infs in dask array.

    Args:
        array: Array of data to validate.

    Returns:
        no_nans: True if there are no Infs, otherwise False.
    """
    infs = da.isinf(array).any().compute()
    if infs:
        logger.warning("Infs detected!")
        _log_inf_counts(array)
    return not infs


def _log_inf_counts(array: da.Array) -> tuple[int, int]:
    """Log counts of Infs.

    Args:
        array: Array of data to validate.

    Returns:
        total: Total number of values.
        total_nans: Total number of Infs.
    """
    total_infs = da.isinf(array).sum().compute()
    total = array.size
    logger.warning(f"Found {total_infs}/{total} Infs")
    return total, total_infs
