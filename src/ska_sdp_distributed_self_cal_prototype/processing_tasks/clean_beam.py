"""Module for functionality relating to a clean beam"""

import numpy as np
from scipy.stats import multivariate_normal


def gaussian_2d(
    sigma_x: float,
    sigma_y: float,
    position_angle: float,
    x_pixels: int,
    y_pixels: int,
) -> np.ndarray:
    """
    Generates a 2D Gaussian based on parameters and pixel dimensions.

    The function computes a 2D Gaussian using the specified major and minor axes, position angle, and pixel
    size. It accounts for the rotation by applying a position angle transformation and returns a normalized
    Gaussian with a peak value of 1.

    Args:
        sigma_x: Standard deviation of the Gaussian in the x direction.
        sigma_y: Standard deviation of the Gaussian in the y direction.
        position_angle: Position angle of the Gaussian in degrees.
        x_pixels: Number of pixels in the x direction (image width).
        y_pixels: Number of pixels in the y direction (image height).

    Returns:
        gaussian_values: A 2D array representing the normalized Gaussian.
    """
    # Convert position angle to radians
    position_angle_rad = np.deg2rad(position_angle)

    # Rotation matrix for position angle
    cos_position_angle = np.cos(position_angle_rad)
    sin_position_angle = np.sin(position_angle_rad)
    rotation_matrix = np.array([[cos_position_angle, -sin_position_angle], [sin_position_angle, cos_position_angle]])

    # Covariance matrix in pixel units
    cov_matrix = np.diag([sigma_x**2, sigma_y**2])
    rotated_cov = rotation_matrix @ cov_matrix @ rotation_matrix.T

    # Create grid of coordinates in pixel units
    x_values = np.linspace(-x_pixels // 2, x_pixels // 2, x_pixels)
    y_values = np.linspace(-y_pixels // 2, y_pixels // 2, y_pixels)

    # Stack coordinates into shape (N, 2) where N = x_pixels * y_pixels
    pixel_coordinates = np.dstack((np.meshgrid(x_values, y_values)))

    # Create multivariate Gaussian
    gaussian = multivariate_normal([0, 0], rotated_cov)
    gaussian_values = gaussian.pdf(pixel_coordinates)

    # Normalize the array so the peak is 1
    gaussian_values /= np.max(gaussian_values)

    return gaussian_values
