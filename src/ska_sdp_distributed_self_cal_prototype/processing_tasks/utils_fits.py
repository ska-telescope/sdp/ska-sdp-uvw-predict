"""Module for FITS utility functions."""

from pathlib import Path
from typing import Tuple, Union

import astropy
import numpy as np
from astropy.coordinates import SkyCoord, frame_transform_graph
from astropy.io import fits
from astropy.wcs import WCS
from ska_sdp_datamodels.image.image_model import Image
from ska_sdp_datamodels.science_data_model import PolarisationFrame

from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import ImageInfo


# pylint: disable = E1101
def load_image_data_from_fits(fits_path: Union[Path, str], image_size: int) -> np.ndarray:
    """Load image data from a FITS file with appropriate metadata applied.

    This function reads a FITS (Flexible Image Transport System) file and extracts
    specific header information along with image data centered at a specified
    right ascension and declination. The image size is determined by the provided
    `image_size` parameter and the pixel scale (`CDELT2`) from the FITS header.

    Args:
        fits_path: The file path to the FITS file.
        image_size: The size of the image length in pixels, which is multiplied by the pixel scale to determine
            the final size in degrees.

    Returns:
        image_data: A 2D array of the image data.
    """
    with fits.open(fits_path) as fits_file:
        primary_hdu = fits_file[0]
    right_ascension = primary_hdu.header["CRVAL1"]
    declination = primary_hdu.header["CRVAL2"]
    size = image_size * primary_hdu.header["CDELT2"]  # size of image in degrees. Currently set to no cropping

    hdu_list = extract_subimage(fits_path, right_ascension, declination, size)
    image_data = hdu_list[0].data

    return image_data


def _flatten(
    hdu: fits.PrimaryHDU, x_centre: float, y_centre: float, size: float, channel: int = 0, freq_axis: int = 3
) -> fits.HDUList:
    """Flatten a FITS file so that it becomes a 2D image.

    Args:
        hdu: FITS header object.
        x_centre: Centre of the image along the first dimension in pixel units.
        y_centre: Centre of the image along the second dimension in pixel units.
        size: Number of pixels along the side of the image.
        channel: Optional frequency channel number to select.
        freq_axis: Optional integer that corresponds to the frequency dimension of the image.

    Returns:
        hdu_list: A HDU list containing the extracted subimage.
    """
    naxis = hdu.header["NAXIS"]
    if naxis < 2:
        raise ValueError("The number of dimensions are less than 2, so the FITS file can't be flattened")

    y_pixels, x_pixels = hdu.data.shape[-2:]
    xmin = max(0, int(x_centre - size))
    xmax = min(x_pixels, int(x_centre + size))
    ymin = max(0, int(y_centre - size))
    ymax = min(y_pixels, int(y_centre + size))
    if ymax <= ymin or xmax <= xmin:
        raise ValueError("Failed to make subimage - the required position is not on the map.")

    wcs_original = WCS(hdu.header)
    wcs_updated = WCS(naxis=2)

    # Update WCS properties
    wcs_updated.wcs.crpix = wcs_original.wcs.crpix[0:2] - [xmin, ymin]
    wcs_updated.wcs.cdelt = wcs_original.wcs.cdelt[0:2]
    wcs_updated.wcs.crval = wcs_original.wcs.crval[0:2]
    wcs_updated.wcs.ctype = wcs_original.wcs.ctype[0:2]

    # Handle pc matrix if it exists
    wcs_updated.wcs.pc = getattr(wcs_original.wcs, "pc", wcs_updated.wcs.pc)[0:2, 0:2]

    header = wcs_updated.to_header()
    header["NAXIS"] = 2

    data_slice = tuple(
        (
            np.s_[xmin:xmax] if i == 1 else np.s_[ymin:ymax] if i == 2 else channel if i == freq_axis else 0
            for i in range(naxis, 0, -1)
        )
    )

    primary_hdu = fits.PrimaryHDU(hdu.data[data_slice], header)

    # Copying keys from original header.
    header_keys = ("EQUINOX", "EPOCH", "BMAJ", "BMIN", "BPA")
    primary_hdu.header.update(
        {header_key: hdu.header[header_key] for header_key in header_keys if header_key in hdu.header}
    )
    if "TAN" in hdu.header["CTYPE1"]:
        primary_hdu.header["LATPOLE"] = hdu.header["CRVAL2"]

    hdu_list = fits.HDUList([primary_hdu])
    return hdu_list


def _get_image_centre(right_ascension: float, declination: float, hdu: fits.PrimaryHDU) -> Tuple[float, float]:
    """Gets centre coordinates in pixel units.

    Args:
        right_ascension: Value for the right ascension of the image.
        declination: Value for the declination of the image.
        hdu: FITS header object.

    Returns:
        x_centre: Centre of the image along the first dimension in pixel units.
        y_centre: Centre of the image along the second dimension in pixel units.
    """
    n_dims = hdu.header["NAXIS"]
    pixel_vector = np.zeros((1, n_dims))
    pixel_vector[0][0] = right_ascension
    pixel_vector[0][1] = declination
    x_centre, y_centre = WCS(hdu.header).wcs_world2pix(pixel_vector, 0)[0]

    return x_centre, y_centre


def extract_subimage(
    fits_path: Union[str, Path], right_ascension: float, declination: float, image_size_degrees: float, hdu_id: int = 0
) -> fits.HDUList:
    """Extract a subimage from a FITS file centered at the specified coordinates.

    This function extracts a subimage from a FITS file by specifying the coordinates
    (right ascension and declination) at which the subimage should be centered, and
    the size of the subimage in degrees. The extraction is based on the header information
    of the specified HDU (Header Data Unit) in the FITS file.

    Args:
        fits_path: The path to the FITS file.
        right_ascension: The right ascension of the centre of the subimage in degrees.
        declination: The declination of the centre of the subimage in degrees.
        image_size_degrees: The size of the subimage in degrees.
        hdu_id: The index of the HDU to use from the FITS file. Defaults to 0.

    Returns:
        hdu_list: A HDU list containing the extracted subimage.
    """
    with fits.open(fits_path) as fits_file:
        hdu = fits_file[hdu_id]
    image_size_pixels = int(image_size_degrees / hdu.header["CDELT2"])
    x_centre, y_centre = _get_image_centre(right_ascension, declination, hdu)
    hdu_list = _flatten(hdu, x_centre, y_centre, image_size_pixels)
    return hdu_list


def create_wcs(
    width_in_pixels: int,
    height_in_pixels: int,
    pixel_width_in_radians: float,
    pixel_height_in_radians: float,
    phase_centre: SkyCoord,
    polarisation_frame: PolarisationFrame = PolarisationFrame("stokesI"),
    frequency: float = 1.0e8,
    channel_bandwidth: float = 1.0e6,
    n_channels: int = 3,
) -> WCS:
    """
    Create a FITS WCS consistent with the inputs.

    Args:
        width_in_pixels: Image width.
        height_in_pixels: Image height.
        pixel_width_in_radians: Width of a pixel in radians.
        pixel_height_in_radians: Height of a pixel in radians.
        phase_centre: Phase centre.
        polarisation_frame: Polarisation frame.
        frequency: Start frequency.
        channel_bandwidth: Channel width (Hz).
        n_channels: Number of channels in output image.

    Returns:
          wcs: A valid WCS object constructed to be used with 4d image data: [channel, polarisation, ra, dec]
    """
    if width_in_pixels is None:
        raise ValueError("width_in_pixels must be specified")
    if height_in_pixels is None:
        raise ValueError("height_in_pixels must be specified")
    if pixel_width_in_radians is None:
        raise ValueError("pixel_width_in_radians must be specified")
    if pixel_height_in_radians is None:
        raise ValueError("pixel_height_in_radians must be specified")
    if phase_centre is None:
        raise ValueError("phase_centre must be specified")
    if polarisation_frame is None:
        raise ValueError("polarisation_frame must be specified")
    if frequency is None:
        raise ValueError("frequency must be specified")
    if channel_bandwidth is None:
        raise ValueError("channel_bandwidth must be specified")
    if n_channels is None:
        raise ValueError("n_channels must be specified")

    npol = polarisation_frame.npol
    pol = PolarisationFrame.fits_codes[polarisation_frame.type]
    if npol > 1:
        dpol = pol[1] - pol[0]
    else:
        dpol = 1.0

    wcs = WCS(naxis=4)
    # The negation in the longitude is needed by definition of RA, DEC
    wcs.wcs.cdelt = [
        -pixel_width_in_radians * 180.0 / np.pi,
        pixel_height_in_radians * 180.0 / np.pi,
        dpol,
        channel_bandwidth,
    ]
    wcs.wcs.crpix = [
        width_in_pixels // 2 + 1,
        height_in_pixels // 2 + 1,
        pol[0],
        1.0,
    ]
    wcs.wcs.ctype = ["RA---SIN", "DEC--SIN", "STOKES", "FREQ"]
    wcs.wcs.crval = [
        phase_centre.ra.deg,
        phase_centre.dec.deg,
        1.0,
        frequency,
    ]
    wcs.naxis = 4
    wcs.wcs.radesys = "ICRS"
    wcs.wcs.equinox = 2000.0

    return wcs


def get_wcs_from_image_info(
    image_info: ImageInfo,
    width_in_pixels: int,
    height_in_pixels: int,
    polarisation_frame: PolarisationFrame = PolarisationFrame("stokesI"),
) -> WCS:
    """
    Create an FITS WCS consistent with the inputs.

    Args:
        image_info: Information on phase centre, pixel size and frequency required to construct the WCS.
        width_in_pixels: Image width in pixels.
        height_in_pixels: Image height in pixels.
        polarisation_frame: Polarisation frame.

    Returns:
        A valid WCS object constructed to be used with 4d image data: [channel, polarisation, ra, dec]
    """
    if image_info is None:
        raise ValueError("image_info must be specified")
    if width_in_pixels is None:
        raise ValueError("width_in_pixels must be specified")
    if height_in_pixels is None:
        raise ValueError("height_in_pixels must be specified")
    if polarisation_frame is None:
        raise ValueError("polarisation_frame must be specified")
    pixel_width_radians = image_info.pixel_width_arcsec * astropy.units.arcsec.to(astropy.units.radian)
    pixel_height_radians = image_info.pixel_height_arcsec * astropy.units.arcsec.to(astropy.units.radian)
    channel_bandwidth = (image_info.end_frequency - image_info.start_frequency) / image_info.n_output_channels
    try:
        sky_coord = SkyCoord(
            image_info.phase_centre.phase_centre_ra,
            image_info.phase_centre.phase_centre_dec,
            unit="rad",
            frame=image_info.phase_centre.phase_centre_frame.lower(),
        )
    except ValueError as error:
        # Likely because of unknown frame, give a more detailed/specific error message if it is.
        if image_info.phase_centre.phase_centre_frame.lower() not in frame_transform_graph.get_names():
            raise ValueError(
                f"frame must be a value supported by astropy."
                f"Given [{image_info.phase_centre.phase_centre_frame.lower()}]"
                f"[Supported {frame_transform_graph.get_names()}]"
            ) from error
        raise error

    return create_wcs(
        width_in_pixels,
        height_in_pixels,
        pixel_width_radians,
        pixel_height_radians,
        sky_coord,
        polarisation_frame,
        image_info.start_frequency,
        channel_bandwidth,
        image_info.n_output_channels,
    )


def create_image_with_wcs(
    image_data: np.ndarray,
    wcs: WCS,
    polarisation_frame: PolarisationFrame = PolarisationFrame("stokesI"),
    clean_beam: dict[str, float] = None,
) -> Image:
    """
    Create an image consistent with the inputs, with the necessary metadata to be saved as a FITS file.

    Args:
        image_data: A four dimensional array of pixel data [channel, polarisation, ra, dec].
        wcs: A WCS object for the image data, this can be constructed using create_wcs.
        polarisation_frame: Polarisation frame.
        clean_beam: dict holding clean beam e.g {"bmaj":0.1, "bmin":0.05, "bpa":-60.0}.

    Returns:
        Astropy Image object.
    """
    if image_data is None:
        raise ValueError("image_data must be specified")
    if wcs is None:
        raise ValueError("wcs must be specified")
    if polarisation_frame is None:
        raise ValueError("polarisation_frame must be specified")
    return Image.constructor(
        data=image_data,
        wcs=wcs,
        polarisation_frame=polarisation_frame,
        clean_beam=clean_beam,
    )


def create_image(
    image_data: np.ndarray,
    width_in_pixels: int,
    height_in_pixels: int,
    pixel_width_in_radians: float,
    pixel_height_in_radians: float,
    phase_centre: SkyCoord,
    polarisation_frame: PolarisationFrame = PolarisationFrame("stokesI"),
    frequency: float = 1.0e8,
    channel_bandwidth: float = 1.0e6,
    n_channels: int = 3,
    clean_beam: dict[str, float] = None,
) -> Image:
    """
    Create an image consistent with the inputs, with the necessary metadata to be saved as a FITS file.

    Args:
        image_data: A four dimensional array of pixel data [channels, polarisation, ra, dec].
        width_in_pixels: Image width.
        height_in_pixels: Image height.
        pixel_width_in_radians: Width of a pixel in radians.
        pixel_height_in_radians: Height of a pixel in radians.
        phase_centre: phase_centre.
        polarisation_frame: Polarisation frame.
        frequency: Start frequency.
        channel_bandwidth: Channel width (Hz).
        n_channels: Number of channels in image.
        clean_beam: dict holding clean beam e.g {"bmaj":0.1, "bmin":0.05, "bpa":-60.0}.

    Returns:
        Astropy Image object.
    """
    wcs = create_wcs(
        width_in_pixels,
        height_in_pixels,
        pixel_width_in_radians,
        pixel_height_in_radians,
        phase_centre,
        polarisation_frame,
        frequency,
        channel_bandwidth,
        n_channels,
    )

    return create_image_with_wcs(
        image_data,
        wcs,
        polarisation_frame,
        clean_beam,
    )


def save_fits_image(
    image_data: np.ndarray,
    wcs: WCS,
    output_path: Path,
):
    """Create and save a fits image file containing image_data.

    This function take a numpy array of floating point image data shaped [channels, polarisation, ra, dec]
    along with associated observation info and writes to disk a FITS image.

    Args:
        image_data: A four dimensional array of pixel data [channels, polarisation, ra, dec].
        wcs: A WCS object for the image data, construct using create_wcs.
        output_path: The path/filename where the image will be saved.

    Returns:
        None

    Notes:
        - Currently only takes one polarisation and assumes Stokes I.
    """
    image_with_wcs = create_image_with_wcs(
        image_data,
        wcs,
        polarisation_frame=PolarisationFrame("stokesI"),
        clean_beam=None,
    )
    image_with_wcs.image_acc.export_to_fits(output_path)
