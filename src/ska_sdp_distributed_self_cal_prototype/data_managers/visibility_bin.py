"""Module containing class for visibility data binned by subgrid configuration."""

from typing import Union

import dask.array as da
import numpy as np
from ska_sdp_exec_swiftly import SubgridConfig

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger

logger = setup_logger(__name__)


class VisibilityBin:
    """Visibility data binned by subgrid configuration.

    Attributes:
        subgrid_config: SubgridConfig object containing configuration for the subgrid.
        obs_vis: An array of unrolled observed visibilities.
        res_vis: An array of unrolled residual visibilities (initialised to observed visibilities,
            updated by subracting model visibilities at the end of each major cycle).
        uvw: An array of unrolled uvw values.
        channel_count: Number of channels.
        start_channels: An array of start channels required for (de)gridding.
        end_channels: An array of end channels required for (de)gridding.
        degridding_task: Holds a dask.delayed task for degridding.
    """

    def __init__(
        self,
        subgrid_config: SubgridConfig,
        vis_data: Union[da.Array, np.ndarray],
        uvw_data: da.Array,
        channel_count: int,
        start_channels: da.Array,
        end_channels: da.Array,
    ):
        self.subgrid_config = subgrid_config
        if isinstance(vis_data, np.ndarray):
            self.res_vis = self.obs_vis = da.from_array(vis_data).astype(np.complex128)
        else:
            self.res_vis = self.obs_vis = vis_data
        self.uvw = uvw_data
        self.channel_count = channel_count
        self.start_channels = start_channels
        self.end_channels = end_channels

    def update_residual_visibilities(self, model_vis: np.ndarray) -> None:
        """Subtract model visibilities from original (observed) visibilities.

        Args:
            model_vis: An array of the model visibilities to subtract.

        Returns:
            None
        """
        self.res_vis = self.obs_vis - model_vis
        logger.info(
            f"Subtracted sum of {np.sum(model_vis)} model visibilities from sum of {np.sum(self.obs_vis.compute())} "
            f"observed visibilities to give sum of {np.sum(self.res_vis.compute())} "
            f"residual visibilities for subgrid ({self.subgrid_config.off0} / {self.subgrid_config.off1})..."
        )
