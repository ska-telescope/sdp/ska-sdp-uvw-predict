"""Class for orchestrating visibility IO"""

# pylint: disable=W0511
# R1710

import numbers
from dataclasses import replace
from itertools import product
from math import ceil
from typing import Any, Callable, Optional, Union

import numpy as np
import xarray as xr
from ska_sdp_exec_swiftly import SubgridConfig
from xradio.correlated_data import ProcessingSet
from xradio.correlated_data.open_processing_set import open_processing_set

from ska_sdp_distributed_self_cal_prototype.constants import SPEED_OF_LIGHT
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import (
    PhaseCenter,
    PipelineConfig,
    ProcessingSetInfo,
)

logger = setup_logger(__name__)


class VisibilityPartition:
    """A partition of labelled xarray datasets.

    Attributes:
        dataset_dict: Keys are partition names and values are xarray datasets, where the datasets include updated uvw
            bounds.
        partition_bounds: An xarray DataArray with the maximum and mimimum values of u, v, and w in wavelengths for the
            whole partition.
    """

    def __init__(
        self,
        dataset_dict: Union[dict[str, xr.Dataset], ProcessingSet],
    ):
        """Sets the input dataset dictionary as an attribute then finds the uvw bounds.

        Args:
            dataset_dict: A dictionary of partition name keys and xarray dataset values.
        """
        self.dataset_dict: Union[dict[str, xr.Dataset], ProcessingSet] = dataset_dict.copy()
        self.min_frequency = self._get_min_frequency()
        self._calculate_uvw_bounds_all_datasets()

    def total_size_gb(self) -> float:  # pragma: no cover
        """Calculates the total size of all the datasets.

        Returns:
            total_gb: Size of the data in the visibility partition in gibibytes.
        """
        total_bytes = sum(dataset.nbytes for dataset in self.dataset_dict.values())
        total_gb = total_bytes / (1024**3)
        return total_gb

    def __str__(self) -> str:  # pragma: no cover
        num_datasets = len(self.dataset_dict)
        total_gb: float = self.total_size_gb()

        return f"VisibilityPartition\nNumber of datasets: {num_datasets}\nTotal size: {total_gb:.2f} GB"

    def _get_min_frequency(self) -> float | None:
        """Gets the smallest frequency in the visibility partition.

        Returns:
            Smallest frequency in the visibility partition. Returns None is the partition is empty.
        """
        list_min_frequencies = []
        for _, dataset in self.dataset_dict.items():
            list_min_frequencies.append(dataset.frequency.min().values)
        if list_min_frequencies:
            return min(list_min_frequencies)
        return None

    def _calculate_uvw_bounds_all_datasets(self) -> None:
        """Calls methods that calculate the uvw bounds on each dataset."""
        for key, dataset in self.dataset_dict.items():
            self.dataset_dict[key] = self._calculate_uvw_bounds_per_baseline(dataset)
            self._calculate_dataset_uvw_bounds(self.dataset_dict[key])

        if len(self.dataset_dict):
            self.partition_bounds: xr.DataArray = self._calculate_partition_uvw_bounds()

    @staticmethod
    def _calculate_uvw_bounds_per_baseline(dataset: xr.Dataset) -> xr.Dataset:
        """Calculates the uvw_bounds for each baseline in a single dataset.

        Args:
            dataset: Input xarray dataset.
        """
        # Get uvw data in metres
        uvw_data_m = dataset["UVW"]

        # If the "frequency_bounds" exists, need to use a different method
        if "frequency_bounds" in dataset:
            # Calculate uvw values in wavelengths
            uvw_data = uvw_data_m * dataset["frequency_bounds"] / SPEED_OF_LIGHT

            # Get the min/max for each 'baseline_id'
            uvw_min = uvw_data.min(dim=["time", "bounds"])
            uvw_max = uvw_data.max(dim=["time", "bounds"])

        else:
            # Get the frequencies
            frequencies = dataset.frequency

            # Calculate uvw values in wavelengths
            uvw_data = uvw_data_m * frequencies / SPEED_OF_LIGHT

            # Get the min/max for each 'baseline_id'
            uvw_min = uvw_data.min(dim=["time", "frequency"])
            uvw_max = uvw_data.max(dim=["time", "frequency"])

        # Reshape the arrays to match the shape of the bounds.
        uvw_min = uvw_min.expand_dims("bounds").transpose("baseline_id", "uvw_label", "bounds")
        uvw_max = uvw_max.expand_dims("bounds").transpose("baseline_id", "uvw_label", "bounds")

        # Concatenate minimum and maximum values along the bounds.
        uvw_bounds = xr.concat([uvw_min, uvw_max], dim="bounds")

        dataset = dataset.assign_coords(bounds=["min", "max"])

        dataset["UVW_bounds"] = uvw_bounds

        return dataset

    @staticmethod
    def _calculate_dataset_uvw_bounds(dataset: xr.Dataset) -> None:
        """Calculates the dataset_bounds from UVW_bounds in a single dataset.

        Args:
            dataset: Input xarray dataset.
        """
        dataset_min = dataset["UVW_bounds"].min(dim=["baseline_id", "bounds"])
        dataset_max = dataset["UVW_bounds"].max(dim=["baseline_id", "bounds"])

        # Concatenate along the bounds dimension.
        dataset_bounds_data = xr.concat([dataset_min, dataset_max], dim="bounds")

        # Create a DataArray for dataset bounds.
        dataset_bounds_coords = {
            "bounds": ["min", "max"],
            "uvw_label": dataset["UVW_bounds"].coords["uvw_label"],
        }
        dataset_bounds = xr.DataArray(dataset_bounds_data, coords=dataset_bounds_coords)

        # Set it as an attribute on the dataset.
        dataset.attrs["dataset_bounds"] = dataset_bounds

    def _iterate_datasets(self, function: Callable, **kwargs) -> "VisibilityPartition":
        """Applies a function to each dataset in the dataset_dict and returns a new VisibilityPartition object.

        Args:
            function: Function to apply to every dataset in the visibility partition.

        Returns:
            New visibility partition with filtered data.
        """
        filtered_dataset_dict = {}
        for key, dataset in self.dataset_dict.items():
            filtered_dataset = function(dataset, **kwargs)
            if filtered_dataset:
                filtered_dataset_dict[key] = filtered_dataset

        # return a processing_set so the .get(id) method can be used
        return VisibilityPartition(filtered_dataset_dict)

    def _calculate_partition_uvw_bounds(self) -> xr.DataArray:
        """Calculates the UVW bounds of the visibility partition.

        Returns:
            DataArray containing the maximum and minimum UVW values of the visibility partition.
        """
        for i, xds in enumerate(self.dataset_dict.values()):
            if 0 == i:
                uvw_vals = xds.dataset_bounds.values

                u_min = uvw_vals[0, 0]
                u_max = uvw_vals[1, 0]

                v_min = uvw_vals[0, 1]
                v_max = uvw_vals[1, 1]

                w_min = uvw_vals[0, 2]
                w_max = uvw_vals[1, 2]
            else:
                uvw_vals = xds.dataset_bounds.values

                new_u_min = uvw_vals[0, 0]
                new_u_max = uvw_vals[1, 0]

                new_v_min = uvw_vals[0, 1]
                new_v_max = uvw_vals[1, 1]

                new_w_min = uvw_vals[0, 2]
                new_w_max = uvw_vals[1, 2]

                u_min = u_min if u_min < new_u_min else new_u_min
                u_max = u_max if u_max > new_u_max else new_u_max
                v_min = v_min if v_min < new_v_min else new_v_min
                v_max = v_max if v_max > new_v_max else new_v_max
                w_min = w_min if w_min < new_w_min else new_w_min
                w_max = w_max if w_max > new_w_max else new_w_max

        partition_bounds_min = xr.DataArray(
            np.array([u_min, v_min, w_min]),
            dims=["uvw_label"],
            coords={"uvw_label": ["u", "v", "w"], "bounds": "min"},
        )
        partition_bounds_max = xr.DataArray(
            np.array([u_max, v_max, w_max]),
            dims=["uvw_label"],
            coords={"uvw_label": ["u", "v", "w"], "bounds": "max"},
        )
        return xr.concat([partition_bounds_min, partition_bounds_max], dim="bounds")

    @staticmethod
    def _apply_where_dataset(
        dataset: xr.Dataset,
        condition: Union[xr.Dataset, xr.DataArray, Callable],
        drop: bool = False,
    ) -> xr.Dataset:
        """Function that applies xr.where to a every DataArray in a Dataset, ensuring dtypes are preserved.

        Args:
            dataset: Dataset to be masked.
            condition: Mask to be applied to the DataArrays.
            drop: If True, drops coordinate values where every value is masked.

        Returns:
            Dataset after masking has been applied.
        """
        # Do we need to support applying dropna() before recasting? (see def _calculate_frequency_bounds)
        # Doesn't support the use of the 'other' keyword argument. This is mutually exclusive with 'drop'

        # Get all dtypes before applying where
        all_dtypes = {name: da.dtype for name, da in dataset.items()}

        # Get the dataset partition by applying where
        dataset_partition = dataset.where(condition, drop=drop)

        for name, data_array in dataset_partition.items():
            da_dtype = data_array.dtype

            # if da_dtype != all_dtypes[name]:
            if not isinstance(da_dtype, type(all_dtypes[name])):
                dataset_partition[name] = data_array.astype(all_dtypes[name], casting="unsafe")

        return dataset_partition

    @staticmethod
    def _apply_where_dataarray(
        dataarray: xr.DataArray,
        condition: Union[xr.Dataset, xr.DataArray, Callable],
        drop: bool = False,
    ) -> xr.DataArray:
        """Function that applies xr.where to a DataArray, ensures dtypes are preserved.

        Args:
            dataarray: DataArray to be masked.
            condition: Mask to be applied to the DataArray.
            drop: If True, drops coordinate values where every value is masked.

        Returns:
            DataArray after masking has been applied.
        """
        # Do we need to support applying dropna() before recasting? (see def _calculate_frequency_bounds)
        # Doesn't support the use of the 'other' keyword argument. This is mutually exclusive with 'drop'

        # Get dtype before applying where
        original_dtype = dataarray.dtype

        # Apply where
        dataarray_partition = dataarray.where(condition, drop=drop)

        # Get the new dtype
        new_dtype = dataarray_partition.dtype

        # Apply recasting if necessary
        if original_dtype != new_dtype:
            dataarray_partition = dataarray_partition.astype(original_dtype, casting="unsafe")

        return dataarray_partition

    @staticmethod
    def _sel_by_coord(dataset: xr.Dataset, method: str, condition: Any) -> Optional[xr.Dataset]:
        """Selects data in a dataset by a coordinate variable.

        Valid inputs for 'condition':
          - An array of values
          - A scalar value (integer if "baseline_id"==method, float if ("time"==method or "frequency"==method))
          - An instance of a slice() object
        For the first two cases we must check if the value(s) exist in the coordinates of the Dataset.
        The third case should have built-in resiliance.
        """

        # Check if condition is array-like or is a single number
        if hasattr(condition, "__len__") or isinstance(condition, numbers.Number):
            # xarray assumes all values exist. Filter out values here
            all_coords = dataset.coords[method].values
            condition = all_coords[np.isin(all_coords, condition)]

        dataset_partition = dataset.sel(indexers={method: condition})

        if len(dataset_partition.coords[method].values) != 0:
            return dataset_partition
        return None

    def _sel_by_uvw(
        self,
        dataset: xr.Dataset,
        uvw_region: Union[np.ndarray, tuple],
        use_dataset_check: bool = False,
    ) -> Optional[xr.Dataset]:
        """Takes an xarray Dataset and returns a subset of data that corresponds to the input subgrid.

        Args:
            dataset: An xarray Dataset object.
            uvw_region: Array containing the u and v bounds of a subgrid given as
                [u_min, u_max, v_min, v_max, w_min, w_max].
            use_dataset_check: Defaults to False. Filters datasets by their dataset_bounds before
                checking UVW_bounds.

        Returns:
            dataset_partition: A subset of the input dataset.
        """
        # Check that at least one baseline has an overlap with the input boundary.
        # Note: Large overhead for small datasets. May become useful for large datasets in a distributed environment.
        if use_dataset_check:
            nonzero_overlap = self._check_dataset_dataset_uvw(dataset, uvw_region)
        else:
            nonzero_overlap = True

        if nonzero_overlap:
            # Get reduced dataset which only contains the relevant baseline_ids
            # Can give false positives but never gives false negatives
            dataset_partition = self._filter_baselines_from_bounds(dataset, uvw_region)

            if dataset_partition.baseline_id.size == 0:
                return None

            # Filter by time (per baseline) s.t. only overlapping data present
            dataset_partition = self._filter_times_frequencies_from_bounds(
                dataset_partition,
                uvw_region,
            )

            if (not dataset_partition) or (dataset_partition.UVW.size == 0):
                return None

            return dataset_partition
        return None

    @staticmethod
    def _check_dataset_dataset_uvw(
        dataset: xr.Dataset,
        uvw_region: Union[np.ndarray, tuple],
    ) -> bool:
        """Takes an xarray Dataset and checks that the dataset uvw bound overlaps with the subgrid boundary.

        Args:
            dataset: An xarray Dataset object.
            uvw_region: Array containing the u and v bounds of a subgrid given as
                [u_min, u_max, v_min, v_max, w_min, w_max].

        Returns:
            overlaps: True if there is overlap, else False.
        """
        dataset_bounds = dataset.attrs["dataset_bounds"]

        u_min_xds = dataset_bounds.sel(bounds="min", uvw_label="u")
        u_max_xds = dataset_bounds.sel(bounds="max", uvw_label="u")

        v_min_xds = dataset_bounds.sel(bounds="min", uvw_label="v")
        v_max_xds = dataset_bounds.sel(bounds="max", uvw_label="v")

        w_min_xds = dataset_bounds.sel(bounds="min", uvw_label="w")
        w_max_xds = dataset_bounds.sel(bounds="max", uvw_label="w")

        u_min, u_max, v_min, v_max, w_min, w_max = uvw_region

        u_overlap = (u_min_xds <= u_max) & (u_max_xds > u_min)
        v_overlap = (v_min_xds <= v_max) & (v_max_xds > v_min)
        w_overlap = (w_min_xds <= w_max) & (w_max_xds > w_min)

        # Must overlap in all three dimensions for a UVW point to lie inside the boundary
        # Converts to boolean because xarray casts the mask to float64
        return bool((u_overlap & v_overlap & w_overlap).values)

    def _filter_baselines_from_bounds(
        self,
        dataset: xr.Dataset,
        uvw_region: Union[np.ndarray, tuple],
    ) -> xr.Dataset:
        """Takes an xarray Dataset and checks which baselines overlap with the subgrid boundary.

        Args:
            dataset: An xarray Dataset object.
            uvw_region: Array containing the u and v bounds of a subgrid given as
                [u_min, u_max, v_min, v_max, w_min, w_max].

        Returns:
            dataset: Reduced dataset eliminating any baselines that don't overlap.
        """

        # Get per-baseline uvw boundary attributes
        baseline_bounds = dataset["UVW_bounds"]

        # Read dataset bounds
        u_min_xds = baseline_bounds.sel(bounds="min", uvw_label="u")
        u_max_xds = baseline_bounds.sel(bounds="max", uvw_label="u")

        v_min_xds = baseline_bounds.sel(bounds="min", uvw_label="v")
        v_max_xds = baseline_bounds.sel(bounds="max", uvw_label="v")

        w_min_xds = baseline_bounds.sel(bounds="min", uvw_label="w")
        w_max_xds = baseline_bounds.sel(bounds="max", uvw_label="w")

        u_min, u_max, v_min, v_max, w_min, w_max = uvw_region

        # Calculate which baselines overlap with the subgrid
        u_overlap = (u_min_xds <= u_max) & (u_max_xds > u_min)
        v_overlap = (v_min_xds <= v_max) & (v_max_xds > v_min)
        w_overlap = (w_min_xds <= w_max) & (w_max_xds > w_min)

        # Get the baseline_id values of the overlapping baselines
        overlapping_indices = np.where((u_overlap & v_overlap & w_overlap).values)[0]

        baseline_ids = dataset.coords["baseline_id"][overlapping_indices].values

        dataset_partition = self._apply_where_dataset(dataset, dataset.baseline_id.isin(baseline_ids), drop=True)

        return dataset_partition

    def _filter_times_frequencies_from_bounds(
        self,
        dataset: xr.Dataset,
        uvw_region: Union[np.ndarray, tuple],
    ) -> Optional[xr.Dataset]:
        """Takes an xarray dataset and filters out the times that a baseline overlaps with the uvw boundary.

        Args:
            dataset: An xarray Dataset object.
            uvw_region: Array containing the u and v bounds of a subgrid given as
                [u_min, u_max, v_min, v_max, w_min, w_max].

        Returns:
            dataset: Reduced dataset eliminating any times that don't overlap.
        """
        # Get the uvw data in meters
        u_values_m = dataset["UVW"].sel(uvw_label="u")
        v_values_m = dataset["UVW"].sel(uvw_label="v")
        w_values_m = dataset["UVW"].sel(uvw_label="w")

        # Get frequency values
        frequencies = dataset.frequency

        # Calculate uvw values in wavelengths
        u_values = u_values_m * frequencies / SPEED_OF_LIGHT
        v_values = v_values_m * frequencies / SPEED_OF_LIGHT
        w_values = w_values_m * frequencies / SPEED_OF_LIGHT

        u_min, u_max, v_min, v_max, w_min, w_max = uvw_region

        # Get Dataset indices for uvw values in the subgrid
        u_mask = (u_values <= u_max) & (u_values > u_min)
        v_mask = (v_values <= v_max) & (v_values > v_min)
        w_mask = (w_values <= w_max) & (w_values > w_min)

        # The 'mask' DataArray will inherit the 'uvw_label' value of 'w_mask'. We need to drop this coord.
        mask = (u_mask & v_mask & w_mask).drop_vars("uvw_label")

        dataset_partition = self._apply_dataset_mask(dataset, mask)

        if dataset_partition.UVW.size == 0:
            return None

        self._calculate_frequency_bounds(dataset_partition, mask)

        return dataset_partition

    def sel(self, **kwargs) -> "VisibilityPartition":
        """Partitions all datasets by a specific method and condition.

        Args:
            method: the dimension to filter all the datasets by. Options
                include "time", "frequency", "polarization", or "uvw".
            condition: the filter condition. Can be a single value, a slice,
                or an array if using "uvw" method.

        Returns:
            VisibilityPartition: a new object that contains an attribute for the
                partitioned datasets.

        Raises:
            ValueError: An error occured applying the method.
        """

        # Get 'method' from the kwargs
        method = kwargs.get("method")

        # Check method isn't None
        if not method and not method == "":
            raise TypeError("sel() is missing 1 required keyword argument: 'method'.")

        if method in ["time", "frequency", "baseline_id", "polarization"]:
            return self._iterate_datasets(self._sel_by_coord, **kwargs)
        if method == "uvw":
            kwargs.pop("method")
            return self._iterate_datasets(self._sel_by_uvw, **kwargs)
        raise ValueError(
            f"The method provided ('{method}') must be either 'time', 'baseline_id', 'frequency', or 'uvw'."
        )

    def _apply_dataset_mask(self, dataset: xr.Dataset, mask: xr.DataArray) -> xr.Dataset:
        """Applies a mask to an xarray Dataset.

        Masks which have more dimensions than some DataArrays leads to expanded DataArrays when using .where(). This
        isn't the desired behaviour so we apply where() more selectively here.

        Args:
            dataset: The dataset to be masked.
            mask: A mask to apply to the dataset.

        Returns:
            masked_dataset: A reduced dataset. Every DataArray has the same dimensions in 'masked_dataset'
                and 'dataset'.
        """

        # Ensure mask is pre-computed
        mask.compute()

        # Create a new dataset which will contain the masked data
        masked_dataset = xr.Dataset(attrs=dataset.attrs)

        for name, data_array in dataset.data_vars.items():
            # Identify dimensions that aren't shared between the DataArray and the mask
            dims_not_shared = list(set(data_array.dims) ^ set(mask.dims))
            dims_to_collapse = [dim for dim in dims_not_shared if dim in mask.dims]

            if dims_to_collapse:
                # Select the mask only along specific dimensions to prevent DataArrays from being expanded along
                # new dimensions.
                reduced_mask = mask.any(dim=dims_to_collapse).compute()

                # Apply the mask using .where()
                masked_da = self._apply_where_dataarray(data_array, reduced_mask, drop=True)
            else:
                # If there are no common dimensions to collapse, just copy the DataArray as is.
                masked_da = data_array.copy()

            # Add the masked DataArray to the masked dataset
            masked_dataset[name] = masked_da
        return masked_dataset

    def _calculate_frequency_bounds(self, dataset: xr.Dataset, mask: xr.DataArray) -> None:
        """Adds a 'frequency_bounds' variable.

        The 'frequency_bounds' variable stores the maximum and minumum frequency per 'time' and 'baseline_id'.

        Args:
            dataset: The dataset to add the 'frequency_bounds' to.
            mask: A boolean mask of indices where the dataset overlaps with a uvw region.

        Returns:
            None.
        """
        unique_frequencies = mask.frequency
        valid_frequencies = self._apply_where_dataarray(unique_frequencies, mask).dropna(dim="baseline_id", how="all")

        min_frequencies = valid_frequencies.min(dim="frequency")
        max_frequencies = valid_frequencies.max(dim="frequency")

        # Combine the minimum and maximum frequencies into a single DataArray
        frequency_bounds = xr.concat([min_frequencies, max_frequencies], dim="bounds")

        # Add 'bounds' as a coordinate to the frequency_bounds DataArray
        frequency_bounds.coords["bounds"] = ["min", "max"]

        # Add 'frequency_bounds' as a new data variable to the existing dataset
        dataset["frequency_bounds"] = frequency_bounds


class ProcessingSetManager:
    """Class for partitioning datasets and distributing disk I/O.

    Args:
        config: Config describing the pipeline.

    Attributes:
        partitions: List of data partitions. Each partition will be local
            to a compute node. The data in a partition has uvw locality.
    """

    def __init__(
        self,
        config: PipelineConfig,
        # visibilities: Union[dict[str, xr.Dataset], processing_set],
        # target_partition_size: int, # NOT IMPLEMENTED. Will be used to recursively split VisibilityPartition objects
    ):
        self._config = config

        # TODO: Add error handling
        msv4 = open_processing_set(str(config.ps_dir))

        # Load a single dataset key. Replace later when iterating over all keys
        try:
            base_visibilities = VisibilityPartition({config.dataset_key: msv4[config.dataset_key]})
        except KeyError as err:
            error_message = f"The dataset_key '{config.dataset_key}' does not exist in the processing set provided."
            logger.error(error_message)
            raise ValueError(error_message) from err
        number_datasets = len(base_visibilities.dataset_dict.keys())

        self.partitions: list[VisibilityPartition] = self._partition_data(
            base_visibilities
        )  # Make these immutable properties?

        number_uvw_partitions = len(self.partitions)
        self.min_frequency = min(partition.min_frequency for partition in self.partitions)
        self._vis_name = self._get_vis_name()  # TODO this is stored in two places

        # TODO: Add config test here that fails pipeline early if phase center is in units/frames we don't support.
        self.phase_center = self._get_phase_center(self.get_dataset())

        config.processing_set_info = ProcessingSetInfo(
            number_datasets=number_datasets,
            number_uvw_partitions=number_uvw_partitions,
            vis_name=self._vis_name,
            min_frequency=self.min_frequency,
        )

        # Update config
        self._config = config
        self._add_subgrid_config_list()

    @property
    def config(self):
        """Returns the config attribute."""
        return self._config

    @property
    def vis_name(self):
        """Returns the visibility name."""
        return self._vis_name

    def _add_subgrid_config_list(
        self,
    ) -> None:
        """Get list of subgrid configurations."""

        # Get metadata
        config = self._config
        image_scale_padded = config.gridder_info.image_scale_padded
        subgrid_size_effective = config.swiftly_info.subgrid_size_effective
        swiftly_info = config.swiftly_info

        # TODO Update once iterating over all partitions/datasets
        # Select dataset and get bounds
        partition = self.partitions[0]
        uvw_min, uvw_max = partition.partition_bounds.data

        # Calculate number of subgrids from uvw_values
        subgrid_count = 2 * ceil(max(*-uvw_min[:2], *uvw_max[:2]) * image_scale_padded / subgrid_size_effective) + 1

        # Update config
        swiftly_info = replace(swiftly_info, subgrid_count=subgrid_count)
        config.swiftly_info = swiftly_info
        self._config = config

        subgrid_start = -(subgrid_count - 1) // 2
        subgrid_end = (subgrid_count - 1) // 2 + 1
        subgrid_offsets = subgrid_size_effective * np.arange(subgrid_start, subgrid_end)

        # Make facet layout, honouring facet offset restrictions
        # (note that this will be more relaxed for the "unshifted contributions" version)
        self._subgrid_config_list = [
            SubgridConfig(subgrid_u_offset, subgrid_v_offset, size=subgrid_size_effective)
            for subgrid_u_offset, subgrid_v_offset in product(subgrid_offsets, subgrid_offsets)
        ]

    def _partition_data(
        self,
        base_visibilities: VisibilityPartition,
    ) -> list[VisibilityPartition]:
        """NOT IMPLEMENTED. Partitions a dataset into a list of datasets that have uvw locality.

        Args:
            base_visibilities: Dataset containing all visibilities.

        Returns:
            partitions: A list of datasets that correspond to a region of uvw space.
        """
        # TODO:
        # Decide on partitioning interface: target_size, num_nodes, memory_per_node?

        return [base_visibilities]

    def _get_vis_name(
        self,
    ) -> str:
        """Checks if corrected visibilities exist in the dataset.

        Returns:
            The name of the visibility dataarray. Returns 'VISIBILITY_CORRECTED' if both 'VISIBILITY' and
                'VISIBILITY_CORRECTED' are present.
        """

        first_uvw_partition = self.partitions[0]
        first_observation_partition = next(iter(first_uvw_partition.dataset_dict))
        dataset_keys = first_uvw_partition.dataset_dict[first_observation_partition].keys()
        variable_names = list(dataset_keys)
        if any(name == "VISIBILITY_CORRECTED" for name in variable_names):
            return "VISIBILITY_CORRECTED"
        return "VISIBILITY"

    def get_dataset(self, partition_index: int = 0) -> xr.Dataset:
        """Get measurement dataset from visibility partition.

        Args:
            partition_index: index to visibility partition, default=0.

        Returns:
            dataset: xarray dataset containing measurements.
        """
        partition = self.partitions[partition_index]
        dataset = partition.dataset_dict[self.config.dataset_key]
        return dataset

    def get_frequencies(self, partition_index: int = 0):
        """Get frequencies from visibility partition.

        Args:
            partition_index: index to visibility partition, default=0.

        Returns:
            Numpy array containing frequency values.
        """
        dataset = self.get_dataset(partition_index)
        return dataset.frequency.values

    def _get_phase_center(self, xds: xr.Dataset) -> PhaseCenter:
        """Creates a PhaseCenter object from the MSv4 metadata.

        Args:
            xds: MAIN xds for the processing set.

        Returns:
            Populated PhaseCentre dataclass.
        """
        field_phase_center_xds = xds[self._vis_name].field_and_source_xds["FIELD_PHASE_CENTER"]
        assert field_phase_center_xds.shape == (2,)
        phase_center_ra, phase_center_dec = field_phase_center_xds.values
        phase_center_frame = field_phase_center_xds.frame

        return PhaseCenter(phase_center_ra, phase_center_dec, phase_center_frame)

    @property
    def subgrid_config_list(self):
        """Returns the subgrid_config_list attribute."""
        return self._subgrid_config_list
