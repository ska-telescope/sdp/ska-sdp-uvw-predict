"""Module for constructing and orchestrating SwiFTly instances"""

# Ignore todos for now
# pylint: disable=W0511

from itertools import product
from math import ceil, isqrt

import numpy as np
from dask import delayed
from ska_sdp_exec_swiftly import FacetConfig, SubgridConfig, SwiftlyBackward, SwiftlyConfig, SwiftlyForward
from ska_sdp_func.grid_data.gridder_wtower_uvw import GridderWtowerUVW

from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.processing_tasks._gridding_utils import _fft
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig

logger = setup_logger(__name__)


class Swiftly:
    """Class to construct and store a SwiFTly instance.

    Args:
        config: PipelineConfig object containing swiftly_info.
    """

    def __init__(
        self,
        config: PipelineConfig,
        limit_facets: bool = False,
    ):
        """
        Function that constructs all SwiFTly classes needed for inversion.

        Args:
            pipeline_config: Pipeline configuration including SwiFTly parameters.
            limit_facets: Choose whether to limit the number of facets to 1 instead of the number
                calculated from the swiftly config. Set this to `True` when used for PSF generation.
        """

        self._config: PipelineConfig = config
        self._facet_config_list = self._get_facet_config_list(limit_facets=limit_facets)
        logger.info(f"Generated list of {len(self._facet_config_list)} facet configs")
        # Initialise backwards pass (subgrids -> facets)
        self.backward: SwiftlyBackward = SwiftlyBackward(
            swiftly_config=SwiftlyConfig(**self.config.swiftly_info.swiftly_config),
            facets_config_list=self._facet_config_list,
            lru_backward=self.config.swiftly_info.backwards_steps,
            queue_size=self.config.swiftly_info.backward_queue_size,
        )

        # Initialise forward pass (facets -> subgrids)
        self.forward = SwiftlyForward(
            swiftly_config=SwiftlyConfig(**self.config.swiftly_info.swiftly_config),
            facet_tasks=None,
            lru_forward=self.config.swiftly_info.forwards_steps,
            queue_size=self.config.swiftly_info.forward_queue_size,
        )

    def get_facet_tasks(self, facets: list[np.ndarray]) -> None:
        """Get list of facet dask tasks for SwiftlyForward.

        Uses list of facets and facet configurations.

        Args:
            facets: List of facets.

        Returns:
            None.
        """
        self.forward.facet_tasks = list(zip(self._facet_config_list, facets))

    def _get_subgrid_tasks(self, subgrid_config_list: list[SubgridConfig]) -> list[delayed]:
        """Get list of subgrid dask tasks for SwiftlyForward.

        Args:
            subgrid_config_list: List of SubgridConfig objects.

        Returns:
            A list of delayed arrays.
        """
        return [self.forward.get_subgrid_task(subgrid_config) for subgrid_config in subgrid_config_list]

    def _get_facet_config_list(self, limit_facets: bool = False) -> list[FacetConfig]:
        """Get list of facet configurations.

        Args:
            limit_facets: Choose whether to use the facets specified in the swiftly config or to limit the facets
                to 1. Should be set to `True` when generating the PSF.

        Returns:
            facet_config_list: List of FacetConfig objects.
            limit_facets: Choose whether to limit the number of facets to 1 instead of the number
                calculated from the swiftly config. Set this to `True` when used for PSF generation.
        """
        facet_size_effective = self._config.swiftly_info.facet_size_effective
        if limit_facets:
            facet_count = 1
        else:
            facet_count = self._config.swiftly_info.facet_count
        return [
            FacetConfig(
                int(ceil((il - facet_count / 2 + 0.5) * facet_size_effective)),
                int(ceil((im - facet_count / 2 + 0.5) * facet_size_effective)),
                size=facet_size_effective,
            )
            for il, im in product(range(facet_count), range(facet_count))
        ]

    def create_facets(self) -> list[np.ndarray]:
        """Create facets using SwiFTly backward.

        Returns:
            facets: A list of image facets as 2D numpy arrays.
        """
        logger.info("Creating facets.")
        facets = [
            facet_task.compute() for facet_task in self.backward.finish()
        ]  # TODO Check if we need only real components
        logger.info(f"{len(facets)} Facets created.")
        return facets

    def compute_normalisation_factor(
        self, facets: list[np.ndarray], total_num_visibilities: int, channel_count: int
    ) -> float:
        """Compute normalisation factor using sum of absolute brightness in image facets.

        Args:
            facets: list of image facets to use for computing maximum brightness
            total_num_visibilities: total number of visibilities in whole dataset
            channel_count: number of channels

        Returns:
            normalisation_factor: normalisation factor to use for this major cycle
        """
        channel_count = 1.0  # TODO: Establish correct normalisation formula
        normalisation_factor = total_num_visibilities / (np.sum(np.absolute(facets)) * channel_count)
        return normalisation_factor

    def generate_grid_corrected_facets(
        self, gridding_kernel: GridderWtowerUVW, total_num_visibilities: int, channel_count: int
    ) -> list[np.ndarray]:
        """Generate grid corrected facets.

        Args:
            gridding_kernel: The gridding kernel used for correcting facet images.
            total_num_visibilities: Factor used with the pixel sum to normalise the pixel values.
            channel_count: number of channels

        Returns:
            facets: A list of image facets as 2D numpy arrays with grid corrections applied.
        """
        facets = self.create_facets()
        corrected_facets = self.apply_grid_corrections(facets, gridding_kernel)
        normalised_facets, normalisation_factor = self.normalise_facets(
            corrected_facets, total_num_visibilities, channel_count
        )
        return normalised_facets, normalisation_factor

    def normalise_facets(
        self, facets: list[np.ndarray], total_num_visibilities: int, channel_count: int
    ) -> list[np.ndarray]:
        """Normalise the pixel values in the facets using total number of visibilities.

        N.B. Channel count is not currently used in the normalisation calculation.

        Args:
            facets: A list of image facets as 2D numpy arrays.
            total_num_visibilities: Total number of visibility values in the dataset.
            channel_count: number of channels

        Returns:
            facets: A list of image facets as 2D numpy arrays with normalised pixel values.
            normalisation_factor: Factor used to normalise the pixel values.
        """
        normalisation_factor = self.compute_normalisation_factor(
            facets=facets, total_num_visibilities=total_num_visibilities, channel_count=channel_count
        )
        logger.info(f"Normalisation: {normalisation_factor}")
        for i, facet in enumerate(facets):
            facets[i] = facet * normalisation_factor
        return facets, normalisation_factor

    def apply_grid_corrections(
        self,
        facets: list[np.ndarray],
        kernel: GridderWtowerUVW,
    ) -> list[np.ndarray]:  # pragma: no cover
        """Applies corrections to the facets after gridding.

        This function iterates over a list of facet images, then applies a grid
        correction using the provided kernel.

        Args:
            facets: A list of image facets as 2D numpy arrays.
            kernel: The gridding kernel used for correcting facet images.
            normalisation_factor: Factor used to normalise the pixel values.

        Returns:
            facets: A list of image facets as 2D numpy arrays with grid corrections applied.
        """
        for i, (facet, facet_config) in enumerate(zip(facets, self._facet_config_list)):
            logger.info(f"Grid correcting facet with offset {facet_config.off0}/{facet_config.off1}")
            assert facet.dtype == np.complex128
            kernel.grid_correct(facet, facet_config.off0, facet_config.off1)
            facets[i] = facet
        return facets

    def apply_degrid_corrections(
        self,
        facets: list[np.ndarray],
        kernel: GridderWtowerUVW,
        normalisation_factor: float,
    ) -> list[np.ndarray]:
        """Applies corrections to the facets before degridding.

        This function iterates over a list of facet images, then applies a degrid
        correction using the provided kernel.

        Args:
            facets: A list of image facets as 2D numpy arrays.
            kernel: The gridding kernel used for correcting facet images.
            normalisation_factor: Factor used to normalise the pixel values.

        Returns:
            facets: A list of image facets as 2D numpy arrays with degrid corrections applied.
        """
        normalisation_factor = 1.0  # TODO Fix inverse normalisation
        logger.info(f"Normalisation: {normalisation_factor}")

        for i, facet in enumerate(facets):
            logger.info(f"Inversing normalisation of facet {i}")
            facets[i] = facet / 1.0
        for i, (facet, facet_config) in enumerate(zip(facets, self._facet_config_list)):
            assert facet.dtype == np.complex128
            logger.info(f"Degrid correcting facet with offset {facet_config.off0}/{facet_config.off1}")
            kernel.degrid_correct(_fft(facet), facet_config.off0, facet_config.off1)
            facets[i] = facet
        return facets

    def generate_image_from_facets(
        self, gridding_kernel: GridderWtowerUVW, total_num_visibilities: int, channel_count: int
    ) -> np.ndarray:
        """Generate full image from facets.

        Args:
            gridding_kernel: The gridding kernel used for correcting
                facet images.
            total_num_visibilities: Constant used with pixel sum to normalise the pixel values.
            channel_count: number of channels

        Returns:
            full_image: The full image after stitching facets together.
        """
        facets, _ = self.generate_grid_corrected_facets(gridding_kernel, total_num_visibilities, channel_count)
        full_image = self.join_facets(facets)
        return full_image

    def join_facets(
        self,
        facets: list[np.ndarray],
    ) -> np.ndarray:
        """Stitch facets together into full image.

        Args:
            facets: A list of image facets as 2D numpy arrays.

        Returns:
            full_image: Full image after stitching facets together
        """
        facet_count = isqrt(len(facets))
        facet_size_effective = self._config.swiftly_info.facet_size_effective
        full_image = np.empty((facet_size_effective * facet_count, facet_size_effective * facet_count))
        for (l_index, m_index), facet in zip(
            product(range(facet_count), range(facet_count)),
            facets,
        ):
            full_image[
                l_index * facet_size_effective : (l_index + 1) * facet_size_effective,
                m_index * facet_size_effective : (m_index + 1) * facet_size_effective,
            ] = facet.real
        return full_image

    @property
    def config(self):
        """Returns the config attribute."""
        return self._config

    @property
    def facet_config_list(self):
        """Returns the facet_config_list attribute."""
        return self._facet_config_list

    @property
    def facet_count(self):
        """Returns the facet_count from the SwiftlyInfo object."""
        return self._config.swiftly_info.facet_count
