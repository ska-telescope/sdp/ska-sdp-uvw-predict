"""Module containing functions used during deconvolution"""

import numpy as np

from ska_sdp_distributed_self_cal_prototype.data_managers.swiftly import Swiftly
from ska_sdp_distributed_self_cal_prototype.data_managers.visibility_bin import VisibilityBin
from ska_sdp_distributed_self_cal_prototype.logger import setup_logger
from ska_sdp_distributed_self_cal_prototype.processing_tasks.gridding import Gridder
from ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config import PipelineConfig

logger = setup_logger(__name__)


class SelfCalibrationManager:
    """Class to orchestrate self-calibration.

    Args:
        config: PipelineConfig object containing swiftly_info.
    """

    def __init__(self, config: PipelineConfig):
        self._config: PipelineConfig = config

        # Create a new Swiftly instance with the same properties as the one used for the final image
        self._swiftly_manager_psf: Swiftly = self._set_up_psf_swiftly(config)

    def _set_up_psf_swiftly(self, config: PipelineConfig) -> Swiftly:
        """Construct a new Swiftly instance for PSF generation.

        Args:
            config: PipelineConfig object containing swiftly_info.

        Returns:
            swiftly_manager_psf: Swiftly manager for PSF generation.
        """

        swiftly_manager_psf = Swiftly(config, limit_facets=True)
        return swiftly_manager_psf

    def generate_psf(
        self,
        visibility_bins: list[VisibilityBin],
        binning_info: dict,
        gridding_manager: Gridder,
    ) -> np.ndarray:
        """Generates the PSF/dirty beam.

        Generates the central facet of the dirty beam. If there are an even number of facets, a new central facet will
            be returned that doesn't align with the facets of the dirty image.

        Args:
            visibility_bins: list of VisibilityBin objects holding binned visibility data.
            binning_info: dictionary containing parameters for sugrids:
                subgrid_size (int): size of subgrids
                min_frequency (float): minimum frequency in Hz across dataset
                channel_width (float): channel width in Hz
                total_num_visibilities (int): total number of visibilities
            gridding_manager: Gridding manager for the pipeline.

        Returns:
            psf: Returns the central facet of the PSF.
        """
        gridding_manager.grid_psf(
            swiftly_manager=self.swiftly_manager_psf,
            visibility_bins=visibility_bins,
            subgrid_size=binning_info["subgrid_size"],
            min_frequency=binning_info["min_frequency"],
            channel_width=binning_info["channel_width"],
        )

        psf_image = self.swiftly_manager_psf.generate_image_from_facets(
            gridding_manager.get_kernel(), binning_info["total_num_visibilities"], binning_info["channel_count"]
        )

        # Normalise so the max value is 1.
        psf_image /= psf_image.max()

        return psf_image

    @property
    def swiftly_manager_psf(self):
        """Returns the Swiftly instance for dirty beam generation."""
        return self._swiftly_manager_psf
