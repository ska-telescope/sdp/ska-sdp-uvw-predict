Tutorials
=========

This section provides tutorials for running :doc:`../pipelines`.

:doc:`running_the_pipeline`: Generates a clean image from a calibrated 3C123 dataset using a YAML configuration file.

:doc:`running_with_multiple_dask_nodes_on_slurm`: Demonstrates how to generates a dirty image, PSF, residual and model
from a calibrated dataset using a template slurm scrip and accompanying YAML configuration file.

:doc:`creating_a_custom_pipeline`: Describes the structure of workflows and how to create a custom pipeline.

.. toctree::
   :maxdepth: 1
   :hidden:

   running_the_pipeline
   running_with_multiple_dask_nodes_on_slurm
   creating_a_custom_pipeline
