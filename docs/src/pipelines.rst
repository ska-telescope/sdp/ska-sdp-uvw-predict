Pipelines
=========

This section provides an overview of the available pipelines that can be specified using the CLI argument ``--pipeline-name``.
Examples of how to use these pipelines can be found in :doc:`tutorials/tutorials_index`. The pipeline options are:

- ``dirty_image``: Creates a dirty image from the MSv4 provided to the pipeline.
- ``dirty_beam_and_psf``: Creates a dirty image and the point spread function from the MSv4 provided to the pipeline.
- ``clean_hogbom``: Creates a model and residual image using the Hogbom CLEAN algorithm.
- ``clean_hogbom_with_degridding``: Creates a cleaned, model and residual image using the Hogbom CLEAN algorithm and predicts visibilities from the model.
- ``continuum_imaging``: Applies both major and minor deconvolution cycles to produce a cleanedm model, and residual image.
