deconvolution
=============

.. automodule:: ska_sdp_distributed_self_cal_prototype.data_managers.deconvolution
    :members:
    :undoc-members:
    :show-inheritance:
