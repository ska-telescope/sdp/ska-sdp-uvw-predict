pipeline_config
===============

.. automodule:: ska_sdp_distributed_self_cal_prototype.workflow.pipeline_config
    :members:
    :undoc-members:
    :show-inheritance:
