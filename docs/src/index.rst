.. _documentation_master:

.. toctree::

SKA SDP Distributed Self-Cal Prototype
######################################

Prototype self-calibration pipeline to distribute processing across HPC cluster
nodes, allowing scalability for large datasets and a reduction in overall
computation time.

The pipeline can currently perform major and minor cycles to produce a clean image from a calibrated zarr
dataset but more functionality is being actively developed.

The project repository can be found `here
<https://gitlab.com/ska-telescope/sdp/ska-sdp-distributed-self-cal-prototype>`_.

This pipeline uses the SKA SDP Processing Function Library
(https://gitlab.com/ska-telescope/sdp/ska-sdp-func) for gridding visibilities
and the SwiFTly algorithm
(https://gitlab.com/ska-telescope/sdp/ska-sdp-exec-swiftly) to perform
distributed imaging. Dask (https://github.com/dask/dask) is used as the
distributed computing framework to handle the scaling of computations on a HPC
cluster.

.. toctree::
   :maxdepth: 3

   installation

.. toctree::
   :maxdepth: 3

   configuration

.. toctree::
   :maxdepth: 3

   pipelines

.. toctree::
   :maxdepth: 3

   tutorials/tutorials_index

.. toctree::
   :maxdepth: 3

   development

.. toctree::
   :maxdepth: 3

   architecture

.. toctree::
   :maxdepth: 3

   api

.. toctree::
   :maxdepth: 1
   :caption: Releases

   CHANGELOG.rst
