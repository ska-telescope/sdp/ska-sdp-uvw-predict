Installation
============

Installation is possible using either ``conda`` or ``spack`` as a package and environment manager. 
- Installation instructions for miniconda can be found `here <https://docs.anaconda.com/miniconda/#quick-command-line-install>`_.
- Installation instructions for spack can be found `here <https://spack.readthedocs.io/en/latest/getting_started.html#installation>`_.


Installation using conda
-------------------------

1. A new environment can be created using ``conda`` and a valid python version:

.. code-block:: bash

    conda create --name self-cal python=3.11

2. Activate the new environment:

.. code-block:: bash

    conda activate self-cal

3. Clone the repository, including SKA submodules, with SSH:

.. code-block:: bash

    git clone --recurse-submodules git@gitlab.com:ska-telescope/sdp/science-pipeline-workflows/ska-sdp-distributed-self-cal-prototype.git

or with HTTPS:

.. code-block:: bash

    git clone --recurse-submodules https://gitlab.com/ska-telescope/sdp/ska-sdp-distributed-self-cal-prototype.git

Submodules are required for use with SKA Makefiles and ``make`` commands.

4. Navigate to the project directory:

.. code-block:: bash

    cd ska-sdp-distributed-self-cal-prototype

5. Install `poetry` (v1.5.1 or later is required) using `pip`:

.. code-block:: bash

    pip install poetry

6. Install project dependencies using `poetry`:

.. code-block:: bash

    poetry install


Installation using spack
-------------------------

1. Clone the repository, including SKA submodules, with SSH:

.. code-block:: bash

    git clone --recurse-submodules git@gitlab.com:ska-telescope/sdp/science-pipeline-workflows/ska-sdp-distributed-self-cal-prototype.git

or with HTTPS:

.. code-block:: bash

    git clone --recurse-submodules https://gitlab.com/ska-telescope/sdp/ska-sdp-distributed-self-cal-prototype.git

Submodules are required for use with SKA Makefiles and ``make`` commands.

2. Navigate to the project directory:

.. code-block:: bash

    cd ska-sdp-distributed-self-cal-prototype

3. Add spack recipe included in this repo to spack (N.B. in the future this will be moved to the `ska-sdp-spack <https://gitlab.com/ska-telescope/sdp/ska-sdp-spack>`_ repo which contains spack recipes for various software packages)

.. code-block:: bash

    spack repo add repos/ska-sdp-distributed-self-cal-prototype/spack

4. A new spack environment can be created using ``spack env create``:

.. code-block:: bash

    spack env create self-cal

5. Activate the new environment:

.. code-block:: bash

    spack env activate self-cal

6. Add the latest py-ska-sdp-distributed-self-cal-prototype package to the environment (with gcc@12.3.0 as an example compiler):

.. code-block:: bash

    spack add py-ska-sdp-distributed-self-cal-prototype@latest %gcc@12.3.0

8. Check package is added

.. code-block:: bash

    spack find

9. Concretize then install. This may take several hours the first time since everything is compiled from scratch. Subsequent runs should take seconds.

.. code-block:: bash

    spack concretize
    spack install

If you run into issues with the spack install step it might be an issue with the gcc version you are using. 
Check that gcc 12.3.0 is available to spack and if not install it using spack and the gcc version that is available before installing this package.

6. Install project dependencies using `poetry` (these dependencies will be moved into the spack recipe in the future):

.. code-block:: bash

    poetry install
