Development
===========

Make Commands
-------------
For Unix-like operating systems the ``make`` command can be used to automate development processes.

Run Tests
~~~~~~~~~
To run all tests and produce a coverage report run:

.. code-block:: bash

   make pytest

Build Docs Locally
~~~~~~~~~~~~~~~~~~
To build the documentation locally run:

.. code-block:: bash

   make docs

Run All Checks
~~~~~~~~~~~~~~
To run all the checks on the repository, including sorting imports, formatting, linting, tests, and documentation, run:

.. code-block:: bash

   make checks

This should ideally be run before any commits because the same checks will be performed on the GitLab CI/CD pipeline.
