swiftly
=======

.. automodule:: ska_sdp_distributed_self_cal_prototype.data_managers.swiftly
    :members:
    :undoc-members:
    :show-inheritance:
