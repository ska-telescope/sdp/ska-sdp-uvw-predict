"""Configuration file for the Sphinx documentation builder."""
import sys
from pathlib import Path

parent_dir = Path(__file__).resolve().parent.parent
sys.path.insert(0, str(parent_dir))

project = "Distributed Self-Cal Prototype"
copyright = "2024, SKA Observatory"
author = "Max Maunder, Sean Stansill"
version = "0.2.0"

extensions = [
    "sphinx_rtd_theme",
    "myst_parser",
    "sphinx_gitstamp",
    'sphinx.ext.autodoc',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
]
autodoc_mock_imports = [
    "matplotlib",
    "astropy",
    "pandas"
    "pytest",
    "numpy",
    "scipy",
    "dask",
    "distributed",
    "xarray",
    "xradio",
    "ska_sdp_func",
    "ska_sdp_func_python",
    "ska_sdp_datamodels",
    "h5py",
]

exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", '**.ipynb_checkpoints']
source_suffix = {".rst": "restructuredtext", ".md": "markdown"}
html_theme = "ska_ser_sphinx_theme"
html_theme_options = {
    'navigation_depth': 3,
    'collapse_navigation': False,
    'titles_only': False,
    'sticky_navigation': True,
}
