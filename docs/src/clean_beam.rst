clean_beam
==========

.. automodule:: ska_sdp_distributed_self_cal_prototype.processing_tasks.clean_beam
    :members:
    :undoc-members:
    :show-inheritance:
