binning
=======

.. automodule:: ska_sdp_distributed_self_cal_prototype.processing_tasks.binning
    :members:
    :undoc-members:
    :show-inheritance:
