constants
=========

.. automodule:: ska_sdp_distributed_self_cal_prototype.constants
    :members:
    :undoc-members:
    :show-inheritance:
