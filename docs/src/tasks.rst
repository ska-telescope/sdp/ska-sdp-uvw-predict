tasks
=====

.. automodule:: ska_sdp_distributed_self_cal_prototype.workflow.tasks
    :members:
    :undoc-members:
    :show-inheritance:
